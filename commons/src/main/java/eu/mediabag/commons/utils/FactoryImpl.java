package eu.mediabag.commons.utils;

import eu.mediabag.commons.auth.UserCreds;

public abstract class FactoryImpl<T, U> {

    public abstract T getImpl(UserCreds creds, U additionalConfig);
    
	public abstract void shutdown();

}
