package eu.mediabag.commons.utils;

import java.util.ArrayList;

import org.slf4j.Logger;

/**
 * Does an Alan Edmonds special logging where exception stack traces are only
 * logged if debug is enabled. Normally an exception and all its causes are
 * logged.
 * 
 * @author sraja
 * 
 */
public class CleanLogger {

	private static final int DEPTH = 5;

	public static void logSelectiveException(Logger logger, String status, String message, Exception e) {
		if (logger.isDebugEnabled()) {
			logger.error(status + ": " + message, e);
		} else {
			ArrayList<String> causes = new ArrayList<String>();
			getCauses(e, causes, DEPTH);
			logger.error(status + ": " + message + " @ " + e.toString() + ":"
					+ (e.getStackTrace() != null ? " @ " + e.getStackTrace()[0] : "") + "; causes " + causes);
		}
	}

	private static void getCauses(Throwable e, ArrayList<String> causes, int depth) {
		if (depth-- == 0 || e.getCause() == null) {
			return;
		}
		e = e.getCause();
		causes.add(e.toString() + (e.getStackTrace() != null ? " @ " + e.getStackTrace()[0] : ""));
		getCauses(e, causes, depth);
	}

}
