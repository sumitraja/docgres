package eu.mediabag.commons.utils;

import java.util.Collection;

import eu.mediabag.commons.Organisation;
import eu.mediabag.commons.auth.NotAuthorisedException;
import eu.mediabag.commons.auth.Permission;
import eu.mediabag.commons.auth.UserCreds;

public interface PermissionsProvider {

	public Collection<Permission> getPermissions(Organisation org , UserCreds callerCreds) throws NotAuthorisedException;
}
