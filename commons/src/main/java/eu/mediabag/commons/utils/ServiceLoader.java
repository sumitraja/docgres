package eu.mediabag.commons.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceLoader {

    private static Logger log = LoggerFactory.getLogger(ServiceLoader.class);
	private static String SVC_PATH = "/services/";

	@SuppressWarnings("unchecked")
	public <T> T load(String service) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		InputStream is = getClass().getResourceAsStream(SVC_PATH + service);
		if (is == null) {
			throw new UnsupportedOperationException("Service could not be loaded as resource " + SVC_PATH
					+ service + " could not be found");
		}
		BufferedReader read = new BufferedReader(new InputStreamReader(is));
		String className;
		try {
			className = read.readLine();
		} catch (IOException e) {
			throw new UnsupportedOperationException("Failed to read from "
					+ SVC_PATH + service, e);
		}
		log.info("Service " + service + " resolved to " + className);
		if (className == null) {
			throw new UnsupportedOperationException("Resource " + SVC_PATH
					+ service + " did not contain the factory classname");
		}
		Class<T> factory = (Class<T>) Class.forName(className);
		return factory.newInstance();
	}
}
