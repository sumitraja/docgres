package eu.mediabag.commons.utils;

import java.util.Collection;

import eu.mediabag.commons.Organisation;
import eu.mediabag.commons.auth.NotAuthorisedException;
import eu.mediabag.commons.auth.Permission;
import eu.mediabag.commons.auth.UserCreds;

public abstract class PermissionsProvidingFactoryImpl<U, V> extends FactoryImpl<U, V> {
	
	public Collection<Permission> getServicePermissions(Organisation org, UserCreds callerCreds) throws NotAuthorisedException {
    	if(callerCreds.isWorldAdmin() || (callerCreds.getOrganisation().getId() == org.getId() && callerCreds.isAdmin())) {
    		return getServicePermissions(org);
    	} else {
    		throw new NotAuthorisedException();
    	}
    }

    protected abstract Collection<Permission> getServicePermissions(Organisation org);


}
