/**
 * 
 */
package eu.mediabag.commons.utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * @author sumitraja
 * 
 */
public class StorageUtils {

	public static void copyFrom(ReadableByteChannel src, WritableByteChannel dest, long size, ProgressListener l)
			throws IOException {
		int bufferSize = size > 8192L ? 8192 : (int) size;

		ByteBuffer buf = ByteBuffer.allocate(bufferSize);
		buf.clear();
		int read = 0;
		for (long remaining = size; remaining > 0; remaining -= read) {
			buf.rewind();
			if (remaining < bufferSize) {
				buf.limit((int) remaining);
			}
			read = src.read(buf);
			buf.flip();
			int wrote = dest.write(buf);
			if (l != null) {
				l.progress(wrote);
			}
		}
	}

	public static void copyFrom(ReadableByteChannel src, WritableByteChannel dest, ProgressListener l)
			throws IOException {
		int bufferSize = 8192;

		ByteBuffer buf = ByteBuffer.allocate(bufferSize);
		buf.clear();
		int read = src.read(buf);
		
		while (read != -1) {
			buf.flip();
			int wrote = dest.write(buf);
			if (l != null) {
				l.progress(wrote);
			}
			buf.rewind();
			read = src.read(buf);
		}
	}

	public static interface ProgressListener {

		void progress(int i);

	}

	public static class TotalTrackingProgressListener implements ProgressListener {
		public long total = 0;

		@Override
		public void progress(int i) {
			total += i;
		}

	}

}
