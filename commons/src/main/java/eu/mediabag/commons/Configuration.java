package eu.mediabag.commons;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class Configuration {

	private Set<String> models = new HashSet<String>();

	private Set<String> workflows = new HashSet<String>();

	private Set<String> allowedLanguages = new HashSet<String>();

	private String apiURL;

	public Set<String> getModels() {
		return Collections.unmodifiableSet(models);
	}

	public void setModels(Set<String> models) {
		this.models.clear();
		this.models.addAll(models);
	}

	public Set<String> getWorkflows() {
		return Collections.unmodifiableSet(workflows);
	}

	public void setWorkflows(Set<String> workflows) {
		this.workflows.clear();
		this.workflows.addAll(workflows);
	}

	public String getApiURL() {
		return apiURL;
	}

	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}

	public void setAllowedLanguages(Set<String> allowedLanguages) {
		this.allowedLanguages.clear();
		this.allowedLanguages.addAll(allowedLanguages);
	}

	public Set<String> getAllowedLanguages() {
		return Collections.unmodifiableSet(allowedLanguages);
	}

	public static Configuration from(Properties configProps) {
		Configuration config = new Configuration();
		config.setApiURL(configProps.getProperty("apiURL"));
		String prop = configProps.getProperty("models");
		Set<String> modelsList = new HashSet<String>(Arrays.asList(prop
				.split(",")));
		config.setModels(modelsList);
		prop = configProps.getProperty("workflows");
		Set<String> workflowsList = new HashSet<String>(Arrays.asList(prop
				.split(",")));
		config.setWorkflows(workflowsList);
		String lang = configProps.getProperty("languages");
		Set<String> allowedLanguages = new HashSet<String>(Arrays.asList(lang
				.split(",")));
		config.setAllowedLanguages(allowedLanguages);
		return config;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apiURL == null) ? 0 : apiURL.hashCode());
		result = prime * result + ((models == null) ? 0 : models.hashCode());
		result = prime * result
				+ ((workflows == null) ? 0 : workflows.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Configuration other = (Configuration) obj;
		if (apiURL == null) {
			if (other.apiURL != null)
				return false;
		} else if (!apiURL.equals(other.apiURL))
			return false;
		if (models == null) {
			if (other.models != null)
				return false;
		} else if (!models.equals(other.models))
			return false;
		if (workflows == null) {
			if (other.workflows != null)
				return false;
		} else if (!workflows.equals(other.workflows))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Configuration [models=" + models + ", workflows=" + workflows
				+ ", allowedLanguages=" + allowedLanguages + ", apiURL=" + apiURL + "]";
	}
}
