package eu.mediabag.commons;

public class Tuple<K, V> {

	private K _1;
	private V _2;

	public Tuple(K one, V two) {
		_1 = one;
		_2 = two;
	}
	
	public Tuple() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((get_1() == null) ? 0 : get_1().hashCode());
		result = prime * result + ((_2 == null) ? 0 : _2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		Tuple other = (Tuple) obj;
		if (get_1() == null) {
			if (other.get_1() != null)
				return false;
		} else if (!get_1().equals(other.get_1()))
			return false;
		if (_2 == null) {
			if (other._2 != null)
				return false;
		} else if (!_2.equals(other._2))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tuple [_1=" + get_1() + ", _2=" + _2 + "]";
	}

	public void set_1(K _1) {
		this._1 = _1;
	}

	public K get_1() {
		return _1;
	}

	public void set_2(V _2) {
		this._2 = _2;
	}

	public V get_2() {
		return _2;
	}
}
