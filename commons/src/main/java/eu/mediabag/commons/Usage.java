/**
 * 
 */
package eu.mediabag.commons;

public enum Usage {

	REQUIRED, OPTIONAL, NOTALLOWED
}