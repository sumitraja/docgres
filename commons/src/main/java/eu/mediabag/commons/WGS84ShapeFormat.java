package eu.mediabag.commons;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.List;

public class WGS84ShapeFormat extends Format {

    private static final long serialVersionUID = -8369471955691961822L;
    private WGS84PointFormat pointFormat = new WGS84PointFormat();

    @SuppressWarnings("unchecked")
    @Override
    public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
	List<WGS84Point> points = (List<WGS84Point>) obj;
	for (WGS84Point point : points) {
	    toAppendTo.append(pointFormat.format(point)).append(",");
	}
	toAppendTo.deleteCharAt(toAppendTo.lastIndexOf(","));
	return toAppendTo;
    }

    @Override
    public Object parseObject(String source, ParsePosition pos) {
	String[] strs = source.split(",");
	if (strs.length < 1) {
	    pos.setErrorIndex(0);
	    return null;
	}
	List<WGS84Point> points = new ArrayList<WGS84Point>();
	for (String latLong : strs) {
	    points.add((WGS84Point) pointFormat.parseObject(latLong, pos));
	    if (pos.getErrorIndex() > 0) {
		pos.setIndex(0);
		return null;
	    }
	}
	return points;
    }

}
