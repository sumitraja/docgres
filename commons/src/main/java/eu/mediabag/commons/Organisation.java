package eu.mediabag.commons;

import eu.mediabag.commons.auth.AuthenticationFailedException;
import eu.mediabag.commons.auth.Role;

public abstract class Organisation {
	
	private String id;
	
	private String name;

	private String adminEmail;
	
	private Configuration configuration;

	protected Organisation() {}
	
	public Organisation(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public abstract Role getRole(String id) throws AuthenticationFailedException;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Organisation))
			return false;
		Organisation other = (Organisation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Organisation [id=" + id + ", name=" + name + ", adminEmail=" + adminEmail + "]";
	}
}
