/**
 * 
 */
package eu.mediabag.commons;

import java.io.Serializable;

public enum Multiplicity implements Serializable {

	SIMPLE, BAG, ALT, SEQ, NONE;
}