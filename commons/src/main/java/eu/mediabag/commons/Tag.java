package eu.mediabag.commons;

import java.io.Serializable;

import eu.mediabag.commons.LangLiteral;

public class Tag implements Serializable {
	private static final long serialVersionUID = -7033219774451322912L;
	public final String id;
    public final LangLiteral title;
    private Tag parent;

    public Tag(String id, LangLiteral title) {
        this(id, title, null);
    }

    public Tag(String id, LangLiteral title, Tag parent) {
        this.id = id;
        this.title = title;
        this.parent = parent;
    }

    public Tag(TagId id, LangLiteral title) {
        this(id.getId(), title, null);
    }

    public String getAbsoluteId() {
        if (parent != null) {
            return parent.getAbsoluteId() + "." + id;
        } else {
            return id;
        }
    }

    public Tag getParent() {
        return parent;
    }

    public void setParent(Tag parent) {
        this.parent = parent;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tag other = (Tag) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!getAbsoluteId().equals(other.getAbsoluteId()))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Tag [absoluteId=" + getAbsoluteId() + ", title=" + title + "]";
    }

}