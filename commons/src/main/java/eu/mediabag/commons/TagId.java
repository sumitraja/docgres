package eu.mediabag.commons;

import java.util.regex.Pattern;

/**
 * Marker class for the ltree handling
 */
public class TagId {

    private final static Pattern pattern = Pattern.compile("^([a-zA-Z0-9]*)(\\.[a-zA-Z0-9]+)*$");

    private final String id;

    public TagId(String id) {
        if (!pattern.matcher(id).find()) {
            throw new IllegalArgumentException("Wrong format for tags");
        }
        this.id = id;
    }


    public String getId() {
        return id;
    }
}
