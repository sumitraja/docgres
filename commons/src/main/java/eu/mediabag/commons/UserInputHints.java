package eu.mediabag.commons;

public enum UserInputHints {

	CreateAsset, FindAsset, BrowseFile
}
