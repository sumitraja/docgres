/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package eu.mediabag.commons;

import java.util.Locale;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * 
 * @author sraja
 */
public interface DateFormats {

	/**
	 * No timezones here, all repository dateTimes are converted to UTC.
	 */
	public static final String ISO8601_FULL_FORMAT_STRING_MS = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	public static final String ISO8601_FULL_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String ISO8601_DATE_FORMAT_STRING = "yyyy-MM-dd";
	/** Format string suitable for xsd:Time */
	public static final String ISO8601_TIME_FORMAT_STRING = "HH:mm:ss";
	public static final String ISO8601_TIME_FORMAT_STRING_MS = "HH:mm:ss.SSS";

	public static DateTimeFormatter modifiedTimestampFormat = DateTimeFormat.forPattern(DateFormats.ISO8601_FULL_FORMAT_STRING_MS)
			.withLocale(Locale.UK).withZone(DateTimeZone.UTC);
}
