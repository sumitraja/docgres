package eu.mediabag.commons.auth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


// Rename to capability??
public class Permission {

	public final String id;

	public final String serviceKey;

	public Permission(String id, String serviceKey) {
		if(id.endsWith(">")) {
			this.id = id.substring(0, id.length()-1);
		} else {
			this.id = id;
		}
		this.serviceKey = serviceKey;
	}

	/**
	 * Checks if this permission covers other one. E.g. assetModel>http://mediabag.biz/model#content covers
	 * assetModel>http://mediabag.biz/model#content>http://purl.org/dcterms/title
	 * @param permission
	 */
	public boolean covers(Permission perm) {
		if(!perm.serviceKey.equals(this.serviceKey)) {
			return false;
		}
		if(perm.id.equals(this.id)) {
			return true;
		}
		
		Pattern pattern = Pattern.compile(id + ">.*");
		Matcher matcher = pattern.matcher(perm.id);
		return matcher.find();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((serviceKey == null) ? 0 : serviceKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permission other = (Permission) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (serviceKey == null) {
			if (other.serviceKey != null)
				return false;
		} else if (!serviceKey.equals(other.serviceKey))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Permission [id=" + id + ", serviceKey=" + serviceKey + "]";
	}
	
	public static String generateId(String... components) {
		StringBuilder builder = new StringBuilder();
		for(String comp : components) {
			builder.append(comp).append(">");
		}
		return builder.substring(0, builder.length()-1);
	}
}


