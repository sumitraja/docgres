package eu.mediabag.commons.auth;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


public abstract class BaseUserCreds implements UserCreds {

	private Set<Role> roles = new HashSet<Role>();
	
	@Override
	public boolean can(RolePermission permission) {
		for (Role role : getRoles()) {
			if (role.getPermissions().contains(permission)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isAdmin() {
		for(Role role : roles) {
			if (role.isAdmin()) {
				return true;
			}
		}
		return false;
	}

    @Override
    public boolean isWorldAdmin() {
        return false;
    }

    @Override
    public boolean isService() {
        return false;
    }

	@Override
	public Collection<Role> getRoles() {
		return Collections.unmodifiableSet(roles);
	}

	public void setRoles(Collection<Role> roles) {
		this.roles.clear();
		this.roles.addAll(roles);
	}
	public void addRole(Role role) {
		this.roles.add(role);
	}
	@Override
	public boolean hasRole(Role roleStr) {
		return roles.contains(roleStr);
	}
	
	@Override
	public String getUniqueRepresentation() {
		 return getUser() + "\u2208" + getOrganisation().getId();
	}

}
