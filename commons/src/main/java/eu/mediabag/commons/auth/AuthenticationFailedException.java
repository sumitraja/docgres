package eu.mediabag.commons.auth;



public class AuthenticationFailedException extends Exception {

	/**
     * 
     */
	private static final long serialVersionUID = -2536443404478073319L;

	public AuthenticationFailedException(String cred, Exception e) {
		super("Failed to authorise " + cred, e);
	}

	public AuthenticationFailedException(String username, String string) {
		super("Failed to authorise " + username +": " + string);
	}

}
