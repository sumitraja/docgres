package eu.mediabag.commons.auth;

import eu.mediabag.commons.Access;

public class RolePermission {
	
	public final Access access;
	
	public final Permission permission;

	public RolePermission(Access access, Permission permission) {
		this.access = access;
		this.permission = permission;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((access == null) ? 0 : access.hashCode());
		result = prime * result
				+ ((permission == null) ? 0 : permission.hashCode());
		return result;
	}

	/**
	 * Equals ignores the service key as there is no way to actually know the service key that registered this permission
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RolePermission other = (RolePermission) obj;
		if (access != other.access)
			return false;
		if (permission == null) {
			if (other.permission != null)
				return false;
		} else if (!permission.equals(other.permission))
			return false;
		return true;
	}
	

}
