package eu.mediabag.commons.auth;

import java.util.Collection;

import eu.mediabag.commons.Organisation;

public interface UserCreds {

	public String getUser();
	
	public Collection<Role> getRoles();
	
	public boolean isAdmin();

    public boolean isWorldAdmin();

    public boolean isService();
	
	public Organisation getOrganisation();
	
	public boolean hasRole(Role role);
	
	public char[] getSecret();
	
	public boolean can(RolePermission permission);
	
	public String getUniqueRepresentation();

}