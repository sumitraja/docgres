package eu.mediabag.commons.auth;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import eu.mediabag.commons.Access;


public class Role {

	private String id;

	private String displayName;

	private Map<String, RolePermission> permissions = new HashMap<String, RolePermission>();
	
	private Set<String> allowedWorkflows = new HashSet<String>();
	
	private Set<String> allowedModels = new HashSet<String>();
	
	private boolean admin = false;

	protected Role() {
	}

	public Role(String id, String displayName, Collection<RolePermission> perms) {
		this.id = id;
		this.displayName = displayName;
		for(RolePermission rp : perms) {
			permissions.put(rp.permission.id, rp);
		}
	}

	public Role(String id, String displayName, boolean admin) {
		this.id = id;
		this.displayName = displayName;
		setAdmin(admin);
	}

	@SuppressWarnings("unused")
	private Role(String id, String displayName) {
		this(id, displayName, true);
	}

	public String getId() {
		return id;
	}

	public void setId(String commonName) {
		this.id = commonName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Role))
			return false;
		Role other = (Role) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", displayName=" + displayName + ", admin=" + isAdmin() + ", permissions="
				+ permissions + "]";
	}

	public void setRolePermissions(Collection<RolePermission> permissions) {
		this.permissions.clear();
		for(RolePermission rp : permissions) {
			this.permissions.put(rp.permission.id, rp);
		}
	}

	/**
	 * Add the most a RolePermission if it is more restrictive than an existing one, if one exists, for that permission id
	 * @param perm
	 */
	public void addRolePermission(RolePermission perm) {
		if(permissions.containsKey(perm.permission)) {
			RolePermission rp = permissions.get(perm.permission);
			if(rp.access.allows(perm.access)) {
				this.permissions.put(perm.permission.id, perm);
			}
		} else {
			this.permissions.put(perm.permission.id, perm);
		}
	}

	public void addRolePermission(Access access, Permission perm) {
		addRolePermission(new RolePermission(access, perm));
	}

	public Set<RolePermission> getPermissions() {
		return new HashSet<>(permissions.values());
	}
	
	public Access getAccessForPermission(Permission perm) {
		RolePermission rolePerm = permissions.get(perm.id);
		if(rolePerm != null) {
			return rolePerm.access;
		}
		for(RolePermission rolePermLoop : permissions.values()) {
			if(rolePermLoop.permission.covers(perm)) {
				return rolePermLoop.access;
			}
		}
		return Access.NONE;
	}

	public void setAllowedModels(Set<String> allowedModels) {
		this.allowedModels = allowedModels;
	}

	public Set<String> getAllowedModels() {
		return allowedModels;
	}

	public void setAllowedWorkflows(Set<String> allowedWorkflows) {
		this.allowedWorkflows = allowedWorkflows;
	}

	public Set<String> getAllowedWorkflows() {
		return allowedWorkflows;
	}

}
