package eu.mediabag.commons;

import java.net.URI;
import java.text.Format;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DataTypeConvertor {

    /**
     * Format string suitable for xsd:DateTime
     */
    protected DateTimeFormatter modifiedTimestampFormat;
    protected DateTimeFormatter dateFormat;
    protected DateTimeFormatter timeFormat;
    protected DateTimeFormatter dateTimeFormat;
    private DurationFormat durationFormat = new DurationFormat();
    protected final Format wgs84PointFormat = new WGS84PointFormat();
    protected final Format wgs84CircleFormat = new WGS84CircleFormat();
    protected final Format wgs84ShapeFormat = new WGS84ShapeFormat();

    public DataTypeConvertor(String formatStringMS, String dateTimeFormatString, String dateFormatString,
            String timeFormatString) {
        modifiedTimestampFormat = DateTimeFormat.forPattern(formatStringMS).withLocale(Locale.ENGLISH)
                .withZone(DateTimeZone.UTC);
        dateTimeFormat = DateTimeFormat.forPattern(dateTimeFormatString).withLocale(Locale.ENGLISH)
                .withZone(DateTimeZone.UTC);
        dateFormat = DateTimeFormat.forPattern(dateFormatString).withLocale(Locale.ENGLISH).withZone(DateTimeZone.UTC);
        timeFormat = DateTimeFormat.forPattern(timeFormatString).withLocale(Locale.ENGLISH).withZone(DateTimeZone.UTC);
    }

    public DateTime convertDateTime(String propVal) {
        return dateTimeFormat.parseDateTime(propVal);
    }

    public LocalTime convertTime(String propVal) {
        return timeFormat.parseDateTime(propVal).toLocalTime();
    }

    public DateTime convertDate(String propVal) {
        return dateFormat.parseDateTime(propVal);
    }

    public Duration convertDuration(String propVal) {
        try {
            return (Duration) durationFormat.parseObject(propVal);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public DateTime convertModifiedTimestamp(String value) {
        return modifiedTimestampFormat.parseDateTime(value);
    }

    public String convertModifiedTimestamp(DateTime time) {
        return modifiedTimestampFormat.print(time);
    }

    public String convertDate(DateTime DateTime) {
        return dateFormat.print(DateTime);
    }

    public String convertDate(DateTime DateTime, Locale locale) {
        return dateFormat.withLocale(locale).print(DateTime);
    }

    public String convertDateTime(DateTime DateTime) {
        return dateTimeFormat.print(DateTime);
    }

    public String convertDateTime(DateTime DateTime, Locale locale) {
        return dateTimeFormat.withLocale(locale).print(DateTime);
    }

    public String convertDuration(Duration duration) {
        return durationFormat.format(duration);
    }

    public String convertTime(DateTime time) {
        return timeFormat.print(time);
    }

    public WGS84Circle convertCircle(String circleString) {
        try {
            return (WGS84Circle) wgs84CircleFormat.parseObject(circleString);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String convertCircle(WGS84Circle circle) {
        return wgs84CircleFormat.format(circle);
    }

    @SuppressWarnings("unchecked")
    public List<WGS84Point> convertShape(String shapeString) {
        try {
            return (List<WGS84Point>) wgs84ShapeFormat.parseObject(shapeString);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String convertShape(List<WGS84Point> shape) {
        return wgs84ShapeFormat.format(shape);
    }

    public WGS84Point convertPoint(String point) {
        try {
            return (WGS84Point) wgs84PointFormat.parseObject(point);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String convertPoint(WGS84Point point) {
        return wgs84PointFormat.format(point);
    }

    public boolean validateDataType(DataType type, String propVal) {
        try {
            if (type != DataType.LITERALLANG) {
                switch (type) {
                case BOOLEAN:
                    return propVal.equalsIgnoreCase(Boolean.TRUE.toString())
                            || propVal.equalsIgnoreCase(Boolean.FALSE.toString());
                case DATETIME:
                    convertDateTime(propVal);
                    break;
                case TIME:
                    convertTime(propVal);
                    break;
                case DATE:
                    convertDate(propVal);
                    break;
                case DURATION:
                    convertDuration(propVal);
                    break;
                case INTEGER:
                    Integer.parseInt(propVal);
                    break;
                case DECIMAL:
                    Float.parseFloat(propVal);
                    break;
                case ANYURI:
                    new URI(propVal);
                    break;
                case UUID:
                case ASSET:
                    UUID.fromString(propVal);
                    break;
                case WGS84_POINT:
                    wgs84PointFormat.parseObject(propVal);
                    break;
                case WGS84_CIRCLE:
                    wgs84CircleFormat.parseObject(propVal);
                    break;
                case WGS84_SHAPE:
                    wgs84ShapeFormat.parseObject(propVal);
                    break;
                case TAG:
                    new TagId(propVal);
                default:
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}