package eu.mediabag.commons;

public interface MediabagURIs {
	public static String BASE_URI = "http://mediabag.biz/schema/";
	public static String MODEL_URI = BASE_URI + "model/";
	public static String DATATYPE_URI = BASE_URI + "datatype/";
	public static String ACCESS = MODEL_URI + "access";
	public static String USAGE = MODEL_URI + "usage";
	public static String VALUES = MODEL_URI + "values";
	public static String VALUE_HANDLER = MODEL_URI + "valueHandler";
	public static String VALUE_HANDLER_CONFIG = VALUE_HANDLER + "Config";
	public static String KEY = MODEL_URI + "key";
	public static String RESTRICTIONTYPE = MODEL_URI + "restriction";
	public static String USER_INPUT_HINTS = MODEL_URI + "userInputHint";
	public static String MULTIPLICITY = MODEL_URI + "multiplicity";

	public static String DESCRIPTION = "http://purl.org/dc/terms/description";
	public static String SUBJECT = "http://purl.org/dc/terms/subject";
	public static String ASSOCIATED = "	http://purl.org/dc/terms/isRequiredBy";

}
