package eu.mediabag.commons;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.ParsePosition;

public class WGS84CircleFormat extends WGS84PointFormat {

	private static final long serialVersionUID = -5316236962155131874L;
	private DecimalFormat radiusFormat = new DecimalFormat("#######0.#####");

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if (obj instanceof WGS84Circle) {
			WGS84Circle circle = (WGS84Circle) obj;
			StringBuffer ret = super.format(obj, toAppendTo, pos);
			ret.append(",").append(radiusFormat.format(circle.getRadius()));
			return ret;
		} else {
			throw new IllegalArgumentException("Cannot format a " + obj.getClass().toString());
		}
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		String[] strs = source.split(",");
		if (strs.length < 2) {
			pos.setErrorIndex(pos.getIndex());
			pos.setIndex(0);
			return null;
		}
		WGS84Point point = (WGS84Point) super.parseObject(strs[0], pos);
		if (pos.getErrorIndex() != -1) {
			pos.setIndex(0);
			return null;
		}
		
		WGS84Circle circle = null;
		if (pos.getIndex() > 0) {
			try {
				circle = new WGS84Circle(point, radiusFormat.parse(strs[1].trim()));
				pos.setIndex(pos.getIndex() + 1);
			} catch (ParseException e) {
				pos.setErrorIndex(pos.getIndex());
			}
		}
		return circle;
	}

}
