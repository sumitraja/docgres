package eu.mediabag.commons;

public enum Access {
    READWRITE, READ, NONE, EXECUTE;

    /** What access does this allow */
	public boolean allows(Access access) {
	   if(this == NONE) {
		   return false;
	   }
	   if (this == access || (this == READWRITE && access == READ)) {
		   return true;
	   }
	   return false;
    }
}
