package eu.mediabag.commons;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;


public class DurationFormat extends Format {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1132210387301395050L;

	@Override
	public StringBuffer format(Object arg0, StringBuffer arg1,
			FieldPosition arg2) {
		if (arg0 instanceof Duration) {
			arg1.append(((Duration) arg0).toString());
		} else {
			throw new IllegalArgumentException("Cannot format a "
					+ arg0.getClass().toString());
		}
		return arg1;
	}

	@Override
	public Object parseObject(String arg0, ParsePosition arg1) {
		Duration dur = null;
		try {
			dur = new Duration(arg0);
			arg1.setIndex(arg0.length());
		} catch (IllegalArgumentException e) {
			arg1.setIndex(0);
		}
		return dur;
	}
}
