package eu.mediabag.commons;

import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

public class WGS84PointFormat extends Format {

	private static final long serialVersionUID = -7870701659561973226L;
	private DecimalFormat latLongFormat = new DecimalFormat("##0.#######;-##0.#######");

	@Override
	public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
		if (obj instanceof WGS84Point) {
			WGS84Point point = (WGS84Point) obj;
			toAppendTo.append(latLongFormat.format(point.getLongitude())).append(" ")
					.append(latLongFormat.format(point.getLatitude()));
		} else {
			throw new IllegalArgumentException("Cannot format a " + obj.getClass().toString());
		}
		return toAppendTo;
	}

	@Override
	public Object parseObject(String source, ParsePosition pos) {
		String[] strs = source.trim().split("\\s");
		if (strs.length < 2) {
			pos.setErrorIndex(pos.getIndex());
			return null;
		}
		String lon = strs[0];
		String lat = strs[1];
		WGS84Point point = new WGS84Point();
		try {
			point.setLatitude(latLongFormat.parse(lat));
			pos.setIndex(pos.getIndex() + 1);
		} catch (ParseException e) {
			pos.setErrorIndex(pos.getIndex());
		}
		try {
			point.setLongitude(latLongFormat.parse(lon));
			pos.setIndex(pos.getIndex() + 1);
		} catch (ParseException e) {
			pos.setErrorIndex(pos.getIndex());
		}
		
		return point;
	}

}
