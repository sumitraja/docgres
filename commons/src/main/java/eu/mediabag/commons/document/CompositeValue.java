package eu.mediabag.commons.document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import eu.mediabag.commons.DataType;
import eu.mediabag.commons.Multiplicity;

public class CompositeValue implements Serializable {

	private static final long serialVersionUID = -5848551601542924981L;
	private Multiplicity multiplicity;
	private Collection<PropertyValueElement> values;

	protected CompositeValue() {

	}

	public CompositeValue(Multiplicity multiplicity) {
		this.multiplicity = multiplicity;
		this.values = createCollectionForMultiplicity(multiplicity);
	}

	public CompositeValue(String value, String lang, DataType type) {
		this.multiplicity = Multiplicity.SIMPLE;
		this.values = new HashSet<PropertyValueElement>();
		values.add(new PropertyValueElement(value, lang, type));
	}

	public CompositeValue(Multiplicity multi, String value, String lang, DataType type) {
		this(multi, new PropertyValueElement(value, lang, type));
	}

	public CompositeValue(String value, DataType type) {
		this(value, null, type);
	}

	public CompositeValue(Multiplicity multi, PropertyValueElement... pves) {
		this.multiplicity = multi;
		this.values = createCollectionForMultiplicity(multi);
		for (PropertyValueElement pve : pves) {
			values.add(pve);
		}
	}

	public Multiplicity getMultiplicity() {
		return multiplicity;
	}

	public Collection<PropertyValueElement> getValues() {
		return Collections.unmodifiableCollection(values);
	}

	public PropertyValueElement getSingleValue() {
		if (values.size() == 0) {
			throw new IllegalStateException("No values found");
		}
		return values.iterator().next();
	}
	
	public String getLiteralForLanguage(String lang) {
		for (PropertyValueElement pve : values) {
			if (pve.getLang().equals(lang)) {
				return pve.getValue();
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((multiplicity == null) ? 0 : multiplicity.hashCode());
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompositeValue other = (CompositeValue) obj;
		if (multiplicity != other.multiplicity)
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	public void addValue(String value, String lang, DataType type) {
		values.add(new PropertyValueElement(value, lang, type));
	}

	public void addValue(PropertyValueElement element) {
		values.add(element);
	}

	public void removeValue(String value, String lang, DataType type) {
		values.remove(new PropertyValueElement(value, lang, type));
	}
	
	public void removeValue(PropertyValueElement pve) {
		values.remove(pve);
	}

	public void removeAll() {
		values.clear();
	}
	
	public boolean hasValues() {
		return !values.isEmpty();
	}
	@Override
	public String toString() {
		return "CompositeValue [multiplicity=" + multiplicity + ", values=" + values + "]";
	}

	private Collection<PropertyValueElement> createCollectionForMultiplicity(Multiplicity multiplicity) {
		Collection<PropertyValueElement> values;
		switch (multiplicity) {
		case SEQ:
			values = new ArrayList<PropertyValueElement>();
			break;
		default:
			values = new HashSet<PropertyValueElement>();
		}
		return values;
	}

}