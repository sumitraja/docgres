package eu.mediabag.commons.document;

import java.io.Serializable;

import eu.mediabag.commons.DataType;


public class PropertyValueElement implements Serializable {

	private static final long serialVersionUID = -5961193483547786524L;
    private String value;
    private String lang;
    private DataType type;
    
	public PropertyValueElement() {
    	
    }
    public PropertyValueElement(String value, String lang, DataType type) {
        this.value = value;
        setLang(lang);
        this.type = type;
    }
    
    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    public String getLang() {
        return lang;
    }

    public DataType getType() {
        return type;
    }

    public void setValue(String value) {
		this.value = value;
	}
    
    public void setLang(String lang) {
    	if (lang != null && lang.trim().length() == 0 ) {
    		lang = null;
    	}
		this.lang = lang;
	}
    
    public void setType(DataType type) {
		this.type = type;
	}
	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((lang == null) ? 0 : lang.hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PropertyValueElement other = (PropertyValueElement) obj;
        if (lang == null) {
            if (other.lang != null)
                return false;
        } else if (!lang.equals(other.lang))
            return false;
        if (getType() != other.getType())
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }
    
    @Override
	public String toString() {
		return "PropertyValueElement [value=" + value + ", lang=" + lang
				+ ", type=" + type + "]";
	}


}
