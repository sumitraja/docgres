/**
 * 
 */
package eu.mediabag.commons.document;

import java.util.HashMap;
import java.util.Map;

import eu.mediabag.commons.DataType;
import eu.mediabag.commons.Multiplicity;

public enum PropertyValidationFailedReason {
    PROPERTY_PROHIBITED, PROPERTY_REQUIRED, PROPERTY_REQUIRED_ALT, PROPERTY_TYPE_UUID, PROPERTY_TYPE_STRING, PROPERTY_TYPE_DATETIME, PROPERTY_TYPE_NUMBER, PROPERTY_TYPE_BOOLEAN, PROPERTY_TYPE_ANYURI, PROPERTY_TYPE_ASSET, PROPERTY_RESTRICTION_LIST, PROPERTY_SCRIPT_EVAL, PROPERTY_MULTIPLICITY_SIMPLE, PROPERTY_GROUP_ALT, PROPERTY_MULTIPLICITY_BAG, PROPERTY_TYPE_DATE, PROPERTY_TYPE_TIME, PROPERTY_TYPE_DURATION, PROPERTY_TYPE_MODIFIED_DATE, PROPERTY_TYPE_CREATE_DATE, PROPERTY_TYPE_LITERAL, PROPERTY_MULTIPLICITY_SEQ, PROPERTY_ACCESS_NOT_ALLOWED, PROPERTY_TYPE_TAG, PROPERTY_TYPE_WGS84_POINT, PROPERTY_TYPE_WGS84_CIRCLE, PROPERTY_TYPE_WGS84_SHAPE;

    private static Map<DataType, PropertyValidationFailedReason> typeErrorMap = new HashMap<DataType, PropertyValidationFailedReason>();

    public static PropertyValidationFailedReason getErrorForDataType(DataType type) {
        if (typeErrorMap.size() == 0) {
            typeErrorMap.put(DataType.BOOLEAN, PROPERTY_TYPE_BOOLEAN);
            typeErrorMap.put(DataType.DATE, PROPERTY_TYPE_DATE);
            typeErrorMap.put(DataType.DATETIME, PROPERTY_TYPE_DATETIME);
            typeErrorMap.put(DataType.DECIMAL, PROPERTY_TYPE_NUMBER);
            typeErrorMap.put(DataType.DURATION, PROPERTY_TYPE_DURATION);
            typeErrorMap.put(DataType.LITERALLANG, PROPERTY_TYPE_LITERAL);
            typeErrorMap.put(DataType.STRING, PROPERTY_TYPE_STRING);
            typeErrorMap.put(DataType.TIME, PROPERTY_TYPE_TIME);
            typeErrorMap.put(DataType.ANYURI, PROPERTY_TYPE_ANYURI);
            typeErrorMap.put(DataType.ASSET, PROPERTY_TYPE_ASSET);
            typeErrorMap.put(DataType.UUID, PROPERTY_TYPE_UUID);
            typeErrorMap.put(DataType.TAG, PROPERTY_TYPE_TAG);
            typeErrorMap.put(DataType.WGS84_CIRCLE, PROPERTY_TYPE_WGS84_CIRCLE);
            typeErrorMap.put(DataType.WGS84_POINT, PROPERTY_TYPE_WGS84_POINT);
            typeErrorMap.put(DataType.WGS84_SHAPE, PROPERTY_TYPE_WGS84_SHAPE);
        }
        return typeErrorMap.get(type);
    }

    public static PropertyValidationFailedReason getErrorForMultiplicity(Multiplicity multiplicity) {
        switch (multiplicity) {
        case SIMPLE:
            return PROPERTY_MULTIPLICITY_SIMPLE;
        case SEQ:
            return PROPERTY_MULTIPLICITY_SEQ;
        default:
            return PROPERTY_MULTIPLICITY_BAG;
        }
    }
}