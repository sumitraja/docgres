package eu.mediabag.commons;

public class WGS84Circle extends WGS84Point {
	
	private static final long serialVersionUID = 6921815891528412524L;
	private Number radius;

	public WGS84Circle(WGS84Point point, Number distance) {
		this.setLatitude(point.getLatitude());
		this.setLongitude(point.getLongitude());
		this.radius = distance;
	}

	public WGS84Circle() {
	}

	public Number getRadius() {
		return radius;
	}

	public void setRadius(Number radius) {
		this.radius = radius;
	} 
	
	@Override
	public boolean equals(Object object) {
		if (object instanceof WGS84Circle) {
			WGS84Circle circle = (WGS84Circle) object;
			return super.equals(object) && circle.radius.doubleValue() == radius.doubleValue();
		}
		return false;
	}

	@Override
	public String toString() {
		return "WGS84Circle [radius=" + radius + ", latitude()=" + getLatitude() + ", longitude()="
				+ getLongitude() + "]";
	}

}
