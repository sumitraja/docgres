package eu.mediabag.commons;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.Map;
import java.util.HashMap;

public enum DataType implements Serializable {

    UUID("http://mediabag.biz/datatypes#uuid"), STRING("http://www.w3.org/2001/XMLSchema#string"), DATETIME(
            "http://www.w3.org/2001/XMLSchema#dateTime"), DATE("http://www.w3.org/2001/XMLSchema#date"), DECIMAL(
            "http://www.w3.org/2001/XMLSchema#decimal"), BOOLEAN("http://www.w3.org/2001/XMLSchema#boolean"), ANYURI(
            "http://www.w3.org/2001/XMLSchema#anyURI"), INTEGER("http://www.w3.org/2001/XMLSchema#integer"), DURATION(
            "http://www.w3.org/2001/XMLSchema#duration"), TIME("http://www.w3.org/2001/XMLSchema#time"), LITERALLANG(
            "http://mediabag.biz/datatypes#languageLiteral"), ASSET("http://mediabag.biz/datatypes#asset"), TAG(
            "http://mediabag.biz/datatypes#tag"), WGS84_POINT("http://mediabag.biz/datatypes#wgs84Point"), WGS84_CIRCLE(
            "http://mediabag.biz/datatypes#wgs84Circle"), WGS84_SHAPE("http://mediabag.biz/datatypes#wgs84Shape"),
            QUERY("http://mediabag.biz/datatypes#query");
    private String xsName;
    private static final Map<String, DataType> lookup = new HashMap<String, DataType>();

    static {
        for (DataType s : EnumSet.allOf(DataType.class)) {
            lookup.put(s.toString(), s);
        }
    }

    private DataType(String xsname) {
        this.xsName = xsname;
    }

    @Override
    public String toString() {
        return xsName;
    }

    public static DataType fromXsdUri(String uri) {
        return lookup.get(uri);
    }
}
