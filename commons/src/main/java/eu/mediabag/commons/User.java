package eu.mediabag.commons;

import java.util.List;

public class User {
	
	private String commonName;
	
	private String surname;
	
	private List<String> givenName;
	
	private String id;
	
	private String emailAddress;
	

	public User(String id, String commondName) {
		this.id = id;
		commonName = commondName;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public List<String> getGivenName() {
		return givenName;
	}

	public void setGivenName(List<String> givenName) {
		this.givenName = givenName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
