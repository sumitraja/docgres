package eu.mediabag.commons;

import java.io.Serializable;

public class WGS84Point implements Serializable {

	private static final long serialVersionUID = 5120183664468113328L;

	private Number latitude;

	private Number longitude;

	public WGS84Point(Number lon, Number lat) {
		this.latitude = lat;
		this.longitude = lon;
	}

	public WGS84Point() {
	}

	public Number getLatitude() {
		return latitude;
	}

	public void setLatitude(Number latitude) {
		this.latitude = latitude;
	}

	public Number getLongitude() {
		return longitude;
	}

	public void setLongitude(Number longitude) {
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof WGS84Point) {
			WGS84Point point = (WGS84Point) object;
			return latitude.doubleValue() == point.getLatitude().doubleValue()
					&& longitude.doubleValue() == point.getLongitude().doubleValue();
		}
		return false;
	}

	@Override
	public String toString() {
		return "WGS84Point [latitude=" + latitude + ", longitude=" + longitude + "]";
	}
}
