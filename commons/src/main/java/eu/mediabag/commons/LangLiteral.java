package eu.mediabag.commons;

import java.io.Serializable;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class LangLiteral implements Serializable {

	private static final long serialVersionUID = -8935092851578506355L;
	private final Map<String, String> langToLitMap = new HashMap<String, String>();

	public LangLiteral() {
	}

	public LangLiteral(String literal, String lang) {
		addLiteral(literal, lang);
	}

	/**
	 * Default literal constructor. Literal will be normalised to the NFC form and then stored
	 * 
	 * @param literal
	 */
	public LangLiteral(String literal) {
		addLiteral(literal, null);
	}
	
	public LangLiteral(Map<String, String> langLitMap) {
	    for(Map.Entry<String, String> entry : langLitMap.entrySet()) {
	        addLiteral(entry.getValue(), entry.getKey());
	    }
	}

	/**
	 * Gets the literal for the given locale. If the literal could not be found
	 * super sets will be tried. E.g. if a request for de_AT comes in and that
	 * have no values, de will be tried followed by "".
	 * 
	 * Variants are removed so en_GB_POSIX will be stripped to en_GB first.
	 * 
	 * @param lang
	 * @return The string for a locale or its super set. If none could be found
	 *         then the default will be returned.
	 */
	public String getLiteral(String lang) {
		if (lang.split("_").length > 2) {
			lang = lang.substring(0, lang.lastIndexOf("_"));
		}
		if (langToLitMap.containsKey(lang)) {
			return langToLitMap.get(lang);
		} else {
			if (lang.contains("_")) {
				lang = lang.split("_")[0];
				return getLiteral(lang);
			} else if (lang.length() > 0) {
				return getLiteral("");
			}
		}
		return null;
	}

	/**
	 * "" is the default language
	 * 
	 * @param literal
	 * @param lang
	 */
	public void addLiteral(String literal, String lang) {
		if (lang == null) {
			lang = "";
		}
		langToLitMap.put(lang, Normalizer.normalize(literal, Form.NFC));
	}

	public Map<String, String> getLangToLiteralMap() {
		return Collections.unmodifiableMap(langToLitMap);
	}

	/**
	 * @param literal
	 *            Literal to use as default
	 * @param lang
	 *            the actual language of the literal
	 */
	public void setDefaultLiteral(String literal, String lang) {
		addLiteral(literal, lang);
		addLiteral(literal, null);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((langToLitMap == null) ? 0 : langToLitMap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LangLiteral other = (LangLiteral) obj;
		if (langToLitMap == null) {
			if (other.langToLitMap != null)
				return false;
		} else if (!langToLitMap.equals(other.langToLitMap))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for (Map.Entry<String, String> entry : langToLitMap.entrySet()) {
			builder.append("\"" + entry.getValue() + "\"@" + entry.getKey()).append(", ");
		}
		builder.append("]");
		return builder.toString();
	}

	public String getDefaultLiteral() {
		return langToLitMap.get("");
	}
}
