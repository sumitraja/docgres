package eu.mediabag.commons;

public enum State {
	CHANGED, REMOVED;
}
