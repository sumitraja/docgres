package eu.mediabag.commons;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AccessTest {

	@Test
	public void testAllows() {
		assertTrue(Access.READWRITE.allows(Access.READ));
		assertTrue(Access.READWRITE.allows(Access.READWRITE));
		assertTrue(Access.EXECUTE.allows(Access.EXECUTE));
		assertFalse(Access.NONE.allows(Access.READ));
		assertFalse(Access.NONE.allows(Access.READWRITE));
		assertFalse(Access.NONE.allows(Access.EXECUTE));
		assertFalse(Access.READ.allows(Access.READWRITE));
		assertFalse(Access.EXECUTE.allows(Access.READWRITE));
		assertFalse(Access.EXECUTE.allows(Access.READ));
		assertFalse(Access.READ.allows(Access.NONE));
		assertFalse(Access.EXECUTE.allows(Access.NONE));
		assertFalse(Access.READWRITE.allows(Access.NONE));	
	}

}
