package eu.mediabag.commons;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LangLiteralTest {

	@Test
	public void testGet() {
		LangLiteral ll = new LangLiteral();
		ll.addLiteral("en_GB_POSIX", "en_GB");
		ll.setDefaultLiteral("en", "en");
		ll.addLiteral("fr", "fr");
		assertEquals("en_GB_POSIX", ll.getLiteral("en_GB_POSIX"));
		assertEquals("en", ll.getLiteral("en"));
		assertEquals("en", ll.getLiteral("en_US"));
		assertEquals("en", ll.getLiteral("de_DE"));
		
		assertEquals("fr", ll.getLiteral("fr_FR"));
		assertEquals("fr", ll.getLiteral("fr"));
	}

}
