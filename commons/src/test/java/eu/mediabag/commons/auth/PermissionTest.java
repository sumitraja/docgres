package eu.mediabag.commons.auth;

import static org.junit.Assert.*;

import org.junit.Test;

public class PermissionTest {

	@Test
	public void testCovers() {
		Permission root = new Permission("level>http://mediabag.biz/model/locationBasedContent", "key");
		Permission cover = new Permission("level>http://mediabag.biz/model/locationBasedContent>http://purl.org/title", "key");
		assertTrue(root.covers(cover));
		assertFalse(cover.covers(root));
	}

	@Test
	public void testGenerateId() {
		assertEquals("r>b", Permission.generateId("r", "b"));
	}

}
