package eu.mediabag.commons.auth;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import eu.mediabag.commons.Access;

public class RoleTest {

	@Test
	public void testPermissionRestriction() {
		Role subject = new Role("id", "displayName", false);
		Permission perm = new Permission("id", "serviceKey");
		subject.addRolePermission(Access.READWRITE, perm);
		assertTrue(subject.getPermissions().contains(new RolePermission(Access.READWRITE, perm)));
		subject.addRolePermission(Access.READ, perm);
		assertTrue(subject.getPermissions().contains(new RolePermission(Access.READ, perm)));
		assertFalse(subject.getPermissions().contains(new RolePermission(Access.READWRITE, perm)));
	}

}
