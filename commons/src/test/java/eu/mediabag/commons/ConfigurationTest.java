package eu.mediabag.commons;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Test;

public class ConfigurationTest {

	private static String props = "models=http://mediabag.biz/model#content\nworkflows=urn:workflow:video approval\n"
			+ "apiURL=http://localhost:8180\n"
			+ "languages=en,fr,de\n";

	@Test
	public void testFrom() throws IOException {
		Properties configProps = new Properties();
		ByteArrayInputStream inStream = new ByteArrayInputStream(props.getBytes());
		configProps.load(inStream);
		Configuration config = Configuration.from(configProps);
		assertEquals("http://localhost:8180", config.getApiURL());
		assertThat(config.getWorkflows(), hasItem("urn:workflow:video approval"));
		assertThat(config.getModels(), hasItem("http://mediabag.biz/model#content"));
		assertThat(config.getAllowedLanguages(), hasItem("en"));
		assertThat(config.getAllowedLanguages(), hasItem("fr"));
		assertThat(config.getAllowedLanguages(), hasItem("de"));
	}

}
