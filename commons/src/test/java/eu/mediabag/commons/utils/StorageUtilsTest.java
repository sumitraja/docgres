package eu.mediabag.commons.utils;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import org.junit.Test;

import eu.mediabag.commons.utils.StorageUtils.ProgressListener;

public class StorageUtilsTest {

    private int total;

    @Test
    public void testCopy() throws Exception {
        total = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 100000; i++) {
            sb.append("pos="+i);
        }
        final ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes());
        bis.skip(5000);
        ReadableByteChannel rbc = Channels.newChannel(bis);
        
        final ByteArrayOutputStream bos = new ByteArrayOutputStream(sb.toString().getBytes().length);
        WritableByteChannel wbc = Channels.newChannel(bos);
        
        
        ProgressListener l = new TestListener();

        StorageUtils.copyFrom(rbc, wbc, 3000, l);
        bos.flush();
        wbc.close();
        String string = bos.toString();
        assertEquals(sb.toString().substring(5000, 8000), string);
        assertEquals(string.length(), total);
    }

    class TestListener implements StorageUtils.ProgressListener {

        {
            total = 0;
        }
        @Override
        public void progress(int i) {
            total += i;
        }
    };
}
