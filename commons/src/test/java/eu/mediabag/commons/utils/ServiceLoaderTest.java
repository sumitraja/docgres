package eu.mediabag.commons.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class ServiceLoaderTest {

	@Test
	public void testLoad() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		ServiceLoader subject = new ServiceLoader();
		assertTrue(subject.load("test.service") instanceof eu.mediabag.commons.utils.DummyService);
	}

}
