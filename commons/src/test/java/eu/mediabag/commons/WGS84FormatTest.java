package eu.mediabag.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;

public class WGS84FormatTest {

	@Test
	public void testPoint() throws Exception {
		String pointStr = "123.333 -86.5543";
		WGS84PointFormat subject = new WGS84PointFormat();
		WGS84Point point = (WGS84Point) subject.parseObject(pointStr);
		assertEquals(123.333, point.getLongitude().doubleValue(), 0);
		assertEquals(new Double("-86.5543"), point.getLatitude());
		assertEquals(pointStr, subject.format(point));

		try {
			String pointBroken = "123.333,-86.5543";
			point = (WGS84Point) subject.parseObject(pointBroken);
			fail("Should not have been able to parse");
		} catch (Exception e) {

		}
	}

	@Test
	public void testCircle() throws Exception {
		String circleStr = "123.333 -86.5543,12750000.223";
		WGS84CircleFormat subject = new WGS84CircleFormat();
		WGS84Circle circle = (WGS84Circle) subject.parseObject(circleStr);
		assertEquals(123.333, circle.getLongitude().doubleValue(), 0);
		assertEquals(new Double("-86.5543"), circle.getLatitude());
		assertEquals(12750000.223, circle.getRadius());
		assertEquals(circleStr, subject.format(circle));
		try {
			String circleStrBroken = "123.333,-86.5543,12750000.223";
			circle = (WGS84Circle) subject.parseObject(circleStrBroken);
			fail("Should not have been able to parse");
		} catch (Exception e) {

		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testShape() throws Exception {
		String shapeStr = "123.333 -86.5543,33.33244 -333,-99 112";
		WGS84ShapeFormat subject = new WGS84ShapeFormat();
		List<WGS84Point> points = (List<WGS84Point>) subject.parseObject(shapeStr);
		
		assertEquals(123.333, points.get(0).getLongitude().doubleValue(), 0);
		assertEquals(new Double("-86.5543"), points.get(0).getLatitude());
		
		assertEquals(33.33244, points.get(1).getLongitude().doubleValue(), 0);
		assertEquals(new Long("-333"), points.get(1).getLatitude());

		assertEquals(new Double("-99"), points.get(2).getLongitude().doubleValue(), 0);
		assertEquals(112L, points.get(2).getLatitude());

		assertEquals(shapeStr, subject.format(points));
		try {
			String shapeStrBroken = "123.333 -86.5543, 33.33244,-333 -99 112";
			points = (List<WGS84Point>) subject.parseObject(shapeStrBroken);
			fail("Should not have been able to parse");
		} catch (Exception e) {

		}
	}
	

}
