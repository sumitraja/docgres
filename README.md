# About #

Document repository using Postgresql as the storage engine. Supports full text search, GIS searching and hierarchical tags using ltree. Can be used as a library at the moment as it only exposes a Scala/JAVA API.

### How do I get set up? ###

Needs the following set up on Postgres 9.x. Once complete the docgres-shell is a good place to start as it is a functional Scala REPL based command shell that can be used as a template and to test the API. 

### Run set up script

```bash
sudo su - postgres
psql -U postgres -c "create database template_mediabag encoding='UTF-8'"
psql -U postgres -c "create user contentmgr nosuperuser nocreatedb nocreaterole password 'contentmgr'"
psql -U postgres -c "create user jbpm nosuperuser nocreatedb nocreaterole password 'jbpm'"
psql -U contentmgr template_mediabag -c "CREATE SCHEMA assetrepository AUTHORIZATION contentmgr;"
psql -U contentmgr template_mediabag -c "CREATE SCHEMA tagmanager AUTHORIZATION contentmgr;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA assetrepository;"
psql -U contentmgr template_mediabag -c 'grant all on spatial_ref_sys to PUBLIC;'
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA assetrepository;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA public;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA assetrepository;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA tagmanager;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;"
psql -U contentmgr template_mediabag -c "CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA assetrepository;"
psql -U postgres -c "create database assetrepository owner=contentmgr template=template_mediabag  encoding='UTF-8'"
psql -U postgres -c "create database test_assetrepository owner=contentmgr template=template_mediabag  encoding='UTF-8'"
```

# TODO #

* Document usage
* Package as a server and add a web API layer
* Revisit some of the concepts around:
    * Query language - its pretty cumbersome
    * Use of factories
    * Testing infrastructure