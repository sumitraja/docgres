package biz.mediabag.postgres.jdbc

import org.scalatest.FunSuite

class JDBCConfigurationTest extends FunSuite {
  
  test("Make connection") {
    val subject = new JDBCConfiguration {
      assert(dataSource.getConnection != null)
      closeDataSource
      protected def configFilename:String = "mediabag.jdbc.properties"
    }
    
  }

}