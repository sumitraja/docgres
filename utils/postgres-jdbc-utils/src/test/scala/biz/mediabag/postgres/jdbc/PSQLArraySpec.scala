package biz.mediabag.postgres.jdbc

import org.scalatest.BeforeAndAfterAll
import org.scalatest.FunSuite
import eu.mediabag.commons.{TagId, WGS84Point}
import java.sql.DriverManager

class PSQLArraySpec extends FunSuite with BeforeAndAfterAll {

  val config = new JDBCConfiguration {
    protected def configFilename:String = "mediabag.jdbc.properties"
    def getDatasource = dataSource
  }

  implicit val conn = config.getDatasource.getConnection

  
  test("Array of Points") {
    val stmtString = "SELECT ?::geography[]"
    val sm = StatementWrapper.wrap(stmtString, List('array))
    sm.arrayValue('array, List(new WGS84Point(-71.064544, 42.28787), new WGS84Point(51.495727, -34.247342)))
    sm.executeQuery
  }

  test("ltree") {
    val stmtString = "select ? <@ '1.2'::ltree"
    val sm = StatementWrapper.wrap(stmtString, List('ltree))
    sm.singleValue('ltree, new TagId("1.2"))
    sm.executeQuery
  }

}

