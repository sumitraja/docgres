package biz.mediabag.postgres.jdbc

import java.net.URI

import scala.collection.JavaConversions._

import org.joda.time._
import org.joda.time.format._
import org.postgresql.ds.PGPoolingDataSource
import org.postgresql.ds.PGSimpleDataSource
import org.postgresql.ds.common.BaseDataSource
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import org.flywaydb.core.Flyway

import grizzled.slf4j.Logging
import javax.sql.DataSource

trait DatabaseTestSetup extends JDBCConfiguration with Logging {

  protected def schema: String
  protected def tablePrefix: String
  protected def flywayLocations: Seq[String]
  protected def printStream = System.out

  private var mFlyway: Flyway = null

  private var configured = false

  def ds = {
    dataSource
  }

  protected def configFilename: String = "simple.jdbc.properties"

  def flyway = {
    if (mFlyway == null) {
      assert(schema != null, "Use pre-initialisation of schema")
      mFlyway = new Flyway
      mFlyway.setDataSource(ds)
      mFlyway.setSchemas(schema)
      mFlyway.setPlaceholders(Map("tablePrefix" -> tablePrefix, "schemaName" -> schema))
      mFlyway.setLocations(flywayLocations: _*)
    }
    mFlyway
  }

  def cleanDatabase = {
    flyway.clean
  }

  def configureDatabase = {
    val schemaCreate = "CREATE SCHEMA IF NOT EXISTS %s".format(schema);
    val conn = ds.getConnection;
    conn.createStatement.execute(schemaCreate)
    conn.close
    flyway.migrate
  }

  def dropSchema = {
    val schemaDrop = "DROP SCHEMA IF EXISTS %s CASCADE".format(schema);
    val conn = ds.getConnection;
    conn.createStatement.execute(schemaDrop)
    conn.close
  }
}

trait Timer {

  def loadLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)
  def longLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)
  private val fmt: DateTimeFormatter = ISODateTimeFormat.dateTime

  def time[A](msg: String)(f: ⇒ A): A = {
    val start = new DateTime
    val ret = f
    val end = new DateTime
    val diff = new Duration(start, end)
    loadLogger.info(fmt.print(end) + "," + msg + ", " + diff.getMillis())
    ret
  }

  def timeWithResult[A](msg: String)(f: ⇒ A): A = {
    val start = new DateTime
    val ret = f
    val end = new DateTime
    val diff = new Duration(start, end)
    loadLogger.info(fmt.print(end) + "," + msg + "," + ret.toString() + "," + diff.getMillis())
    ret
  }

  def timeWithResultWithLongLog[A](msg: String, longLogMsg: String, longDuration: Int)(f: ⇒ A): A = {
    val start = new DateTime
    val ret = f
    val end = new DateTime
    val diff = new Duration(start, end)
    loadLogger.info(fmt.print(end) + "," + msg + "," + ret.toString() + "," + diff.getMillis())
    if (diff.getMillis() > longDuration) {
      longLogger.info("--------------------------------")
      longLogger.info(fmt.print(end) + "," + msg + "," + ret.toString() + "," + diff.getMillis())
      longLogger.info(longLogMsg)
      longLogger.info("--------------------------------")
    }
    ret
  }

}

