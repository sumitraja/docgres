package biz.mediabag.postgres.jdbc

import java.sql.Connection
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.mock.EasyMockSugar
import org.easymock.EasyMock._
import javax.sql.DataSource
import org.scalatest.FunSpec
import java.sql.SQLException
import org.easymock.EasyMock

class ConcurrentUpdateSpec extends FunSpec with ShouldMatchers with EasyMockSugar {
  val ds = mock[DataSource]
  val con = strictMock[Connection]

  def configureMocks(commit: Boolean, rollback:Boolean) = {
    EasyMock.reset(ds, con)
    ds.getConnection().andReturn(con).times(1)
    con.getTransactionIsolation.andReturn(Connection.TRANSACTION_READ_UNCOMMITTED).times(1)
    con.getAutoCommit.andReturn(true)
    con.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE).times(1)
    con.setAutoCommit(false).once
    if (rollback) con.rollback.times(1)
    if (commit) con.commit.times(1)
    con.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED).times(1)
    con.setAutoCommit(true).once
    con.close.once
  }

  describe("QueryRepeater") {
    it("should repeat") {
      configureMocks(false, true)
      val subject = new JDBCOps {
        def dataSource = ConcurrentUpdateSpec.this.ds

        def write = {
          repeatOnSQLStates(List("1234"), 1) execute { con =>
            throw new SQLException("reason", "1234", 1)
          }
        }
      }
      whenExecuting(ds, con) {
        intercept[SerialException] {
          subject.write
        }
      }

    }

    it("should repeat and commit") {
      configureMocks(true, false)
      val subject = new JDBCOps {
        def dataSource = ConcurrentUpdateSpec.this.ds

        def write = {
          repeatOnSQLStates(List("1234"), 1) execute { con =>

          }
        }
      }
      whenExecuting(ds, con) {
        subject.write
      }

    }

    it("should nest repeaters and rollback on failure") {
      configureMocks(false, true)
      val subject = new JDBCOps {
        def dataSource = ConcurrentUpdateSpec.this.ds

        def nest = {
          repeatOnSQLStates(List("1234"), 1) execute { con =>
            repeatOnSQLStates(List("1234"), 1, con) execute { innerCon =>
              con should be theSameInstanceAs innerCon
            }
            throw new SQLException("reason", "1234", 1)
          }
        }
      }
      whenExecuting(ds, con) {
        intercept[SerialException] {
          subject.nest
        }
      }
    }

    it("should rollback on nest repeater failure but commit on successful retry") {
      configureMocks(true, true)
      val subject = new JDBCOps {
        def dataSource = ConcurrentUpdateSpec.this.ds
        private var exceptionThrown = false
        def nest = {
          repeatOnSQLStates(List("1234"), 1) execute { con =>
            repeatOnSQLStates(List("1234"), 2, con) execute { innerCon =>
              if (!exceptionThrown) {
                exceptionThrown = true
                throw new SQLException("reason", "1234", 1)
              }
            }
          }
        }
      }
      whenExecuting(ds, con) {
        subject.nest
      }
    }

  }

}