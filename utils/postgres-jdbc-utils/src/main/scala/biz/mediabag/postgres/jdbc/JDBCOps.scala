package biz.mediabag.postgres.jdbc

import java.sql.{ SQLException, ResultSet, Connection }
import grizzled.slf4j.Logging
import javax.sql.DataSource
import scala.annotation.tailrec
import java.sql.Statement

trait JDBCOps extends Logging {

  private val MaxRetryCount = 1000
  
  protected def dataSource: DataSource

  def withConn[A](f: (Connection) => A, tranLevel: Int = Connection.TRANSACTION_SERIALIZABLE): A = {
    val con = dataSource.getConnection
    val oldTranLevel = con.getTransactionIsolation
    val oldAutoCommit = con.getAutoCommit
    try {
      con.setTransactionIsolation(tranLevel)
      con.setAutoCommit(false)
      val a = f(con)
      con.commit
      a
    } catch {
      case e: Throwable => {
        con.rollback
        throw e
      }
    } finally {
      con.setTransactionIsolation(oldTranLevel)
      con.setAutoCommit(oldAutoCommit)
      con.close
    }
  }

  def withConn[A](tranLevel: Int)(f: (Connection) => A): A = {
    withConn(f, tranLevel)
  }

  def repeatOnSQLStates[A](states: List[String], retryCounts: Int) = {
    new QueryRepeater[A](states, retryCounts, None)
  }
  
  def repeatOnSQLStates[A](states: List[String], retryCounts: Int, conn:Connection) = {
    new QueryRepeater[A](states, retryCounts, Some(conn))
  }
  
  def repeatOnSQLStates[A](states: List[String], conn:Connection) = {
    new QueryRepeater[A](states, MaxRetryCount, Some(conn))
  }

  def forEach[A](f: (ResultSet) => A) = {
    new ForEach(f)
  }

  def forOne[A](f: (ResultSet) => A) = {
    new ForOne(f)
  }

  def reduceAll[A](f: (ResultSet, A) => A) = {
    new ReduceAll(f)
  }

  def time[T](id: String)(fun: => T) = {
    val start = System.currentTimeMillis
    try {
      fun
    } finally {
      val end = System.currentTimeMillis
      debug(id + " took " + (end - start) + "ms")
    }
  }

  class ReduceAll[A](reduceAll: (ResultSet, A) => A) extends Processor[A] {
    def process(rs: ResultSet) = {
      var result: A = null.asInstanceOf[A]
      do {
        result = reduceAll(rs, result)
      } while ((rs.next))
      result
    }
  }

  class ForEach[A](forEach: (ResultSet) => A) extends Processor[List[A]] {
    def process(rs: ResultSet) = {
      var result: List[A] = Nil
      do {
        result = forEach(rs) :: result
      } while ((rs.next))
      result
    }
  }

  class ForOne[A](forOne: (ResultSet) => A) extends Processor[A] {
    def process(rs: ResultSet) = {
      forOne(rs)
    }
  }

  trait Processor[B] {
    private var alt: () => B = _
    protected def process(rs: ResultSet): B

    def using(ps: StatementWrapper): Option[B] = {
      go(ps.executeQuery)
    }

    def using(rs: ResultSet): Option[B] = {
      go(rs)
    }

    def orElse(alt: => B) = {
      this.alt = alt _
      this
    }

    private def go(rs: ResultSet) = {
      try {
        if (rs.next()) {
          Some(process(rs))
        } else {
          rs.close
          if (alt != null) {
            Some(alt())
          } else {
            None
          }
        }
      } finally {
        if (!rs.isClosed()) {
          rs.close
        }
      }
    }
  }

  class QueryRepeater[B](private val onCommitRepeatStates: List[String], onCommitRetryCounts: Int,
    connOpt: Option[Connection]) {

    def execute(func: (Connection) => B): B = {
      retry(onCommitRetryCounts - 1, func)
    }

    private def retry(count: Int, func: (Connection) => B): B @tailrec = {
      try {
        val completedResult = connOpt match {
          case Some(conn) => func(conn)
          case None => withConn { implicit conn =>
            func(conn)
          }
        }
        debug(s"Query success after ${onCommitRetryCounts - count}/onCommitRetryCounts trys")
        completedResult
      } catch {
        case e: SQLException if (onCommitRepeatStates.contains(e.getSQLState)) => {
          if (count == 0) {
            throw new SerialException(onCommitRetryCounts, e)
          }
          debug(s"Query retry ${onCommitRetryCounts - count}/onCommitRetryCounts times due to ${e.getSQLState} - ${e.getMessage}")
          connOpt.foreach(_.rollback)
          retry(count - 1, func)
        }
      }
    }
  }

}

trait PgLangMapper extends Logging {
  private val pglangMap = Map("da" -> "danish", "nl" -> "dutch",
    "en" -> "english",
    "fi" -> "finnish",
    "fr" -> "french",
    "de" -> "german",
    "hu" -> "hungarian",
    "it" -> "italian",
    "no" -> "norwegian",
    "pt" -> "portuguese",
    "ro" -> "romanian",
    "ru" -> "russian",
    "" -> "simple",
    "es" -> "spanish",
    "sv" -> "swedish",
    "tr" -> "turkish")

  protected def mapToPgLang(lang: String) = {
    pglangMap.getOrElse(lang, {
      warn("Unknown language " + lang + " using simple")
      pglangMap("")
    })
  }

}

class SerialException(count: Int, e: SQLException) extends Exception("Retried " + count + " times", e)

