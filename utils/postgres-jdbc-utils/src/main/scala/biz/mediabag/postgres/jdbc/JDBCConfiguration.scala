package biz.mediabag.postgres.jdbc

import java.io.File

import org.apache.commons.configuration2.PropertiesConfiguration
import com.jolbox.bonecp.BoneCPDataSource
import org.flywaydb.core.Flyway
import javax.sql.DataSource
import org.apache.commons.configuration2.builder.fluent.Configurations

import scala.collection.JavaConversions._

trait JDBCConfiguration {
  private var mConfig: Option[PropertiesConfiguration] = None

  private val configs = new Configurations
  // Read data from this file

  protected def config = {
    assert(configFilename != null, "configFilename may need early init")
    if (mConfig == null || mConfig.isEmpty) {
      val propertiesFile = new File(configFilename)
      val cfg = configs.properties(propertiesFile)
      Class.forName(cfg.getString("jdbc.class"))
      mConfig = Some(cfg)
    }
    mConfig.get
  }

  private var mDataSource: Option[BoneCPDataSource] = None

  protected def closeDataSource = {
    mDataSource.foreach(_.close)
  }

  protected def dataSource: DataSource = {
    if (mDataSource == null || mDataSource.isEmpty) {
      val ds = new BoneCPDataSource()
      ds.setJdbcUrl(config.getString("jdbc.url"))
      ds.setUsername(config.getString("jdbc.user"))
      ds.setPassword(config.getString("jdbc.pass"))
      ds.setMaxConnectionsPerPartition(config.getInt("jdbc.maxConnectionsPerPartition"))
      ds.setMinConnectionsPerPartition(config.getInt("jdbc.minConnectionsPerPartition"))
      ds.setPartitionCount(config.getInt("jdbc.partitionCount"))
      mDataSource = Some(ds)
    }
    mDataSource.get
  }

  protected def configFilename: String
}

trait FlywayConfiguration extends JDBCOps {
  protected def dataSource: DataSource

  if (createSchemas) {
    debug("Create schema " + getFlywaySchema)
    val schemaCreate = "CREATE SCHEMA IF NOT EXISTS %s";
    withConn { conn =>
      conn.createStatement.execute(schemaCreate.format(getFlywaySchema))
    }
  }

  private val flyway = new Flyway
  flyway.setDataSource(dataSource)
  flyway.setSchemas(getFlywaySchema)
  flyway.setLocations(getFlywayLocations: _*)
  flyway.setTable(getFlywayTable)
  flyway.setBaselineOnMigrate(true)
  flyway.setPlaceholders(Map("tablePrefix" -> getFlywayTablePrefix, "schemaName" -> getFlywaySchema))
  flyway.migrate

  protected def getFlywayLocations: List[String]
  protected def getFlywayTable: String
  protected def getFlywaySchema: String
  protected def getFlywayTablePrefix: String

  protected def createSchemas: Boolean
  protected def clearSchemas = flyway.clean

}