package biz.mediabag.postgres.jdbc
import java.sql.Connection
import eu.mediabag.commons.{TagId, DataType, WGS84Point}
import java.math.BigDecimal
import org.joda.time.DateTime
import java.sql.Types
import org.joda.time.Instant
import org.joda.time.LocalTime
import scala.collection.JavaConversions._
import grizzled.slf4j.Logging
import scala.collection.mutable.ListBuffer
import org.joda.time.DateTimeFieldType

object StatementWrapper extends HashUtils {
  val DataTypeMap: Map[Manifest[_ <: Any], String] = Map(manifest[Instant] -> "timestamp", manifest[DateTime] -> "timestamp", manifest[LocalTime] -> "time",
    manifest[Int] -> "bigint", Manifest.Int -> "bigint", manifest[Long] -> "bigint", manifest[java.lang.Long] -> "bigint", Manifest.Long -> "bigint", manifest[String] -> "varchar",
    manifest[WGS84Point] -> "geography", manifest[TagId] -> "ltree")

  def wrap(statementString: String, expectedColumns: List[Symbol])(implicit con: Connection) = {
    new StatementWrapper(statementString, expectedColumns, con)
  }

  def wrap(statementString: String)(implicit con: Connection) = {
    new StatementWrapper(statementString, Nil, con)
  }
}

class StatementWrapper(val statementString: String, expectedColumnList: List[Symbol], con: Connection) extends Logging {
  val ps = con.prepareStatement(statementString)
  private var map: Map[Symbol, String] = Map()
  private var batchCount = 0
  private var globalIndex = 1
  private var expectedColumns = new ListBuffer[Symbol]
  expectedColumns ++= expectedColumnList

  /**
   * This is a type helper method that handles the situation where parameters have been added to a collection so
   * the caller cannot call singleValue or arrayValue without further introspection.
   * The preference is to call the actual method that distinguish between single values and collections
   */
  def singleOrArrayValue[T <: Any](column: Symbol, value: T)(implicit mani: Manifest[T]) = {
    if (classOf[Iterable[T]].isAssignableFrom(value.getClass())) {
      arrayValue(column, value.asInstanceOf[Iterable[T]].toList)
    } else if (classOf[java.util.Collection[T]].isAssignableFrom(value.getClass())) {
      arrayValue(column, value.asInstanceOf[java.util.Collection[T]].toList)
    } else {
      singleValue(column, value)
    }
  }

  def singleValue[T <: Any](column: Symbol, value: T)(implicit mani: Manifest[T]) = {
    getIndiciesOfColumn(column).foreach { index ⇒
        setStmtValue(column, value, index)
    }
  }

  /**
   * Set a single value by simply incrementing the last index used
   */
  def singleValue[T <: Any](value: T)(implicit mani: Manifest[T]) = {
    val sym = Symbol("index" + globalIndex)
    expectedColumns += sym
    setStmtValue(sym, value, globalIndex)
    globalIndex += 1
  }

  def toDebugSQL = {
    replace(statementString, 0)
  }

  def setStmtValue[T <: Any](column: Symbol, value: T, index:Int)(implicit mani: Manifest[T]) = {
    try {
      if (mani <:< manifest[String]) {
        map = map + (column -> ("'" + value.asInstanceOf[String] + "'"))
        ps.setString(index, value.asInstanceOf[String])
      } else if (mani <:< manifest[TagId]) {
        map = map + (column -> ("'" + value.asInstanceOf[TagId] + "'"))
        ps.setObject(index, value.asInstanceOf[TagId].getId, Types.OTHER)
      } else if (mani <:< Manifest.Int || mani <:< manifest[java.lang.Integer]) {
        map = map + (column -> value.toString())
        ps.setInt(index, value.asInstanceOf[Int])
      } else if (mani <:< Manifest.Boolean || mani <:< manifest[Boolean]) {
        map = map + (column -> value.toString())
        ps.setBoolean(index, value.asInstanceOf[Boolean])
      } else if (mani <:< Manifest.Long || mani <:< manifest[Long]) {
        map = map + (column -> value.toString())
        ps.setLong(index, value.asInstanceOf[Long])
      } else if (mani <:< manifest[java.math.BigDecimal]) {
        map = map + (column -> value.toString())
        ps.setBigDecimal(index, value.asInstanceOf[BigDecimal])
      } else if (mani <:< Manifest.Double || mani <:< manifest[java.lang.Double]) {
        map = map + (column -> value.toString())
        ps.setDouble(index, value.asInstanceOf[Double])
      } else if (mani <:< manifest[Long] || mani <:< manifest[java.lang.Long]) {
        map = map + (column -> value.toString())
        ps.setLong(index, value.asInstanceOf[Long])
      } else if (mani <:< manifest[org.joda.time.DateTime]) {
        val date = value.asInstanceOf[DateTime]
        map = map + (column -> date.toString())
        ps.setDate(index, new java.sql.Date(date.getMillis))
      } else if (mani <:< manifest[org.joda.time.LocalTime]) {
        val time = value.asInstanceOf[LocalTime]
        map = map + (column -> time.toString())
        ps.setTime(index, new java.sql.Time(time.get(DateTimeFieldType.hourOfDay), time.get(DateTimeFieldType.minuteOfHour), time.get(DateTimeFieldType.secondOfMinute)))
      } else if (mani <:< manifest[org.joda.time.Instant]) {
        val date = value.asInstanceOf[Instant]
        map = map + (column -> date.toString())
        ps.setTimestamp(index, new java.sql.Timestamp(date.getMillis))
      } else if (mani equals manifest[WGS84Point]) {
        val point = makePoint(value.asInstanceOf[WGS84Point])
        map = map + (column -> ("'" + point + "'"))
        ps.setObject(index, point)
      } else {
        throw new UnsupportedOperationException("Not defined for " + mani.toString)
      }
    } catch {
      case e: Exception ⇒ throw new IllegalArgumentException("Failed to set " + column.name + " to value " + value, e)
    }
  }
  private def replace(string: String, index: Int): String = {
    if (index == expectedColumns.size) {
      string
    } else {
      replace(string.replaceFirst("\\?", map(expectedColumns(index))), index + 1)
    }
  }

  private def makePoint(point: WGS84Point) = {
    "POINT (" + point.getLongitude().toString() + " " + point.getLatitude().toString() + ")";
  }

  def objectValue[T](column: Symbol, value: T) = {
    map = map + (column -> value.toString)
    getIndiciesOfColumn(column).foreach { index ⇒ ps.setObject(index, value, Types.OTHER) }
  }

  def arrayValue[T](column: Symbol, array: Iterable[T])(implicit mani: Manifest[T]) = {
    StatementWrapper.DataTypeMap.get(mani) match {
      case Some(dbType) ⇒ {
        map = map + (column -> ("{" + array.mkString(",") + "}"))
        val typedArray: Array[Any] = if (mani <:< manifest[org.joda.time.DateTime]) {
          array.asInstanceOf[Iterable[DateTime]].map { dt ⇒
            new java.sql.Date(dt.getMillis())
          }.toArray
        } else if (mani <:< manifest[org.joda.time.Instant]) {
          array.asInstanceOf[Iterable[Instant]].map { dt ⇒
            new java.sql.Timestamp(dt.getMillis())
          }.toArray
        } else if (mani <:< manifest[LocalTime]) {
          array.asInstanceOf[Iterable[LocalTime]].map { time ⇒
            new java.sql.Time(time.get(DateTimeFieldType.hourOfDay), time.get(DateTimeFieldType.minuteOfHour), time.get(DateTimeFieldType.secondOfMinute))
          }.toArray
        } else if (mani equals manifest[WGS84Point]) {
          array.asInstanceOf[Iterable[WGS84Point]].map { point ⇒
             makePoint(point.asInstanceOf[WGS84Point])
          }.toArray
        } else {
          array.toArray
        }
        val a = con.createArrayOf(dbType, typedArray.asInstanceOf[Array[java.lang.Object]])
        getIndiciesOfColumn(column).foreach { index ⇒ ps.setArray(index, a) }
      }
      case None ⇒ throw new UnsupportedOperationException("Not defined for " + mani.toString)
    }
  }

  def addBatch = {
    ps.addBatch()
    batchCount += 1
    if (batchCount == 1000) {
      debug("Batch count exceeds threshold, executing batch")
      executeBatch
    }
  }

  def executeBatch = {
    ps.executeBatch()
    batchCount = 0
  }
  def executeQuery = {
    ps.executeQuery()
  }

  def executeUpdate = {
    ps.executeUpdate()
  }

  def close = {
    ps.close
  }
  private def getIndiciesOfColumn(column: Symbol) = {
    val zip = expectedColumns.zipWithIndex
    val indicies = zip.filter { case (sym, index) ⇒ sym == column }.map { case (sym, index) ⇒ index + 1 }
    if (indicies.size == 0) throw new IllegalArgumentException(column + " not defined in the expected column list")
    indicies
  }
}