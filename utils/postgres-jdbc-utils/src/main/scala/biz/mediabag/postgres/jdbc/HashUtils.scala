package biz.mediabag.postgres.jdbc

import xbird.util.hashes.JenkinsHash

trait HashUtils {

  private val HashIntVal = 0x02e117d9
  
  def generateHash(str: String): Long = {
    JenkinsHash.hash64(str.getBytes, HashIntVal)
  }
}