namespace java biz.mediabag.thriftcommons

enum Multiplicity {
	SIMPLE,	BAG, ALT, SEQ, NONE
}

enum DataType{
    UUID, STRING, DATETIME, DATE, DECIMAL, BOOLEAN, ANYURI, INTEGER, 
    DURATION, TIME, LITERAL_LANG, ASSET, TAG, WGS84_POINT, WGS84_CIRCLE, WGS84_SHAPE, QUERY
}

enum Usage {
	REQUIRED, OPTIONAL, NOTALLOWED
}

enum Domain {
	USER, SYSTEM
}

enum Access {
    READWRITE, READ, NONE, EXECUTE
}

struct LangLiteral {
	1: required string literal;
	2: optional string lang;
}

struct PropertyValueElement {
	1: required string value;
    2: optional string lang;
    3: required DataType type;
}

struct CompositeValue {
	1: required set<PropertyValueElement> values;
	2: optional Multiplicity multiplicity = Multiplicity.SIMPLE;
}

struct CompositeValueWithValidation {
	1: required CompositeValue value;
	2: required string updateTime;
	3: required binary updateTimeHash;
}

struct Document {
	1: required map<string, CompositeValueWithValidation> properties;
	2: required string modelURI;
	3: optional list<string> removedProperties;
}

struct WGS84Point {
	1: required double latitude;
	2: required double longitude;
}

struct WGS84Circle {
	1: required WGS84Point centre;
	2: required double radius;
}

enum RestrictionType {
	ListOfValues, Tag, Range, WithinCircle, WithinShape
}

struct Restriction {
	1: required RestrictionType restrictionType;
	2: optional string upper;
	3: optional string lower;
	4: optional list<string>  listOfValues;
	5: optional string tagPattern;
	6: optional list<WGS84Point> shape;
	7: optional WGS84Circle circle;
}

struct Metadata {
	1: required string id;
	2: required Usage usage;
    3: required Domain domain;
	4: required DataType type;
	5: required Multiplicity multiplicity;
	6: required Access access = Access.READWRITE;
	7: optional Restriction restriction;
	8: optional LangLiteral description;
}

struct MetadataGroup {
	1: required Multiplicity collectionType = Multiplicity.BAG;
	2: required list<Metadata> metadataItems;
}

struct DocumentModel {
	1: required string uri;
	2: required LangLiteral description;
	3: required list<MetadataGroup> metadataGroups;	
}