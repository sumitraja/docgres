package biz.mediabag.tagstore;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.mediabag.commons.LangLiteral;
import eu.mediabag.commons.Tag;

public class TagManagerTest {
    
    @Test
    public void testTagHierarchy() {
        Tag parent = new Tag("1", new LangLiteral("1"), null);
        Tag child2 = new Tag("2", new LangLiteral("2"), parent);
        Tag child3 = new Tag("3", new LangLiteral("3"), child2);
        Tag child4 = new Tag("4", new LangLiteral("4"), child3);
        assertEquals("1.2.3.4", child4.getAbsoluteId());
        assertEquals("1.2", child2.getAbsoluteId());
        assertEquals("1", parent.getAbsoluteId());
        
    }

}
