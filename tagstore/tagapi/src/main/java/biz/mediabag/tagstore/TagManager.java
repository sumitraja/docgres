package biz.mediabag.tagstore;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import eu.mediabag.commons.Tag;


public interface TagManager {

    public Tag getTagById(String id);

    /**
     * Search for tags
     * 
     * @param title
     * @param lang
     * @return List of tags ordered by relevance
     */
    public List<Tag> getTagsByTitle(String title, String lang);

    public void addTags(Set<Tag> tag);

    /**
     * Search for tags with ids that match a pattern
     * @param handlerConfig
     * @return A collection of matching tags or an empty collection if none were found
     */
    public Collection<Tag> getTagsWithIdMatchingPattern(String pattern);

}
