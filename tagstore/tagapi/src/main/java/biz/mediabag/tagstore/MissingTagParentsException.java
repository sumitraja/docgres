package biz.mediabag.tagstore;

import java.util.Collection;

public class MissingTagParentsException extends RuntimeException {

    private static final long serialVersionUID = 2012034904844962929L;
    private final Collection<String> parentLangMissing;

    public Collection<String> getParentLangMissing() {
        return parentLangMissing;
    }

    public MissingTagParentsException(Collection<String> parentLangMissing) {
        super("Parents missing: " + parentLangMissing.toString());
        this.parentLangMissing = parentLangMissing;
    }

}
