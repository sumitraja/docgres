package biz.mediabag.documentapi.thrift

import biz.mediabag.documentapi.Document
import biz.mediabag.thriftcommons.{
  Document => TDocument,
  CompositeValue => TCompositeValue,
  PropertyValueElement => TPropertyValueElement,
  DataType => TDataType,
  CompositeValueWithValidation => TCompositeValueWithValidation,
  Multiplicity => TMultiplicity
}
import eu.mediabag.commons.document.CompositeValue
import eu.mediabag.commons.document.PropertyValueElement
import scala.collection.JavaConversions._
import eu.mediabag.commons.Multiplicity
import eu.mediabag.commons.DataType
import biz.mediabag.documentapi.DocumentRepository
import biz.mediabag.documentapi._
import java.net.URI
import biz.mediabag.documentapi.impl._
import org.joda.time.Instant
import ove.crypto.digest.Blake2b
import ove.crypto.digest.Blake2b.Engine
import java.nio.ByteBuffer

trait ThriftDocument[T <: Metadata, U <: MetadataGroup[T]] { self: DocumentLike[T, U] =>

  def hashEngine: Blake2b

  def toThrift = {

    val toRemove = cachedProperties.collect {
      case (uri, (Remove(cv), instant, User())) => uri
    }
    val tprops = properties ++ cachedProperties.collect {
      case (uri, (Change(cv), instant, User())) => uri -> (Change(cv), instant)
    } map {
      case (uri, (pa: PropertyAction, instant)) => {
        val (updateTime, timeHash) = genHash(instant)
        uri.toString -> ThriftCompositeValueWithValidation(pa.cv, updateTime, timeHash)
      }
    }
    TDocument(tprops, model.getURI.toString)
  }

  def updateFromThrift(td: TDocument) = {
    if (td.modelURI != model.getURI) {
      throw new DocumentException("Model " + td.modelURI + " does not match current model " + model.getURI)
    }

    td.properties.foreach {
      case (k, v) =>
        v match {
          case ThriftCompositeValueWithValidation(cv, time, timeHash) => {
            validateTimeWithHash(time, timeHash)
            val property = new URI(k)
            updatePropertyAndInstant(model.getProperty(property), cv, ISODataTypeConvertor.convertModifiedTimestamp(time).toInstant)
          }
        }
    }

  }
  
  private def updatePropertyAndInstant(pdef: T, cv: CompositeValue, time:Instant) = {
    val change = pdef.id -> ((Change(cv), time, User()))
    cachedProperties = cachedProperties + change
  }

  private def validateTimeWithHash(time: String, timeHash: Array[Byte]) = {
    hashEngine.reset
    val checkTimeHash = hashEngine.digest(time.getBytes)
    if (checkTimeHash != timeHash) {
      throw new DocumentException("Time was tampered with, its hash doesn't match")
    }
  }
  private def genHash(instant: Instant) = {
    val str = ISODataTypeConvertor.convertModifiedTimestamp(instant.toDateTime)
    hashEngine.reset
    val timeHash = hashEngine.digest(str.getBytes)
    (str, timeHash)
  }

}

object ThriftCompositeValueWithValidation {

  def apply(cv: CompositeValue, updateTime: String, timeHash: Array[Byte]) = {
    val tcv = ThriftCompositeValue(cv)
    TCompositeValueWithValidation(tcv, updateTime, ByteBuffer.wrap(timeHash))
  }

  def unapply(tcv: TCompositeValueWithValidation) = {
    val cv = tcv._3 match {
      case ThriftCompositeValue(cv) => cv
    }
    Some((cv, tcv.updateTime, tcv.updateTimeHash.array))
  }
}

object ThriftCompositeValue {
  def apply(cv: CompositeValue) = {
    val pves = cv.getValues.map { pve => ThriftPropertyValueElement(pve) }
    val multi = TMultiplicity.valueOf(cv.getMultiplicity.name) match {
      case Some(m) => m
      case None => throw new IllegalArgumentException("Unknown multiplicity " + cv.getMultiplicity.name)
    }
    TCompositeValue(pves.toSet, multi)
  }

  def unapply(tcv: TCompositeValue) = {
    Some(new CompositeValue(Multiplicity.valueOf(tcv._2.name.toUpperCase), tcv._1.map {
      case ThriftPropertyValueElement(pve) => pve
    }.toSeq: _*))
  }
}

object ThriftPropertyValueElement {
  def apply(pve: PropertyValueElement) = {
    val dt = TDataType.valueOf(pve.getType.name) match {
      case Some(dt) => dt
      case None => throw new IllegalArgumentException("Unknown datatype " + pve.getType.name)
    }
    TPropertyValueElement(pve.getValue, if (pve.getLang == null) None else Some(pve.getLang), dt)
  }

  def unapply(tpve: TPropertyValueElement): Option[PropertyValueElement] = {
    Some(new PropertyValueElement(tpve._1, tpve._2.getOrElse(null), DataType.valueOf(tpve._3.name.toUpperCase())))
  }
}