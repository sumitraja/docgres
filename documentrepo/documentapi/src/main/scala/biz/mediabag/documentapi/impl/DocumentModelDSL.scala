package biz.mediabag.documentapi.impl

import eu.mediabag.commons.LangLiteral
import scala.collection.JavaConversions._
import java.net.URI
import biz.mediabag.documentapi.DocumentModel
import eu.mediabag.commons.Multiplicity
import eu.mediabag.commons.Usage
import eu.mediabag.commons.DataType
import biz.mediabag.tagstore.TagManager
import biz.mediabag.documentapi.MetadataGroup
import biz.mediabag.documentapi.Metadata
import biz.mediabag.documentapi.TagRestriction
import biz.mediabag.documentapi.Restriction
import biz.mediabag.documentapi.ListOfValuesRestriction

object DocumentModelDSL {
  implicit def toLangLiteralWrapper(str: String): LangLiteralWrapper = {
    new LangLiteralWrapper(str)
  }

  implicit def toLangLiteralWrapper(ll: LangLiteral): LangLiteralWrapper = {
    new LangLiteralWrapper(ll)
  }

  implicit def toURI(str: String) = new URI(str)
}

trait DocumentModelDSL[M <: Metadata, MD <: MetadataGroup[M]] {

  def restriction(values: Map[String, LangLiteral]) = {
    val tagList = new ListOfValuesRestriction
    tagList.setValues(values)
    tagList
  }

  def createMetadataGroup(multi: Multiplicity): MD

  class DocumentModelWrapper(val documentModel: DocumentModelLike[M, MD]) {

    def propertyGroup(multi: Multiplicity)(f: (MetadataGroupWrapper) => Unit) = {
      val metadataGroup = createMetadataGroup(multi)
      val groupWrapper = new MetadataGroupWrapper(documentModel, metadataGroup)
      f(groupWrapper)
      documentModel.addPropertyGroup(metadataGroup)
      metadataGroup
    }

    def propertyGroup(group: MD) = {
      documentModel.addPropertyGroup(group)
      DocumentModelWrapper.this
    }
  }
  
  trait DocumentModelWrapperLike {
    val documentModel: DocumentModelLike[M, MD]
    
    def restriction(tagPattern: String)(implicit tagManager: TagManager) = {
      new TagRestriction(tagPattern, tagManager)
    }

    def addAll(group: MD) = {
      documentModel.addPropertyGroup(group)
      this
    }

    def addAll(groups: Iterable[MD]) = {
      groups.foreach { grp =>
        documentModel.addPropertyGroup(grp)
      }
      this
    }

  }

  class MetadataGroupWrapper(am: DocumentModelLike[M, MD], propertyDefGroup: MD) {

    def metadata(uri: String, desc: LangLiteralWrapper, dataType: DataType, multi: Multiplicity = Multiplicity.SIMPLE,
      usage: Usage = Usage.OPTIONAL, restriction: Restriction = null) = {

      val propertyDef = createMetadataItem(new URI(uri), dataType, multi, usage)
      propertyDef.setDescription(desc.ll)
      if (restriction != null) {
        propertyDef.setRestriction(restriction)
      }

      propertyDefGroup.addMetadataItem(propertyDef)
      propertyDef
    }

  }

  def createMetadataItem(uri: URI, datatype: DataType, multi: Multiplicity, usage:Usage): M

}

class LangLiteralWrapper(val literal: String, val ll: LangLiteral = new LangLiteral) {
  def this(ll: LangLiteral = new LangLiteral) = this(null, ll)
  def `@`(lang: String) = {
    ll.addLiteral(literal, lang)
    ll
  }

  def +(inll: LangLiteral) = {
    val map = inll.getLangToLiteralMap() ++ (ll.getLangToLiteralMap())
    new LangLiteral(map)
  }
}
