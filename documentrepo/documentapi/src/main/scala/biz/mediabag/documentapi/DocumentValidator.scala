package biz.mediabag.documentapi

import java.net.URI
import java.util.Collection
import java.util.HashMap
import java.util.HashSet
import java.util.Iterator
import java.util.Map

import scala.collection.JavaConversions._

import eu.mediabag.commons.DataType
import eu.mediabag.commons.DataTypeConvertor
import eu.mediabag.commons.Multiplicity
import eu.mediabag.commons.State
import eu.mediabag.commons.Usage
import eu.mediabag.commons.document.CompositeValue
import eu.mediabag.commons.document.PropertyValidationFailedReason
import eu.mediabag.commons.document.PropertyValueElement

abstract class DocumentValidator[T <: Document, M <: Metadata, MD <: MetadataGroup[M]](protected val convertor: DataTypeConvertor) {

  protected val reasons = new HashMap[URI, Collection[PropertyValidationFailedReason]]()

  private var encounteredGroups: Collection[MD] = new HashSet()

  protected[this] var unsetProperties: HashMap[URI, State] = _

  def validate(document: T, documentModel: DocumentModel[M, MD], ignoreProperties: Collection[URI]): Map[URI, Collection[PropertyValidationFailedReason]] = {
    if (documentModel.getURI != document.getModel.getURI) {
      throw new ValidatorException("Asset is model " + documentModel.getURI + " but validating against " + document.getModel.getURI)
    }
    if (unsetProperties == null) {
      unsetProperties = new HashMap[URI, State](getUnsavedProperties(document, ignoreProperties))
    }
    checkSetProperties(document, documentModel, unsetProperties)
    checkRequiredProperties(document, documentModel, unsetProperties)
    reasons
  }

  protected def getUnsavedProperties(document: T, ignoreProperties: Collection[URI]): Map[URI, State]

  private def checkRequiredProperties(document: T, documentModel: DocumentModel[M, MD], unsetProperties: Map[URI, State]) {
    val requiredProps = documentModel.getRequiredProperties
    val requiredGroups = new HashSet[MD]()
    for (pdef <- requiredProps) {
      val state = unsetProperties.get(pdef.id)
      val group = documentModel.getPropertyGroupForPropertyDef(pdef.id)
      if (group.getCollectionType == Multiplicity.ALT) {
        if (encounteredGroups.contains(group)) {
          //continue
        } else {
          requiredGroups.add(group)
        }
      } else {
        val unset = document.getProperty(pdef.id)
        if (pdef.getUsage == Usage.REQUIRED && (unset == null || state == State.REMOVED)) {
          addReason(pdef.id, PropertyValidationFailedReason.PROPERTY_REQUIRED)
        }
      }
    }
    if (requiredGroups.size > 0) {
      for (group <- requiredGroups) {
        var found = false
        var itr = group.getMetadataItems.iterator()
        while (itr.hasNext && !found) {
          val pdef = itr.next()
          val unset = document.getProperty(pdef.id)
          found = unset != null
        }
        if (!found) {
          for (pdef <- group.getMetadataItems) {
            addReason(pdef.id, PropertyValidationFailedReason.PROPERTY_REQUIRED_ALT)
          }
        }
      }
    }
  }

  private def checkSetProperties(document: T, documentModel: DocumentModel[M, MD], unsetProperties: Map[URI, State]) {
    for ((key, value) <- unsetProperties) {
      val `def` = documentModel.getProperty(key)
      if (`def` == null) {
        if (!documentModel.isFreeForm) {
          addReason(key, PropertyValidationFailedReason.PROPERTY_PROHIBITED)
        }
      } else {
        val value = document.getProperty(`def`.id)
        if (value != null) {
          if (`def`.getUsage == Usage.NOTALLOWED) {
            addReason(key, PropertyValidationFailedReason.PROPERTY_ACCESS_NOT_ALLOWED)
          } else {
            validatePropertyValue(value, `def`)
            validatePropertyGroup(`def`, documentModel)
          }
        }
      }
    }
  }

  private def validatePropertyGroup(pdef: M, documentModel: DocumentModel[M, MD]) {
    val group = documentModel.getPropertyGroupForPropertyDef(pdef.id)
    if (group != null && !validGroup(pdef, group)) {
      addReason(pdef.id, PropertyValidationFailedReason.PROPERTY_GROUP_ALT)
    }
  }

  private def validGroup(pdef: M, group: MD): Boolean = {
    if (group.getCollectionType == Multiplicity.ALT) {
      if (encounteredGroups.contains(group)) {
        return false
      }
      encounteredGroups.add(group)
    }
    true
  }

  def validatePropertyValue(value: CompositeValue, pdef: M) {
    checkMultiplicity(pdef, value)
    checkPropertyDataType(pdef, value)
    checkRestrictionList(pdef, value)
  }

  private def checkRestrictionList(pdef: M, value: CompositeValue) {
    val restriction = pdef.getRestriction
    if (restriction != null) {
      restriction match {
        case rest:ListOfValuesRestriction =>
        case rest:TagRestriction =>
        case rest:RangeRestriction =>
      }
      for (pvalue <- value.getValues if !restriction.isValid(pvalue.getValue)) {
        addReason(pdef.id, PropertyValidationFailedReason.PROPERTY_RESTRICTION_LIST)
      }
    }
  }

  private def checkPropertyDataType(pdef: M, value: CompositeValue) {
    val definedType = pdef.getType
    for (elem <- value.getValues) {
      if (elem.getType != definedType && !(elem.getType == DataType.TAG && pdef.getRestriction != null)) {
        addReason(pdef.id, PropertyValidationFailedReason.getErrorForDataType(definedType))
        //break
      }
      val validation = validatePropertyType(definedType, elem.getValue)
      if (validation != null) {
        addReason(pdef.id, PropertyValidationFailedReason.getErrorForDataType(definedType))
      }
    }
  }

  private def checkMultiplicity(pdef: M, value: CompositeValue) {
    if (pdef.getMultiplicity != value.getMultiplicity) {
      addReason(pdef.id, PropertyValidationFailedReason.getErrorForMultiplicity(pdef.getMultiplicity))
    } else if (pdef.getMultiplicity == Multiplicity.SIMPLE && value.getValues.size > 1) {
      addReason(pdef.id, PropertyValidationFailedReason.PROPERTY_MULTIPLICITY_SIMPLE)
    }
  }

  protected def addReason(uri: URI, failedReason: PropertyValidationFailedReason) {
    var reasonList = reasons.get(uri)
    if (reasonList == null) {
      reasonList = new HashSet[PropertyValidationFailedReason]()
      reasons.put(uri, reasonList)
    }
    reasonList.add(failedReason)
  }

  private def validatePropertyType(`type`: DataType, propVal: String): PropertyValidationFailedReason = {
    if (!convertor.validateDataType(`type`, propVal)) {
      PropertyValidationFailedReason.getErrorForDataType(`type`)
    } else {
      null
    }
  }
}