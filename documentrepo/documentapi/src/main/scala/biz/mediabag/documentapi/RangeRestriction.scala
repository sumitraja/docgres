package biz.mediabag.documentapi

import eu.mediabag.commons.DataType
import eu.mediabag.commons.DataType._
import eu.mediabag.commons.DataTypeConvertor

class RangeRestriction(dataType: DataType, convertor: DataTypeConvertor) extends Restriction {

  private val expected = List(DataType.DATE, DataType.DATETIME, DataType.DECIMAL, DataType.DURATION, DataType.INTEGER, DataType.TIME)
  if (!expected.contains(dataType)) {
    throw new IllegalArgumentException("Cannot use " + dataType.name
      + " in range restricitions. Expected one of " + expected.mkString(","))
  }

  private var ranges: List[Range] = List()

  def addRange(upper: String, lower: String) {
    ranges ::= new Range(upper, lower)
  }

  def isValid(value: String): Boolean = ranges.exists(range => range.isIn(value))

  class Range(val upper: String, val lower: String) {

    val upperConv = convert(upper)
    val lowerConv = convert(lower)

    def isIn(value: String) = {
      val valueConv = convert(value)
      valueConv.compareTo(upperConv) <= 0 && valueConv.compareTo(lowerConv) >= 0
    }

    def convert(str: String): Comparable[Any] = dataType match {
      case DATETIME => convertor.convertDateTime(str).asInstanceOf[Comparable[Any]]
      case DATE => convertor.convertDate(str).asInstanceOf[Comparable[Any]]
      case DURATION => convertor.convertDuration(str).asInstanceOf[Comparable[Any]]
      case INTEGER => Integer.parseInt(str).asInstanceOf[Comparable[Any]]
      case DECIMAL => str.toFloat.asInstanceOf[Comparable[Any]]
      case _ => throw new IllegalArgumentException("Cannot use " + dataType.name
        + " in range restricitions. Expected one of " + expected.mkString(","))
    }
  }
}