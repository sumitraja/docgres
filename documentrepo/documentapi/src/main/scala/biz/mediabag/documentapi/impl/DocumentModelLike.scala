package biz.mediabag.documentapi.impl

import java.net.URI
import java.util.Collection
import scala.beans.BeanProperty
import scala.beans.BooleanBeanProperty
import scala.collection.JavaConversions._
import eu.mediabag.commons.LangLiteral
import eu.mediabag.commons.Usage
import eu.mediabag.commons.document.PropertyValidationFailedReason
import eu.mediabag.commons.State
import scala.annotation.meta.beanGetter
import biz.mediabag.documentapi.Document
import biz.mediabag.documentapi.DocumentModel
import biz.mediabag.documentapi.MetadataGroup
import biz.mediabag.documentapi.Metadata
import scala.collection.immutable.TreeSet

trait DocumentModelLike[M <: Metadata, MD <: MetadataGroup[M]] { self:DocumentModel[M, MD] =>

  val ModifiedTimestampURI = new URI("http://mediabag.biz/document/modifiedTimestamp")
  val CreateDateURI = new URI("http://purl.org/dc/terms/created")
  val ModifiedDateURI = new URI("http://purl.org/dc/terms/modified")
  
  val uri: URI
  val description: LangLiteral
  val freeForm: Boolean

  override def getModifiedDateProperty = ModifiedDateURI

  override def getCreateDateProperty = CreateDateURI

  override def getModifiedTimestampProperty = ModifiedTimestampURI
  
  protected var metadataGroups = List[MD]()

  protected var allMetadata = Map[URI, M]()

  protected var requiredMetadata = Set[M]()

  protected var displayProps = List[M]()

  def getDescription = description

  def getURI = uri

  def isFreeForm = freeForm

  def getPropertyGroups: java.util.List[MD] = {
    metadataGroups
  }

  def addPropertyGroup(grp: MD) {
    metadataGroups ::= grp
    allMetadata ++= grp.getMetadataItems.map { item =>
      item.id -> item
    }
  }

  override def toString(): String = {
    "DocumentModel[uri=" + getURI + ", MetadataGroups=" + getPropertyGroups + "]"
  }

  def getProperty(name: String): M = getProperty(new URI(name))

  def getProperty(name: URI): M = allMetadata.getOrElse(name, null.asInstanceOf[M])

  def getPropertyGroupForPropertyDef(key: URI): MD = {
    val groups = for (group <- this.metadataGroups
        if group.getMetadataItems.exists(_.id == key)) yield group
    groups match {
      case head::tail => head
      case Nil => null.asInstanceOf[MD]
    }
  }

  def getDisplayProperties(): java.util.Collection[M] = {
    displayProps
  }

  def getAllProperties(): java.util.Map[URI, M] = allMetadata

  def getRequiredProperties() = {
    if (requiredMetadata.size == 0) {
      for (md <- allMetadata.values if md.getUsage == Usage.REQUIRED) {
        requiredMetadata += md
      }
    }
    asJavaCollection(requiredMetadata)
  }

  def displayProperties(uris : List[URI]) = {
    displayProps = allMetadata.filterKeys(k => uris.contains(k)).values.toList
  }


}