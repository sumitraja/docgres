package biz.mediabag.documentapi.impl

import java.net.URI
import org.joda.time.Instant
import scala.collection.JavaConversions._
import eu.mediabag.commons.document.CompositeValue
import biz.mediabag.documentapi.Document
import biz.mediabag.documentapi.DocumentModel
import scala.beans.BeanProperty
import eu.mediabag.commons.document.CompositeValue
import biz.mediabag.documentapi.ValidatorException
import biz.mediabag.documentapi.DocumentValidator
import java.util.Collection
import eu.mediabag.commons.document.PropertyValidationFailedReason
import biz.mediabag.documentapi.MetadataGroup
import biz.mediabag.documentapi.Metadata
import eu.mediabag.commons.DateFormats._

object ISODataTypeConvertor extends eu.mediabag.commons.DataTypeConvertor(ISO8601_FULL_FORMAT_STRING_MS, ISO8601_FULL_FORMAT_STRING,
  ISO8601_DATE_FORMAT_STRING, ISO8601_TIME_FORMAT_STRING)
  
trait DocumentLike[T <: Metadata, U <: MetadataGroup[T]] { self: Document =>
   
  val properties: Map[URI, (Unchanged, Instant)]
  val model: DocumentModel[T, U]

  protected var cachedProperties: Map[URI, (PropertyAction, Instant, Domain)] = Map()
  protected lazy val modified = new Instant()

  protected def existingTimestampInstant = modified

  protected def existingModifiedInstant = modified

  protected def getProperty(pdef: T) = {
    if (pdef != null) {
      val value = cachedProperties.get(pdef.id) match {
        case Some((Remove(cv), instant, User())) => None
        case Some((Change(cv), instant, User())) => Some(cv)
        case _ => None
      }
      value match {
        case Some(cv) => cv
        case None => {
          properties.get(pdef.id) match {
            case Some((Unchanged(cv), instant)) => cv
            case _ => null
          }
        }
      }
    } else {
      null
    }
  }

  protected def getProperties: java.util.Map[URI, CompositeValue] = {
    var ret = properties.map {
      case (uri, (Unchanged(cv), instant)) =>
        (uri -> cv)
    }.toMap
    val toRemove = cachedProperties.collect {
      case (uri, (Remove(cv), instant, User())) => uri
    }
    ret = ret -- toRemove
    ret ++: cachedProperties.collect {
      case (uri, (Change(cv), instant, User())) => uri -> cv
    }
    ret
  }

  protected def getPropertyURIs: java.util.Collection[URI] = getProperties.keys

  protected def removeProperty(pdef: T) = {
    if (pdef != null) {
      properties.get(pdef.id) match {
        case Some((Unchanged(cv), instant)) => {
          cachedProperties = cachedProperties + (pdef.id -> (Remove(cv), instant, User()))
          Some(cv)
        }
        case _ => None
      }
    } else {
      None
    }

  }

  protected def setProperty(pdef: T, cv: CompositeValue) = {
    if (pdef != null) {
      val property = properties.get(pdef.id)
      property match {
        case Some((propAction, instant)) if propAction.cv != cv => {
          val change = pdef.id -> (Change(cv), instant, User())
          cachedProperties = cachedProperties + change
          change
        }
        case Some((propAction, instant)) => {
          val change = pdef.id -> (Unchanged(cv), instant, User())
          change
        }
        case None => {
          val change = pdef.id -> (Change(cv), new Instant, User())
          cachedProperties = cachedProperties + change
          change
        }
      }
    }
  }
  
  protected def validate:java.util.Map[URI, Collection[PropertyValidationFailedReason]]

}

sealed abstract class Domain
case class System() extends Domain
case class User() extends Domain

sealed abstract class PropertyAction(val cv: CompositeValue)

case class Remove(override val cv: CompositeValue) extends PropertyAction(cv)

case class Unchanged(override val cv: CompositeValue) extends PropertyAction(cv)

//case class Add(val cv:CompositeValue) extends PropertyAction
case class Change(val newValue: CompositeValue) extends PropertyAction(newValue)


