package biz.mediabag.documentapi.query;

import java.net.URI;
import java.util.List;
import java.util.Map;

import biz.mediabag.documentapi.Document;

public interface DocumentResultList<DOC extends Document> {

    /**
     * @return Maximum results from this query. Could be an artificial max set
     *         by repository. Returns without blocking and could throw
     *         QueryException if query is invalid
     */
    public int getNumberOfResults();

    /**
     * @return Number (@see {@link DocumentResultList<U extends MetadataDOC extends Document<U,V>>#setNumber(int)}) asset
     *         results from start (@see {@link DocumentResultList<U extends MetadataDOC extends Document<U,V>>#setStart(int)}).
     *         Will block while query is run and will throw QueryException if
     *         the query is invalid
     */

    public List<DocumentResult<DOC>> getResults();

    public Map<URI, List<DocumentResult<DOC>>> getDocumentsGroupedByProperties(List<URI> properties);

    public Map<URI, List<DocumentResult<DOC>>> getDocumentsGroupedByModel();

    public void setStart(int start);

    public void setNumber(int number);
}
