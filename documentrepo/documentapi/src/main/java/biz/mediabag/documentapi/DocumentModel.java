package biz.mediabag.documentapi;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import eu.mediabag.commons.LangLiteral;

public interface DocumentModel<U extends Metadata, T extends MetadataGroup<U>> {
    
    public static String MODEL_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

	public abstract URI getURI();

	public abstract LangLiteral getDescription();

	public abstract List<T> getPropertyGroups();

	public abstract U getProperty(String name)
			throws URISyntaxException;

	public abstract U getProperty(URI name);

	public abstract boolean isFreeForm();

	public abstract T getPropertyGroupForPropertyDef(URI key);

	public abstract Collection<U> getDisplayProperties();

	/**
	 * @return All the properties defined in the model, even the properties that define the
	 *         essence, modified date, created date etc. Callers are expected to filter this
	 *         out.
	 */
	public abstract Map<URI, U> getAllProperties();

	public abstract Collection<U> getRequiredProperties();
	
    /**
     * Gets the URI for the create DateTime property that is used when creating
     * a document
     * 
     * @return
     */
    public URI getCreateDateProperty();

    /**
     * Gets the URI for the modified DateTime property that is used when
     * indicating a document has been modified
     * 
     * @return
     */
    public URI getModifiedDateProperty();

    /**
     * Gets the URI for the modified DateTime property that is used when
     * indicating a document has been modified
     * 
     * @return
     */
    public URI getModifiedTimestampProperty();
    
}