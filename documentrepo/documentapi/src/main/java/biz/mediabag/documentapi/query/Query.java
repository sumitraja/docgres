package biz.mediabag.documentapi.query;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.mediabag.commons.DataType;
import eu.mediabag.commons.Multiplicity;

public class Query {

    public enum DataTypeQueryGroup implements Serializable {

        DATES(new DataType[] { DataType.DATE, DataType.DATETIME }, new OpType[] { OpType.EQUALS, OpType.GREATER_THAN,
                OpType.LESS_THAN, OpType.ANY_OF, OpType.IS_NOT_SET, OpType.IS_SET, OpType.BETWEEN }),
        TIMES(new DataType[] { DataType.TIME }, new OpType[] { OpType.EQUALS, OpType.GREATER_THAN, OpType.LESS_THAN,
                        OpType.ANY_OF, OpType.IS_NOT_SET, OpType.IS_SET, OpType.BETWEEN }),
        DATETIMES(new DataType[] { DataType.DATETIME }, new OpType[] { OpType.EQUALS, OpType.GREATER_THAN,
                        OpType.LESS_THAN, OpType.ANY_OF, OpType.IS_NOT_SET, OpType.IS_SET, OpType.BETWEEN }),
        NUMERICS(new DataType[] { DataType.INTEGER, DataType.DECIMAL }, new OpType[] { OpType.EQUALS,
                        OpType.GREATER_THAN, OpType.LESS_THAN, OpType.ANY_OF, OpType.IS_NOT_SET, OpType.IS_SET,
                        OpType.BETWEEN }),
        BOOLEANS(new DataType[] { DataType.BOOLEAN }, new OpType[] {
                OpType.IS_NOT_SET, OpType.IS_SET }),
        TEXT(new DataType[] { DataType.LITERALLANG, }, new OpType[] {OpType.TEXT_SEARCH, OpType.IS_NOT_SET,
                OpType.IS_SET }),
        TAGS(new DataType[] { DataType.TAG }, new OpType[] { OpType.EQUALS, OpType.TEXT_SEARCH, OpType.IS_NOT_SET,
                OpType.IS_SET, OpType.ANCESTOR, OpType.DESCENDANT }),
        STRINGS(new DataType[] { DataType.ANYURI, DataType.STRING, DataType.TAG, DataType.UUID }, new OpType[] {
                        OpType.EQUALS, OpType.REGEX, OpType.IS_NOT_SET, OpType.IS_SET, OpType.ANY_OF }),
        GEOGRAPHIES(new DataType[] { DataType.WGS84_POINT, DataType.WGS84_CIRCLE, DataType.WGS84_SHAPE },
                new OpType[] { OpType.EQUALS, OpType.WITHIN, OpType.WITHIN_DISTANCE_OF_POINT, OpType.IS_NOT_SET,
                        OpType.IS_SET }),
        GEOGRAPHIES_WITH_DIM(new DataType[] { DataType.WGS84_CIRCLE, DataType.WGS84_SHAPE }, new OpType[] {
                OpType.EQUALS, OpType.OVERLAPS, OpType.WITHIN_DISTANCE_OF_POINT, OpType.IS_NOT_SET, OpType.IS_SET }),
        RELATIONSHIPS(new DataType[] { DataType.ASSET }, new OpType[] { OpType.EQUALS, OpType.IS_NOT_SET, OpType.IS_SET,
                        OpType.ANY_OF }),
        ASSET_ID(new DataType[] { DataType.ASSET }, new OpType[] { OpType.EQUALS, OpType.ANY_OF });

        private final Set<DataType> dataTypes = new HashSet<DataType>();

        private final Set<OpType> opTypes = new HashSet<OpType>();

        private DataTypeQueryGroup(DataType[] types, OpType[] allowedOperations) {
            this.dataTypes.addAll(Arrays.asList(types));
            this.opTypes.addAll(Arrays.asList(allowedOperations));
        }

        public Collection<DataType> getDataTypes() {
            return Collections.unmodifiableCollection(dataTypes);
        }

        public Collection<OpType> getOpTypes() {
            return Collections.unmodifiableCollection(opTypes);
        }

    }

    public enum OpType {
        EQUALS(Multiplicity.SIMPLE), GREATER_THAN(Multiplicity.SIMPLE), LESS_THAN(Multiplicity.SIMPLE), ANY_OF(
                Multiplicity.SEQ), BETWEEN(Multiplicity.SEQ), WITHIN_DISTANCE_OF_POINT(Multiplicity.SIMPLE), WITHIN(
                Multiplicity.SIMPLE), OVERLAPS(Multiplicity.SIMPLE), TEXT_SEARCH(Multiplicity.SIMPLE), MODEL(
                Multiplicity.SIMPLE), IS_NOT_SET(Multiplicity.NONE), IS_SET(Multiplicity.NONE), REGEX(
                Multiplicity.SIMPLE), QUERY(Multiplicity.SIMPLE), DESCENDANT(Multiplicity.SIMPLE), ANCESTOR(
                Multiplicity.SIMPLE);

        private final Multiplicity multiplicity;

        private OpType(Multiplicity multi) {
            this.multiplicity = multi;
        }

        public Multiplicity getMultiplicity() {
            return multiplicity;
        }
    }

    private List<FindMeAll<?>> operations = new ArrayList<FindMeAll<?>>();

    private URI orderBy;

    private OrderDirection orderDirection = OrderDirection.ASC;

    private Collection<URI> propsToPreload;

    public List<FindMeAll<?>> getOperations() {
        return Collections.unmodifiableList(operations);
    }

    public <T> void findAnAssetWith(DataTypeQueryGroup group, OpType opType, DataType valueType, T value, URI property) {
        if (!group.getOpTypes().contains(opType)) {
            throw new IllegalArgumentException("Cannot search for " + group.name() + " that " + opType.name()
                    + ". Allowed operations are " + group.getOpTypes());
        }
        if (!(opType == OpType.IS_SET || opType == OpType.IS_NOT_SET) && !group.getDataTypes().contains(valueType)) {
            throw new IllegalArgumentException("Cannot search for " + group.name() + " that " + opType.name() + " a "
                    + valueType.name() + " Allowed valueTypes are " + group.getDataTypes());
        }
        FindMeAll<T> find = new FindMeAll<T>(group).that(opType).value(value, valueType).restrictTo(property);
        operations.add(find);
    }

    public <T> void findAnAssetWith(DataTypeQueryGroup group, OpType opType, DataType valueType, Collection<T> values,
            URI property) {
        if (!group.getOpTypes().contains(opType)) {
            throw new IllegalArgumentException("Cannot search for " + group.name() + " that " + opType.name()
                    + ". Allowed operations are " + group.getOpTypes());
        }
        if (!(opType == OpType.IS_SET || opType == OpType.IS_NOT_SET) && !group.getDataTypes().contains(valueType)) {
            throw new IllegalArgumentException("Cannot search for " + group.name() + " that " + opType.name() + " a "
                    + valueType.name() + " Allowed valueTypes are " + group.getDataTypes());
        } else if (opType.multiplicity == Multiplicity.SIMPLE && (values != null && values.size() > 1)) {
            throw new IllegalArgumentException("Multiple values specified for " + opType.name()
                    + " but it takes only a single value");
        }
        FindMeAll<T> find = new FindMeAll<T>(group).that(opType).values(values, valueType).restrictTo(property);
        operations.add(find);
    }

    public void setOrderBy(URI orderBy) {
        this.orderBy = orderBy;
    }

    public URI getOrderBy() {
        return orderBy;
    }

    public void setOrderDirection(OrderDirection orderDirection) {
        this.orderDirection = orderDirection;
    }

    public OrderDirection getOrderDirection() {
        return orderDirection;
    }

    public void setPropsToPreload(Collection<URI> propsToPreload) {
        this.propsToPreload = propsToPreload;
    }

    public Collection<URI> getPropsToPreload() {
        return propsToPreload;
    }

    public enum OrderDirection {
        ASC, DESC
    }

    public void setOperations(Collection<FindMeAll<?>> ops) {
        operations.addAll(ops);
    }

}
