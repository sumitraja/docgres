package biz.mediabag.documentapi;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import biz.mediabag.documentapi.query.DocumentResultList;
import biz.mediabag.documentapi.query.Query;
import biz.mediabag.documentapi.query.QueryException;
import eu.mediabag.commons.document.PropertyValidationFailedReason;

public interface DocumentRepository<T extends Metadata, U extends MetadataGroup<T>, DOC extends Document, DM extends DocumentModel<T, U>> {
	
	/**
	 * Saves a document. Validates the document before saving according to the model. 
	 */
	
	public Map<URI, Collection<PropertyValidationFailedReason>> validateAndSave(DOC document) throws Exception;
	
	public DOC get(String uuid);
	
	/**
	 * Creates a document. The document might not have a generated UUID at this point
	 * so caller should not bank on having one until they call {@link save}
	 * 
	 * @return
	 */
	public DOC create(URI type) throws DocumentModelNotFoundException;
	
	public DocumentResultList<DOC> search(Query query) throws QueryException;
	
	public void remove(String id);
	
	/**
	 * Get a specific model supported by this repository
	 * 
	 * @param modelURI
	 * @return
	 */
	public DM getModel(URI modelURI) throws DocumentModelNotFoundException;

}
