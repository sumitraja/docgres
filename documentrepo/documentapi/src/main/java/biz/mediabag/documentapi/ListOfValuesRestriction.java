/**
 * 
 */
package biz.mediabag.documentapi;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;

import eu.mediabag.commons.LangLiteral;
import eu.mediabag.commons.Tag;

public class ListOfValuesRestriction implements Serializable, Restriction {

    private static final long serialVersionUID = 6646945317191747974L;
    /** key -> (lang, string) **/
	private Collection<Tag> tags = new LinkedList<>();

	public Collection<Tag> getValues() {
		return Collections.unmodifiableCollection(tags);
	}

	/**
	 * @param values
	 *            the values to set
	 */
	public void setValues(Map<String, LangLiteral> keyDescription) {
		this.tags.clear();
		for (Map.Entry<String, LangLiteral> entry : keyDescription.entrySet()) {
			Tag tag = new Tag(entry.getKey(), entry.getValue());
			tags.add(tag);
		}
		
	}

	public void addValue(String string, LangLiteral langLiteral) {
		this.tags.add(new Tag(string, langLiteral));
	}

	@Override
	public boolean isValid(String id) {
		for (Tag tag : tags) {
			if(tag.id.equals(id)) {
				return true;
			}
		}
		return false;
	}
}