package biz.mediabag.documentapi;

import java.io.Serializable;
import java.net.URI;

import eu.mediabag.commons.Access;
import eu.mediabag.commons.DataType;
import eu.mediabag.commons.LangLiteral;
import eu.mediabag.commons.Multiplicity;
import eu.mediabag.commons.Usage;

public class Metadata implements Serializable {

	private static final long serialVersionUID = 2047925334634863303L;
	private final Usage usage;
	private final DataType type;
	private final Multiplicity multiplicity;
	private Restriction restriction;
	private LangLiteral description;
	public final URI id;
	private Access access = Access.READWRITE;

	public Metadata(URI id) {
		this(id, DataType.STRING, Multiplicity.SIMPLE);
	}
	
	public Metadata(URI id, DataType type) {
		this(id, type, Multiplicity.SIMPLE);
	}

	public Metadata(URI id, DataType type, Multiplicity multi) {
		this(id, type, multi, Usage.OPTIONAL);
	}

	public Metadata(URI id, DataType type, Multiplicity multi, Usage usage) {
		this.id = id;
		this.type = type;
		this.multiplicity = multi;
		this.usage = usage;
	}
	
	/**
	 * @return the type
	 */
	public DataType getType() {
		return type;
	}

	/**
	 * @return the usage
	 */
	public Usage getUsage() {
		return usage;
	}

	/**
	 * @return the multiplicity
	 */
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}

	/**
	 * @return the restrictionList
	 */
	public Restriction getRestriction() {
		return restriction;
	}

	public void addDescription(String literal, String lang) {
		if (description == null) {
			description = new LangLiteral();
		}
		description.addLiteral(literal, lang);
	}

	/**
	 * @param restriction
	 *            the restrictionList to set
	 */
	public void setRestriction(Restriction restriction) {
		this.restriction = restriction;
	}

	/**
	 * @return the description
	 */
	public LangLiteral getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(LangLiteral description) {
		this.description = description;
	}

	public boolean isComposite() {
		return multiplicity != Multiplicity.SIMPLE;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Metadata other = (Metadata) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Metadata{" +
				"usage=" + usage +
				", type=" + type +
				", multiplicity=" + multiplicity +
				", restriction=" + restriction +
				", description=" + description +
				", id=" + id +
				", access=" + access +
				'}';
	}

	/**
	 * 
	 * @return Map of group access or an empty map if none
	 */
	public Access getAccess() {
	    return access;
	}

	public void setAccess(Access groupAccess) {
		if(access == Access.EXECUTE) {
			throw new IllegalArgumentException("EXECUTE access is not allowed on model properties");
		}
	    this.access = groupAccess;
	}
	
}