package biz.mediabag.documentapi;

import java.net.URI;

public class DocumentModifiedException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2579212129357911320L;

	public DocumentModifiedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public DocumentModifiedException(URI arg0) {
		super(arg0.toString());
	}
	
	public DocumentModifiedException(String arg0) {
		super(arg0);
	}

	public DocumentModifiedException(Throwable arg0) {
		super(arg0);
	}


}
