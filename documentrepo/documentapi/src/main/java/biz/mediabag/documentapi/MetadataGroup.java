package biz.mediabag.documentapi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import eu.mediabag.commons.Multiplicity;

public class MetadataGroup<T extends Metadata> {

	private Multiplicity collectionType;
	private Collection<T> metadataItems;

	public MetadataGroup(Multiplicity groupType) {
		this.collectionType = groupType;
		if (groupType.equals(Multiplicity.BAG)) {
			metadataItems = new HashSet<T>();
		} else {
			metadataItems = new ArrayList<T>();
		}
	}

	public Multiplicity getCollectionType() {
		return collectionType;
	}

	public Collection<T> getMetadataItems() {
		return metadataItems;
	}

	public void addMetadataItem(T item) {
		metadataItems.add(item);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof MetadataGroup)) return false;

		MetadataGroup<?> that = (MetadataGroup<?>) o;

		if (collectionType != that.collectionType) return false;
		return !(metadataItems != null ? !metadataItems.equals(that.metadataItems) : that.metadataItems != null);

	}

	@Override
	public String toString() {
		return "MetadataGroup{" +
				"collectionType=" + collectionType +
				", metadataItems=" + metadataItems +
				'}';
	}

	@Override
	public int hashCode() {
		int result = collectionType.hashCode();
		result = 31 * result + (metadataItems != null ? metadataItems.hashCode() : 0);
		return result;
	}
}