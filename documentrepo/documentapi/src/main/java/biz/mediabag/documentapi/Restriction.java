package biz.mediabag.documentapi;

public interface Restriction {
    
    public boolean isValid(String id);
}