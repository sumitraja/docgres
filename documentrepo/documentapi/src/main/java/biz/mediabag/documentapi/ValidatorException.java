package biz.mediabag.documentapi;

public class ValidatorException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 361926862147552858L;

	public ValidatorException() {
	}

	public ValidatorException(String arg0) {
		super(arg0);
	}

	public ValidatorException(Throwable arg0) {
		super(arg0);
	}

	public ValidatorException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
