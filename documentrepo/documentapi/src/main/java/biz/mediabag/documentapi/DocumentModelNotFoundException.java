package biz.mediabag.documentapi;

import java.net.URI;

public class DocumentModelNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -363162045488531822L;
	
	public DocumentModelNotFoundException() {
		super("Model not found");
	}

	public DocumentModelNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public DocumentModelNotFoundException(URI arg0) {
		super(arg0.toString());
	}
	
	public DocumentModelNotFoundException(String arg0) {
		super(arg0);
	}

	public DocumentModelNotFoundException(Throwable arg0) {
		super(arg0);
	}


}
