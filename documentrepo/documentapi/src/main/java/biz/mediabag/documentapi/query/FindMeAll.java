package biz.mediabag.documentapi.query;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import biz.mediabag.documentapi.query.Query.DataTypeQueryGroup;
import biz.mediabag.documentapi.query.Query.OpType;
import eu.mediabag.commons.DataType;
import eu.mediabag.commons.Multiplicity;

public class FindMeAll<T> {

    public static Map<OpDataTypeKey, List<DataType>> valueDataTypes = new HashMap<FindMeAll.OpDataTypeKey, List<DataType>>();

    static {
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATES, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.DATE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATES, OpType.GREATER_THAN),
                Arrays.asList(new DataType[] { DataType.DATE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATES, OpType.LESS_THAN),
                Arrays.asList(new DataType[] { DataType.DATE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATES, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.DATE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATES, OpType.BETWEEN),
                Arrays.asList(new DataType[] { DataType.DATE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATETIMES, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.DATETIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATETIMES, OpType.GREATER_THAN),
                Arrays.asList(new DataType[] { DataType.DATETIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATETIMES, OpType.LESS_THAN),
                Arrays.asList(new DataType[] { DataType.DATETIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATETIMES, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.DATETIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.DATETIMES, OpType.BETWEEN),
                Arrays.asList(new DataType[] { DataType.DATETIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TIMES, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.TIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TIMES, OpType.GREATER_THAN),
                Arrays.asList(new DataType[] { DataType.TIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TIMES, OpType.LESS_THAN),
                Arrays.asList(new DataType[] { DataType.TIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TIMES, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.TIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TIMES, OpType.BETWEEN),
                Arrays.asList(new DataType[] { DataType.TIME }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.NUMERICS, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.DECIMAL }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.NUMERICS, OpType.GREATER_THAN),
                Arrays.asList(new DataType[] { DataType.DECIMAL }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.NUMERICS, OpType.LESS_THAN),
                Arrays.asList(new DataType[] { DataType.DECIMAL }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.NUMERICS, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.DECIMAL }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.NUMERICS, OpType.BETWEEN),
                Arrays.asList(new DataType[] { DataType.DECIMAL }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TEXT, OpType.TEXT_SEARCH),
                Arrays.asList(new DataType[] { DataType.LITERALLANG }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TAGS, OpType.TEXT_SEARCH),
                Arrays.asList(new DataType[] { DataType.STRING }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TAGS, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.TAG }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TAGS, OpType.ANCESTOR),
                Arrays.asList(new DataType[] { DataType.TAG }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.TAGS, OpType.DESCENDANT),
                Arrays.asList(new DataType[] { DataType.TAG }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.STRINGS, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.STRING }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.STRINGS, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.STRING }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.GEOGRAPHIES, OpType.WITHIN),
                Arrays.asList(new DataType[] { DataType.WGS84_SHAPE, DataType.WGS84_CIRCLE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.GEOGRAPHIES, OpType.WITHIN_DISTANCE_OF_POINT),
                Arrays.asList(new DataType[] { DataType.WGS84_CIRCLE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.GEOGRAPHIES, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.WGS84_POINT, DataType.WGS84_CIRCLE, DataType.WGS84_SHAPE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.GEOGRAPHIES, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.WGS84_POINT, DataType.WGS84_CIRCLE, DataType.WGS84_SHAPE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.GEOGRAPHIES_WITH_DIM, OpType.OVERLAPS),
                Arrays.asList(new DataType[] { DataType.WGS84_CIRCLE, DataType.WGS84_SHAPE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.GEOGRAPHIES_WITH_DIM, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.WGS84_CIRCLE, DataType.WGS84_SHAPE }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.BOOLEANS, OpType.IS_SET),
                Arrays.asList(new DataType[] { DataType.BOOLEAN }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.BOOLEANS, OpType.IS_NOT_SET),
                Arrays.asList(new DataType[] { DataType.BOOLEAN }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.BOOLEANS, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.BOOLEAN }));
        valueDataTypes.put(new OpDataTypeKey(null, OpType.IS_SET), Arrays.asList(new DataType[] { DataType.BOOLEAN }));
        valueDataTypes.put(new OpDataTypeKey(null, OpType.IS_NOT_SET),
                Arrays.asList(new DataType[] { DataType.BOOLEAN }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.RELATIONSHIPS, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.ASSET }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.RELATIONSHIPS, OpType.IS_NOT_SET),
                Arrays.asList(new DataType[] { DataType.ASSET }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.RELATIONSHIPS, OpType.IS_SET),
                Arrays.asList(new DataType[] { DataType.ASSET }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.RELATIONSHIPS, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.ASSET }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.ASSET_ID, OpType.EQUALS),
                Arrays.asList(new DataType[] { DataType.ASSET }));
        valueDataTypes.put(new OpDataTypeKey(DataTypeQueryGroup.ASSET_ID, OpType.ANY_OF),
                Arrays.asList(new DataType[] { DataType.ASSET }));
    }

    public final DataTypeQueryGroup group;
    private OpType opType;
    private DataType dataType;
    private Collection<T> value = new LinkedList<T>();
    private URI property;

    public FindMeAll(DataTypeQueryGroup group) { // assets with
                                                 // Dates/Times/Points etc
        this.group = group;
    }

    public FindMeAll<T> that(OpType opType) { // that = or < or > or textMatch
        this.opType = opType;
        return this;
    }

    public FindMeAll<T> value(T value, DataType dataType) {
        if (value == null && opType.getMultiplicity() != Multiplicity.NONE) {
            throw new IllegalArgumentException("Value cannot be null for op type " + opType.name());
        }
        if (value != null) {
            this.value.add(value);
        }
        this.dataType = dataType;
        return this;
    }

    public FindMeAll<T> values(Collection<T> value, DataType dataType) {
        if (value == null && opType.getMultiplicity() != Multiplicity.NONE) {
            throw new IllegalArgumentException("Value cannot be null for op type " + opType.name());
        }
        if (value != null) {
            this.value.addAll(value);
        }
        this.dataType = dataType;
        return this;
    }

    public FindMeAll<T> restrictTo(URI property) {
        this.property = property;
        return this;
    }

    public Collection<T> getValues() {
        return value;
    }

    public T getSingleValue() {
        if (value.size() == 0) {
            throw new IllegalArgumentException("Expected size of values to be at least 1");
        }
        return value.iterator().next();
    }

    public OpType getOpType() {
        return opType;
    }

    public DataType getDataType() {
        return dataType;
    }

    public URI getProperty() {
        return property;
    }

    public List<DataType> getValueType() {
        // find the specific group -> OpType map
        OpDataTypeKey odk = new OpDataTypeKey(group, opType);
        List<DataType> list = valueDataTypes.get(odk);
        if (list == null) {
            // find the generic match for group
            odk = new OpDataTypeKey(group, null);
            list = valueDataTypes.get(odk);

        }
        return list;
    }

    public static List<DataType> getAllowedDataTypes(DataTypeQueryGroup group, OpType opType) {
        // find the specific group -> OpType map
        OpDataTypeKey odk = new OpDataTypeKey(group, opType);
        List<DataType> list = valueDataTypes.get(odk);
        if (list == null) {
            // find the generic match for group
            odk = new OpDataTypeKey(group, null);
            list = valueDataTypes.get(odk);

        }
        return list;
    }

    static class OpDataTypeKey {
        public final OpType op;
        public final DataTypeQueryGroup dataTypeQueryGroup;

        OpDataTypeKey(DataTypeQueryGroup dataTypeQueryGroup, OpType op) {
            this.op = op;
            this.dataTypeQueryGroup = dataTypeQueryGroup;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((dataTypeQueryGroup == null) ? 0 : dataTypeQueryGroup.hashCode());
            result = prime * result + ((op == null) ? 0 : op.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            OpDataTypeKey other = (OpDataTypeKey) obj;
            if (dataTypeQueryGroup != other.dataTypeQueryGroup)
                return false;
            if (op != other.op)
                return false;
            return true;
        }

    }

    @Override
    public String toString() {
        return "Find me " + group.name() + " that " + opType.name() + " " + value;
    }

}
