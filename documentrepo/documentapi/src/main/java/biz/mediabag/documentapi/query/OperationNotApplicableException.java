package biz.mediabag.documentapi.query;


public class OperationNotApplicableException extends QueryException {

    private static final long serialVersionUID = 6514071412622814575L;

    public OperationNotApplicableException(String string) {
        super(string);
    }

}
