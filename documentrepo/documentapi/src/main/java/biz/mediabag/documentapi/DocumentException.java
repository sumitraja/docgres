/**
 * 
 */
package biz.mediabag.documentapi;

/**
 * @author sumitraja
 *
 */
public class DocumentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7418223246961958858L;

	/**
	 * 
	 */
	public DocumentException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public DocumentException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public DocumentException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DocumentException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
