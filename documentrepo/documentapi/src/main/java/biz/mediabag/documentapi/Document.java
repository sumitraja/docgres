package biz.mediabag.documentapi;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import eu.mediabag.commons.document.CompositeValue;

public interface Document {

	/**
	 * @return the unique identifier of the document. Implementations are free
	 *         to use any UUID implementation hence this is a String
	 */
	public abstract String getUuid();

	/**
	 * Sets a property on the document. Property values should be expected to be
	 * overwritten. In the case of multi-value properties the implementation
	 * must replace the multi-value list with the incoming values and not simply
	 * add to the existing list. It is the responsibility of the caller to add
	 * to an existing list of values
	 * 
	 * @throws AssetPropertyException
	 * @throws PropertyAccessException
	 * 
	 */
	public abstract void setProperty(URI name, CompositeValue value);

	/**
	 * @return the value of the property with the specified name. The caller
	 *         must be permission checked so in case they don't actually have
	 *         access to the property. If there aren't explicit property
	 *         permissions set then the permissions of the model must be used as
	 *         a default.
	 * @throws PropertyAccessException
	 * @throws AssetPropertyException
	 */
	public abstract CompositeValue getProperty(URI name);

	/**
	 * 
	 * @return A collection containing the names of the properties of this
	 *         document
	 */
	public abstract Collection<URI> getPropertyURIs();

	/**
	 * 
	 * @return A Map containing the names and values of all properties of this
	 *         document. This map will be mutable but changes will not be
	 *         reflected on the actual document. Only properties that the user
	 *         has READWRITE or READ access to will be returned.
	 */
	public abstract Map<URI, CompositeValue> getProperties();

	/**
	 * Removes a property from an document. Removing a required property will
	 * cause validation errors.
	 * 
	 * @param property
	 * @throws PropertyAccessException
	 */
	public abstract void removeProperty(URI property);

	/**
	 * Gets the URI of the model of this document.
	 * 
	 * @return
	 */
	public abstract DocumentModel<?,?> getModel();

}