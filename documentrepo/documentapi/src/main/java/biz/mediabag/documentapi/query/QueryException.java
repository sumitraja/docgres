package biz.mediabag.documentapi.query;

public class QueryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3424662043987696417L;

	public QueryException() {
		// TODO Auto-generated constructor stub
	}

	public QueryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public QueryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public QueryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
