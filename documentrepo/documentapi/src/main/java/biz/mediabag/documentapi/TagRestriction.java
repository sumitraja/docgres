package biz.mediabag.documentapi;

import java.util.Collection;

import biz.mediabag.tagstore.TagManager;
import eu.mediabag.commons.Tag;

public class TagRestriction implements Restriction {

    private final TagManager tagManager;
    private final String tagPattern;
    
    public TagRestriction(String tagPattern, TagManager tagManager) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        this.tagManager = tagManager;
        if(tagPattern == null) {
            throw new IllegalArgumentException("Configuration cannot be null");
        }
        this.tagPattern = tagPattern;
    }

    public Collection<Tag> getValues() {
        return tagManager.getTagsWithIdMatchingPattern(tagPattern);
    }

	public boolean isValid(String id) {
		return tagManager.getTagById(id) != null;
	}

}
