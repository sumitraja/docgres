package biz.mediabag.documentapi.query;

import biz.mediabag.documentapi.Document;

public class DocumentResult<DOC extends Document> {
    public final DOC result;

    public final float score;

    public DocumentResult(DOC doc, float score) {
        this.result = doc;;
        this.score = score;
    }
}