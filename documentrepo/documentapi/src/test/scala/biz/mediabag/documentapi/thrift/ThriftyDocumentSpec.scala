package biz.mediabag.documentapi.thrift

import java.net.URI
import java.util.Collection
import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.mock.EasyMockSugar
import biz.mediabag.documentapi.Document
import biz.mediabag.documentapi.DocumentModel
import biz.mediabag.documentapi.Metadata
import biz.mediabag.documentapi.MetadataGroup
import biz.mediabag.documentapi.impl.DocumentLike
import biz.mediabag.documentapi.impl.DocumentModelLike
import eu.mediabag.commons.LangLiteral
import eu.mediabag.commons.document.CompositeValue
import eu.mediabag.commons.document.PropertyValidationFailedReason
import java.util.UUID
import eu.mediabag.commons.DataType
import eu.mediabag.commons.Multiplicity
import eu.mediabag.commons.Usage
import scala.beans.BeanProperty
import biz.mediabag.documentapi.impl._
import org.joda.time.Instant
import scala.collection.JavaConversions._
import ove.crypto.digest.Blake2b
import org.scalatest.FlatSpec
import biz.mediabag.thriftcommons.{
  Document => TDocument,
  CompositeValue => TCompositeValue,
  PropertyValueElement => TPropertyValueElement,
  DataType => TDataType,
  CompositeValueWithValidation => TCompositeValueWithValidation,
  Multiplicity => TMultiplicity
}

class ThriftDocumentSpec extends FlatSpec with ShouldMatchers with EasyMockSugar {

  class SubjectDocumentModel(val uri: URI, val description: LangLiteral, val freeForm: Boolean)
  extends DocumentModel[Metadata, MetadataGroup[Metadata]] with DocumentModelLike[Metadata, MetadataGroup[Metadata]]
  
  val documentModel = new SubjectDocumentModel(new URI("http://document.model"), new LangLiteral("A document", "en"), false)
  val pdg = new MetadataGroup[Metadata](Multiplicity.BAG)
  pdg.addMetadataItem(new Metadata(new URI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), DataType.STRING))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/dam/owner"), DataType.STRING, Multiplicity.BAG))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/dam/organisation"), DataType.STRING))
  pdg.addMetadataItem(new Metadata(new URI("http://purl.org/dc/terms/duration"), DataType.DURATION))
  pdg.addMetadataItem(new Metadata(new URI("http://purl.org/dc/terms/title"), DataType.LITERALLANG, Multiplicity.BAG, Usage.REQUIRED))
  pdg.addMetadataItem(new Metadata(new URI("http://my.url.org/number"), DataType.DECIMAL, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/location"), DataType.WGS84_POINT, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/integer"), DataType.INTEGER, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/circle"), DataType.WGS84_CIRCLE, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/shape"), DataType.WGS84_SHAPE, Multiplicity.SIMPLE))
  val essence = new Metadata(new URI("http://purl.org/dc/terms/source"), DataType.ANYURI, Multiplicity.SIMPLE)
  pdg.addMetadataItem(essence)
  documentModel.addPropertyGroup(pdg)
  
  class Subject extends Document with DocumentLike[Metadata, MetadataGroup[Metadata]] with ThriftDocument[Metadata, MetadataGroup[Metadata]] {
    
    @BeanProperty val uuid = UUID.randomUUID.toString
    @BeanProperty val model = documentModel
    val properties: Map[URI, (Unchanged, Instant)] = Map()
    override val hashEngine = Blake2b.Mac.newInstance("Mediabag".getBytes);
    
    override def getProperty(name: URI): CompositeValue = {
      getProperty(model.getProperty(name))
    }

    override def setProperty(property: URI, value: CompositeValue) {
      setProperty(model.getProperty(property), value)
    }

    override def removeProperty(property: URI) {
      removeProperty(model.getProperty(property))
    }

    override def validate = {
      Map[URI, Collection[PropertyValidationFailedReason]]()
    }
  }

  "A ThriftDocument" should "serialise correctly" in {
	  val subject = new Subject
	  subject.setProperty(new URI("http://mediabag.biz/integer"), new CompositeValue("1", DataType.INTEGER))
	  subject.setProperty(new URI("http://purl.org/dc/terms/title"), new CompositeValue("Hello", "en", DataType.LITERALLANG))
	  val td = subject.toThrift
	  var cvwv = td.properties("http://mediabag.biz/integer")
	  cvwv.value should be (TCompositeValue(Set(TPropertyValueElement("1", None, TDataType.Integer))))
	  cvwv = td.properties("http://purl.org/dc/terms/title")
	  cvwv.value should be (TCompositeValue(Set(TPropertyValueElement("Hello", Some("en"), TDataType.LiteralLang))))
  }
}