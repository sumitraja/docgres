package biz.mediabag.docgres

import java.io.File
import java.net.URI
import java.util.UUID

import biz.mediabag.documentapi.impl.ISODataTypeConvertor
import biz.mediabag.documentapi.query.Query
import biz.mediabag.postgres.jdbc.{DatabaseTestSetup, JDBCOps}
import biz.mediabag.docgres.document.{SearchgresDocumentWriter, DocumentCreator}
import biz.mediabag.docgres.model. FileModelReader
import eu.mediabag.commons.{LangLiteral, Multiplicity, DataType}
import eu.mediabag.commons.document.CompositeValue
import org.scalatest.{fixture, Matchers}
import biz.mediabag.documentapi.query.Query.DataTypeQueryGroup._
import biz.mediabag.documentapi.query.Query.OpType._
import scala.collection.JavaConversions._

class SearchgresDocumentRepositorySpec extends DatabaseConSpecBase  with fixture.FlatSpecLike
  with DatabaseTestSetup with Matchers with DocumentCreator {

  override type FixtureParam =  SearchgresDocumentRepository

  override def withFixture(test: OneArgTest) = {
    val theFixture = new  {
      val configFilename = "searchgres.jdbc.properties"
      val modelReader = new FileModelReader(new File("src/test/resources/models"))
      val getFlywaySchema = "searchgres"
    } with SearchgresDocumentRepository with SearchgresDocumentRepositoryConfiguration
    withFixture(test.toNoArgTest(theFixture))
  }

  class Writer extends {
    val schema = SearchgresDocumentRepositorySpec.this.schema
    val tablePrefix = SearchgresDocumentRepositorySpec.this.tablePrefix
    override val dataTypeConvertor = ISODataTypeConvertor
  } with SearchgresDocumentWriter with JDBCOps with SQLConstants {
    override def dataSource = ds
  }

  override def beforeAll() = {
    configureDatabase
    createDocument
  }

  override def afterAll() = {
    dropSchema
  }

  "A SearchgresDocumentRepository" should "get a document by uuid" in { subject =>
    subject.get(uuid)
  }

  "A SearchgresDocumentRepository" should "create a document" in { subject =>
    val d = subject.create("http://mediabag.biz/model#content")
    val v = new CompositeValue(Multiplicity.BAG, UUID.randomUUID.toString, "en", DataType.LITERALLANG)
    d.setProperty("http://purl.org/dc/terms/title", v)
    subject.validateAndSave(d) shouldBe null
    val check = subject.get(d.getUuid)
    check shouldNot be(null)
    check.getModel.getURI shouldEqual (new URI("http://mediabag.biz/model#content"))
  }

  "A SearchgresDocumentRepository" should "create and seach for a document" in { subject =>
    val d = subject.create("http://mediabag.biz/model#content")
    val v = new CompositeValue(Multiplicity.BAG, "Hello", "en", DataType.LITERALLANG)
    d.setProperty("http://purl.org/dc/terms/title", v)
    subject.validateAndSave(d) shouldBe null
    val ll = new LangLiteral("hello", "en")
    val q = new Query
    q.findAnAssetWith(TEXT, TEXT_SEARCH, DataType.LITERALLANG, ll, null)
    val res = subject.search(q)
    res.getNumberOfResults shouldEqual 1
    val first = res.getResults.head
    first.score
  }
}