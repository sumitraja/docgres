package biz.mediabag.docgres

import java.sql.BatchUpdateException
import javax.sql.DataSource

import biz.mediabag.postgres.jdbc.DatabaseTestSetup
import biz.mediabag.docgres.document.DocumentModelFixture
import org.scalatest.{Suite, BeforeAndAfterAll, fixture}

/**
 * Created by sraja2 on 16/02/2015.
 */
class DatabaseConSpecBase extends {
  override val schema = "searchgres"
  override val tablePrefix = "searchgres"
  override val flywayLocations = List("db/searchgres")
} with Suite with BeforeAndAfterAll with DatabaseTestSetup {

  override def beforeAll() {
    dropSchema
    configureDatabase
  }

  override def afterAll() {
    dropSchema
  }

}
