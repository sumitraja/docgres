package biz.mediabag.docgres.document

import java.net.URI

import biz.mediabag.documentapi.{Metadata, MetadataGroup}
import biz.mediabag.docgres.model.SearchgresDocumentModel
import eu.mediabag.commons.{DataType, LangLiteral, Multiplicity, Usage}

trait DocumentModelFixture {

  val documentModel = new SearchgresDocumentModel("http://document.model", new LangLiteral("A document", "en"), false)
  val pdg = new MetadataGroup[Metadata](Multiplicity.BAG)
  pdg.addMetadataItem(new Metadata(new URI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), DataType.STRING))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/dam/owner"), DataType.STRING, Multiplicity.BAG))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/dam/organisation"), DataType.STRING))
  pdg.addMetadataItem(new Metadata(new URI("http://purl.org/dc/terms/duration"), DataType.DURATION))
  pdg.addMetadataItem(new Metadata(new URI("http://purl.org/dc/terms/title"), DataType.LITERALLANG, Multiplicity.BAG, Usage.REQUIRED))
  pdg.addMetadataItem(new Metadata(new URI("http://my.url.org/number"), DataType.DECIMAL, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/location"), DataType.WGS84_POINT, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/integer"), DataType.INTEGER, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/circle"), DataType.WGS84_CIRCLE, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/shape"), DataType.WGS84_SHAPE, Multiplicity.SIMPLE))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/tagCloud"), DataType.TAG, Multiplicity.BAG))
  pdg.addMetadataItem(new Metadata(new URI("http://mediabag.biz/stringOftagCloud"), DataType.STRING, Multiplicity.BAG))
  val essence = new Metadata(new URI("http://purl.org/dc/terms/source"), DataType.ANYURI, Multiplicity.SIMPLE)
  pdg.addMetadataItem(essence)
  documentModel.addPropertyGroup(pdg)
  
  implicit def uri(str: String): URI = new URI(str)
}