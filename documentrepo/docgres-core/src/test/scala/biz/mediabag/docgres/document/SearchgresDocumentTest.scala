package biz.mediabag.docgres.document

import java.net.URI
import java.util.UUID

import biz.mediabag.documentapi.impl._
import eu.mediabag.commons.document.CompositeValue
import eu.mediabag.commons.{DataType, Multiplicity}
import org.joda.time.Instant
import org.scalatest.{FunSuite, Matchers}

class SearchgresDocumentTest extends FunSuite with Matchers with DocumentModelFixture {

  test("Validation of document") {
    val titleInstant = new Instant
    val uuid = UUID.randomUUID().toString()
    val title = new CompositeValue(Multiplicity.BAG, "Here goes the title of this fantastic asset", "en", DataType.LITERALLANG)
    title.addValue("Aquí va el título de este activo fantástico", "es", DataType.LITERALLANG)
    title.addValue("Here goes the title of this fantastic asset", "de", DataType.LITERALLANG)
    val duration = new CompositeValue("PT01H30M5.445S", DataType.DURATION)
  
    val props: Map[URI, (Unchanged, Instant)] =
      Map(uri("http://purl.org/dc/terms/title") -> (Unchanged(title), titleInstant),
          uri("http://purl.org/dc/terms/duration") -> (Unchanged(duration), titleInstant))
    val subject = new SearchgresDocument("1", documentModel, props)
    
  }
}