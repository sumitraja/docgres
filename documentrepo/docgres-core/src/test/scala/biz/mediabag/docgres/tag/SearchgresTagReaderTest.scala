package biz.mediabag.docgres.tag

import biz.mediabag.tagstore.MissingTagParentsException
import eu.mediabag.commons.{LangLiteral, Tag}
import org.scalatest.Assertions._

class SearchgresTagReaderTest extends TagFixtureSpecBase {

  import TagFixture._

  "Tags" should "be retrieved" in { fp  =>
    val reader = fp.reader
    val savedTag = reader.searchTagById("food.indian")
    savedTag match {
      case Some(st) =>
        assert(st === indian)
        assert(st.getParent == food)
      case None => fail("Tags were not found")
    }

  }

  "Tag by title" should "be found" in { fp =>
    val tags = fp.reader.searchTagsByTitle("french", "en")
    assert(tags.size == 1, tags)
    val result = tags(0)
    assert(result === french)
    assert(result.getParent === food)
  }

  "Tags by pattern" should "be found" in { fp =>
    val tags = fp.reader.searchTagsWithIdMatchingPattern("food.*")
    assert(tags.size == 3, tags)
    assert(tags.contains(indian), tags)
    assert(tags.contains(southIndian), tags)
    assert(tags.contains(french), tags)
  }

  "Searching for parent tag" should "have children" in { fp =>
    val tags = fp.reader.searchTagsByTitle("food", "en")
    assert(tags.size == 4, tags)
    val result = tags(0)
    assert(result === food)
    assert(result.getParent == null, result.getParent)
    val secondResult = tags(1)
    assert(secondResult === french, secondResult)
    assert(secondResult.getParent == food)
    val thirdResult = tags(2)
    assert(thirdResult === indian, thirdResult)
    assert(thirdResult.getParent == food)
    val fourthResult = tags(3)
    assert(fourthResult === southIndian, fourthResult)
    assert(fourthResult.getParent == indian)
  }
}
