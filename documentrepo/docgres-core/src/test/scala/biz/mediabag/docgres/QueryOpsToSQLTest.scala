package biz.mediabag.repong.sql

import java.net.URI

import biz.mediabag.documentapi.impl.ISODataTypeConvertor
import biz.mediabag.documentapi.query.Query.{DataTypeQueryGroup, OpType}
import biz.mediabag.documentapi.query.{FindMeAll, Query}
import biz.mediabag.postgres.jdbc.DatabaseTestSetup
import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.docgres.{SQLQueryBuilder, QueryOpsToSQL, SQLConstants, ToSQL}
import eu.mediabag.commons._
import org.joda.time.{DateTime, LocalTime}
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.collection.JavaConversions.seqAsJavaList

class QueryOpsToSQLTest extends {
  override val schema = "searchgres"
  override val tablePrefix = "searchgres"
  override val flywayLocations = List("db/searchgres")
} with FunSuite with BeforeAndAfterAll with SQLConstants with ToSQL with QueryOpsToSQL  with DatabaseTestSetup {
  
  override def beforeAll() = {
	  configureDatabase
  }
  
  override def afterAll() = {
    dropSchema
  }

  test("NumericEquals") {
    val findMeAll = new FindMeAll[Long](DataTypeQueryGroup.NUMERICS) that (OpType.EQUALS) value (30, DataType.DECIMAL)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("NumericAnyOf") {
    val findMeAll = new FindMeAll[Long](DataTypeQueryGroup.NUMERICS) that (OpType.ANY_OF) values (List(30L, 40L, 50L), DataType.DECIMAL)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("NumericBetween") {
    val findMeAll = new FindMeAll[Long](DataTypeQueryGroup.NUMERICS) that (OpType.BETWEEN) values (List(30L, 40L), DataType.DECIMAL)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("TimeEquals") {
    val findMeAll = new FindMeAll[LocalTime](DataTypeQueryGroup.TIMES) that (OpType.EQUALS) value (ISODataTypeConvertor.convertTime("12:00:00"), DataType.TIME)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("DatesEquals") {
    val findMeAll = new FindMeAll[DateTime](DataTypeQueryGroup.DATES) that (OpType.EQUALS) value (new DateTime, DataType.DATETIME)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("DatesAnyOf") {
    val baseDateTime = new DateTime
    val anyOf = List(baseDateTime, baseDateTime.plusDays(10), baseDateTime.plusMonths(2))
    val findMeAll = new FindMeAll[DateTime](DataTypeQueryGroup.DATES) that (OpType.ANY_OF) values (anyOf, DataType.DATETIME)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("DatesBetween") {
    val baseDateTime = new DateTime
    val between = List(baseDateTime, baseDateTime.plusDays(10))
    val findMeAll = new FindMeAll[DateTime](DataTypeQueryGroup.DATES) that (OpType.BETWEEN) values (between, DataType.DATETIME)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("TimeAnyOf") {
    val baseDateTime = new DateTime
    val anyOf = List(baseDateTime.toLocalTime(), baseDateTime.plusDays(10).toLocalTime(), baseDateTime.plusMonths(2).toLocalTime())
    val findMeAll = new FindMeAll[LocalTime](DataTypeQueryGroup.TIMES) that (OpType.ANY_OF) values (anyOf, DataType.TIME)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("DateTimesAreSet") {
    val findMeAll = new FindMeAll[LocalTime](DataTypeQueryGroup.TIMES) that (OpType.IS_SET)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("StringsAreSet") {
    val findMeAll = new FindMeAll[String](DataTypeQueryGroup.STRINGS) that (OpType.IS_SET)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("StringsAnyOf") {
    val anyOf = List("one", "two", "three")
    val findMeAll = new FindMeAll[String](DataTypeQueryGroup.STRINGS) that (OpType.ANY_OF) values (anyOf, DataType.STRING)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("PointsAreSet") {
    val findMeAll = new FindMeAll[WGS84Point](DataTypeQueryGroup.GEOGRAPHIES) that (OpType.IS_SET)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("PointsAreEqual") {
    val findMeAll = new FindMeAll[WGS84Point](DataTypeQueryGroup.GEOGRAPHIES) that (OpType.EQUALS) value (new WGS84Point(51.495727, -0.247342), DataType.WGS84_POINT)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("PointsAnyOf") {
    val anyOf = List(new WGS84Point(51.495727, -0.247342), new WGS84Point(1.495727, 52.247342), new WGS84Point(51.495727, -34.247342))
    val findMeAll = new FindMeAll[WGS84Point](DataTypeQueryGroup.GEOGRAPHIES) that (OpType.ANY_OF) values (anyOf, DataType.WGS84_POINT)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("MultipleIsSetQuery") {
    implicit val con = ds.getConnection
    val query = new Query
    query.findAnAssetWith(Query.DataTypeQueryGroup.GEOGRAPHIES, Query.OpType.IS_SET, null, null, null)
    query.findAnAssetWith(Query.DataTypeQueryGroup.TEXT, Query.OpType.TEXT_SEARCH, DataType.LITERALLANG, new LangLiteral("détails", "fr"), null)

    val aq = buildDocumentQuery(query, 100, 0)
    val prep = aq.prepare
    println(prep.toDebugSQL)
    val rs = prep.executeQuery
    println(rs.next())
  }

  test("DateEquals") {
    val findMeAll = new FindMeAll[DateTime](DataTypeQueryGroup.DATES) that (OpType.GREATER_THAN) value (ISODataTypeConvertor.convertDate("2012-01-01"), DataType.DATE)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("TagEquals") {
    val findMeAll = new FindMeAll[TagId](DataTypeQueryGroup.TAGS) that (OpType.EQUALS) value(new TagId("food.european"), DataType.TAG)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("TagAncestorDescendent") {
    val findMeAll = new FindMeAll[TagId](DataTypeQueryGroup.TAGS) that (OpType.DESCENDANT) value (new TagId("food.european"), DataType.TAG)
    executeQuery(buildSQLQuery(findMeAll))
    val findMeAll2 = new FindMeAll[TagId](DataTypeQueryGroup.TAGS) that (OpType.ANCESTOR) value (new TagId("food.european"), DataType.TAG)
    executeQuery(buildSQLQuery(findMeAll2))
  }

  test("StringsEquals") {
    val findMeAll = new FindMeAll[String](DataTypeQueryGroup.STRINGS) that (OpType.EQUALS) value ("Any String", DataType.STRING)
    executeQuery(buildSQLQuery(findMeAll))

    val findMeAllLtd = findMeAll.restrictTo(new URI("blahblah"))
    executeQuery(buildSQLQuery(findMeAllLtd))
  }

  test("LangLiteral") {
    var findMeAll = new FindMeAll[LangLiteral](DataTypeQueryGroup.TEXT) that (OpType.TEXT_SEARCH) value (new LangLiteral("grass & monkey"), DataType.LITERALLANG)
    executeQuery(buildSQLQuery(findMeAll))

    findMeAll = new FindMeAll[LangLiteral](DataTypeQueryGroup.TEXT) that (OpType.TEXT_SEARCH) value (new LangLiteral("grass & monkey", "en"), DataType.LITERALLANG)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("WithinDistanceOfPoint") {
    var findMeAll = new FindMeAll[WGS84Circle](DataTypeQueryGroup.GEOGRAPHIES) that (OpType.WITHIN_DISTANCE_OF_POINT) value (new WGS84Circle(
      new WGS84Point(51.495727, -0.247342), 10), DataType.WGS84_CIRCLE)
    executeQuery(buildSQLQuery(findMeAll))
  }

  test("AssetQuery") {
    implicit val con = ds.getConnection
    try {
      val query = new Query
      query.findAnAssetWith(DataTypeQueryGroup.GEOGRAPHIES, OpType.WITHIN_DISTANCE_OF_POINT, DataType.WGS84_CIRCLE, new WGS84Circle(
        new WGS84Point(-0.1261814, 51.4998845), 10), null)
      query.findAnAssetWith(DataTypeQueryGroup.TEXT, OpType.TEXT_SEARCH, DataType.LITERALLANG, new LangLiteral("open  |  ba&th .(co)n-+=scious"),
        new URI("http://purl.org/dc/terms/description"))
      query.findAnAssetWith(DataTypeQueryGroup.DATES, OpType.GREATER_THAN, DataType.DATE, new DateTime().withDate(2012, 06, 06), null)
      val anyOf: java.util.Collection[String] = List("one", "two", "three")
      query.findAnAssetWith(DataTypeQueryGroup.STRINGS, OpType.ANY_OF, DataType.STRING, anyOf, null)
      val aq = buildDocumentQuery(query, 100, 0)
      val prep = aq.prepare
      println(prep.toDebugSQL)
      val rs = prep.executeQuery
      println(rs.next())
    } finally {
      con.close
    }
  }

  def executeQuery(sqlbs: SQLQueryBuilder): Unit = {

    val docQuery = new DocumentQuery
    implicit val con = ds.getConnection
    try {
      println(docQuery.cteToSQL(sqlbs))

      val plist = for {
        cteParam <- sqlbs.params
      } yield (cteParam._1, cteParam._2, cteParam._3)
      
      val ps = wrap(docQuery.cteToSQL(sqlbs), plist.map(v => v._1))
      plist.foreach(v => ps.singleOrArrayValue(v._1, v._2)(v._3.asInstanceOf[Manifest[Any]]))
      println(ps.toDebugSQL)
      ps.executeQuery
    } finally {
      con.close;
    }
  }
}