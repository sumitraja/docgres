package biz.mediabag.docgres.document

import java.net.URI
import java.sql.BatchUpdateException
import javax.sql.DataSource

import biz.mediabag.documentapi.impl.ISODataTypeConvertor
import biz.mediabag.documentapi.{Metadata, MetadataGroup}
import biz.mediabag.postgres.jdbc.{DatabaseTestSetup, JDBCOps}
import biz.mediabag.docgres.model.SearchgresDocumentModel
import biz.mediabag.docgres.{DatabaseConSpecBase, SQLConstants, ToSQL}
import org.scalatest.{BeforeAndAfterAll, fixture}

class DocumentFixture extends DatabaseConSpecBase with fixture.FlatSpecLike with DocumentModelFixture with SQLConstants with ToSQL {

  type SearchgresDocumentReaderMD = SearchgresDocumentReader[Metadata, MetadataGroup[Metadata], SearchgresDocumentModel]

  case class FixtureParam(reader: SearchgresDocumentReaderMD, writer: SearchgresDocumentWriter)

  class Writer extends {
    override val schema = this.schema
    override val tablePrefix = this.tablePrefix
    override val dataTypeConvertor = ISODataTypeConvertor
  } with SearchgresDocumentWriter with JDBCOps with SQLConstants {
    override def dataSource = ds
  }

  class Reader extends {
    override val schema = this.schema
    override val tablePrefix = this.tablePrefix
  } with SearchgresDocumentReaderMD with JDBCOps with SQLConstants {
    override val maxCount = 100
    override def dataSource = ds
    override def getModel(uri:URI) = documentModel
  }

  override def withFixture(test: OneArgTest) = {
    val fixture = FixtureParam(new Reader, new Writer)
    withFixture(test.toNoArgTest(fixture))
  }
}
