package biz.mediabag.docgres.document

import java.net.URI
import java.util.UUID

import biz.mediabag.documentapi.impl.{Change, ISODataTypeConvertor, PropertyAction}
import biz.mediabag.postgres.jdbc.{DatabaseTestSetup, JDBCOps}
import biz.mediabag.docgres.SQLConstants
import eu.mediabag.commons.{DataType, Multiplicity, WGS84Circle}
import eu.mediabag.commons.document.CompositeValue
import org.joda.time.Instant

trait DocumentCreator { self: DatabaseTestSetup =>

  class Writer extends SearchgresDocumentWriter with JDBCOps with SQLConstants {
    override def dataSource = ds
    override val schema = DocumentCreator.this.schema
    override val tablePrefix = DocumentCreator.this.tablePrefix
    override val dataTypeConvertor = ISODataTypeConvertor
  }

  val uuid = UUID.randomUUID().toString()
  
  def createDocument = {
    val s = new Writer
    val title = new CompositeValue(Multiplicity.BAG, "Here goes the title of this fantastic asset", "en", DataType.LITERALLANG)
    title.addValue("Aquí va el título de este activo fantástico", "es", DataType.LITERALLANG)
    title.addValue("Here goes the title of this fantastic asset", "de", DataType.LITERALLANG)
    val duration = new CompositeValue("PT01H30M5.445S", DataType.DURATION)
    val modelId = new CompositeValue("http://mediabag.biz/model#content", DataType.ANYURI)
    val desc = new CompositeValue(Multiplicity.BAG, "Description is one of four rhetorical modes (also known as modes of discourse), along with exposition, argumentation, and narration. Each of the rhetorical modes is present in a variety of forms and each has its own purpose and conventions. The act of description may be related to that of definition. Description is also the fiction-writing mode for transmitting a mental image of the particulars of a story.",
        "en", DataType.LITERALLANG)
    
    val circle = new WGS84Circle
    circle.setLongitude(-0.0838332d)
    circle.setLatitude(51.6160625d)
    circle.setRadius(200000)

    val durInstant = new Instant
    val titleInstant = new Instant
    val props: Map[URI, (PropertyAction, Instant)] =
      Map(uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type") -> (Change(modelId), titleInstant),
        uri("http://purl.org/dc/terms/extent") -> (Change(duration), durInstant),
        uri("http://purl.org/dc/terms/title") -> (Change(title), titleInstant),
        uri("http://purl.org/dc/terms/description") -> (Change(desc), titleInstant),
        uri("http://mediabag.biz/model/property#latlong") -> (Change(new CompositeValue("51.495727 -0.257342", DataType.WGS84_POINT)), titleInstant),
        uri("http://mediabag.biz/model/property#coversCircle") -> (Change(new CompositeValue(ISODataTypeConvertor.convertCircle(circle), DataType.WGS84_CIRCLE)), titleInstant))
    s.saveProperties(Map(uuid -> props))
  }

  implicit def uri(str: String): URI = new URI(str)
}