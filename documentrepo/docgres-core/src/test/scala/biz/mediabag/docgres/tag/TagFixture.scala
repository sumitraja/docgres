package biz.mediabag.docgres.tag

import biz.mediabag.postgres.jdbc.{DatabaseTestSetup, JDBCOps}
import biz.mediabag.docgres.DatabaseConSpecBase
import eu.mediabag.commons.{LangLiteral, Tag}
import org.scalatest.{BeforeAndAfterAll, fixture}

import scala.collection.JavaConversions._

class TagFixtureSpecBase extends DatabaseConSpecBase  with fixture.FlatSpecLike {

  case class FixtureParam(reader: SearchgresTagReader, writer: SearchgresTagWriter)

  import biz.mediabag.docgres.tag.TagFixture._

  val writer = new Writer
  override def beforeAll() {
    super.beforeAll()
    writer.addTags(Set(food, indian, southIndian, french))
  }

  class Writer extends {
    override val TagsTable: String = s"${schema}.${tablePrefix}_tags"
  } with SearchgresTagWriter with JDBCOps {
    override def dataSource = ds
  }

  class Reader extends {
    override val TagsTable: String = s"${schema}.${tablePrefix}_tags"
  } with SearchgresTagReader with JDBCOps {
    override def dataSource = ds
  }

  override def withFixture(test: OneArgTest) = {
    val theFixture = FixtureParam(new Reader, writer)
    withFixture(test.toNoArgTest(theFixture))
  }

}

object TagFixture {
  val food = new Tag("food", new LangLiteral(Map("en" -> "Food", "fr" -> "Alimentaire")))
  val indian = new Tag("indian", new LangLiteral(Map("en" -> "Indian", "fr" -> "Indien")), food)
  val southIndian = new Tag("south", new LangLiteral(Map("en" -> "South", "fr" -> "Sud")), indian)
  val french = new Tag("french", new LangLiteral(Map("en" -> "French", "fr" -> "Français")), food)

  val mbTags = Set(food, indian, french, southIndian)
}