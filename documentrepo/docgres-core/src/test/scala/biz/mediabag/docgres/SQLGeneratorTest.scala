package biz.mediabag.docgres

import java.sql.Connection
import javax.sql.DataSource

import biz.mediabag.documentapi.query.Query.OpType
import biz.mediabag.postgres.jdbc.JDBCOps
import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.docgres.ToSQL._
import org.joda.time.DateTime
import org.scalatest.fixture

class SQLGeneratorTest extends DatabaseConSpecBase with fixture.FlatSpecLike with SQLConstants with ToSQL with JDBCOps {

  override type FixtureParam = DataSource

  override def withFixture(test: OneArgTest) = {
    withFixture(test.toNoArgTest(ds))
  }

  val docQuery = new DocumentQuery

  "Text query" should "parse correctly" in { datas =>
    val textCTE = select(TextValue) withRank {
      varname => "ts_rank_cd(" + varname + ", q, 16)"
    } join "to_tsquery(?::regconfig, ?) as q" on (varname => varname.fieldName + "@@ q") param("english",
      manifest[String]) param("grass | (weather & animal)", manifest[String]) withLimitAndOffset(10, 50)

    executeQuery(textCTE, datas)
  }

  "Limited text query" should "parse correctly" in { datas =>
    val textCTELimit = (select(TextValue) withRank {
      varname => "ts_rank_cd(" + varname + ", q, 16)"
    } join "to_tsquery(?::regconfig, ?) as q" on (varname => varname.fieldName + "@@ q") param("english",
      manifest[String]) param("grass | (weather & animal)", manifest[String])).limitPropertyParam(List("http://limit.by/property", "http://limit.by/another"))
    executeQuery(textCTELimit, datas)
  }

  "Numeric query" should "parse correctly" in { datas =>
    val numericCTE = select(NumericValue) where(OpType.GREATER_THAN,
      And) where (OpType.LESS_THAN) param(74, Manifest.Int) param(84, Manifest.Int)
    executeQuery(numericCTE, datas)
  }

  "Limited Numeric query" should "parse correctly" in { datas =>
    val numericCTELimit = select(NumericValue).limitPropertyParam(List("http://limit.by/property", "http://limit.by/another")) where(OpType.GREATER_THAN,
      And) where (OpType.LESS_THAN) param(74, Manifest.Int) param(84, Manifest.Int)
    executeQuery(numericCTELimit, datas)
  }

  def executeQuery(sqlbs: SQLQueryBuilder, ds: DataSource): Unit = {

    implicit val con = ds.getConnection
    try {
      println(docQuery.cteToSQL(sqlbs))

      val plist = for {
        cteParam <- sqlbs.params
      } yield (cteParam._1, cteParam._2, cteParam._3)

      val ps = wrap(docQuery.cteToSQL(sqlbs), plist.map(v => v._1))
      plist.foreach(v => ps.singleOrArrayValue(v._1, v._2)(v._3.asInstanceOf[Manifest[Any]]))
      println(ps.toDebugSQL)
      ps.executeQuery
    } finally {
      con.close;
    }
  }

  "AssetQuery" should "correctly function" in { datas =>
    withConn {
      implicit con =>
        val textCTE = select(TextValue) withRank {
          varname => "ts_rank_cd(" + varname + ", q, 16)"
        } join "to_tsquery(?::regconfig, ?) as q" on (varname => varname.fieldName + "@@ q") param("english",
          manifest[String]) param("grass | (weather & animal)", manifest[String])
        val textCTEWithLimit = textCTE.limitPropertyParam(List("http://purl.org/dc/terms/title"))

        val numericCTEBase = select(NumericValue) withRank ("1")
        val numericCTE = numericCTEBase where(OpType.GREATER_THAN,
          And) where (OpType.LESS_THAN) param(74, Manifest.Int) param(84, Manifest.Int)
        val numericCteWithLimit = numericCTEBase.limitPropertyParam(List("http://limit.by/property", "http://limit.by/another"))

        val a = new DocumentQuery
        a cte textCTE
        a cte numericCTE
        val aq = populateParameters(a.toSQL, List(textCTE, numericCTE))
        println(aq.toDebugSQL)
        aq.executeQuery

        val b = new DocumentQuery
        b cte textCTEWithLimit
        b cte numericCteWithLimit
        val bq = populateParameters(b.toSQL, List(textCTEWithLimit, numericCteWithLimit))
        println(bq.toDebugSQL)
        bq.executeQuery
    }
  }

  "Join function" should "correctly query" in { datas =>
    withConn {
      implicit con =>
        val withinCTE = select(PointValue) withRank (varname => "ST_Distance(ST_GeographyFromText(?), " + varname + ")/?") join "ST_Buffer(ST_GeographyFromText(?),?) as geog" on ({
          varname => "ST_Contains(geog::geometry, " + varname.fieldName + "::geometry)"
        })
        val params = List('rankStart, 'rankDistance, 'start, 'distance)
        val sm = wrap(docQuery.cteToSQL(withinCTE), params)
        sm.singleValue('rankStart, "POINT(51.495727 -0.247342)")
        sm.singleValue('start, "POINT(51.495727 -0.247342)")
        sm.singleValue('rankDistance, 5000)
        sm.singleValue('distance, 5000)
        sm.executeQuery
    }
  }

  "Any function" should "correctly query" in { datas =>
    withConn {
      implicit con =>
        val hashes = List(100L, 200L, 300L, 400L)
        val any = select where (HashValue, OpType.ANY_OF, And) param(hashes, Manifest.Long)
        val anyParams = any.params.toList
        val sm = wrap(docQuery.cteToSQL(any), anyParams.map(v => v._1))
        val v = anyParams.head
        sm.singleOrArrayValue(v._1, v._2)(v._3.asInstanceOf[Manifest[Any]])
        println(sm.toDebugSQL)
        sm.executeQuery
    }
  }

  "Between function" should "correctly query" in { datas =>
    withConn {
      implicit con =>
        val start = new DateTime
        val between = select(DateTimeValue) where (OpType.BETWEEN) implicitParam (start) implicitParam (start.plusHours(1))
        val betweenParams = between.params.toList
        val sm = wrap(docQuery.cteToSQL(between), betweenParams.map(v => v._1))
        val v = betweenParams.head
        sm.singleValue(v._1, v._2)(v._3.asInstanceOf[Manifest[Any]])
        val v2 = betweenParams(1)
        sm.singleValue(v2._1, v2._2)(v2._3.asInstanceOf[Manifest[Any]])
        println(sm.toDebugSQL)
        sm.executeQuery
    }
  }

  "Unnamed Params" should "correctly query" in { datas =>
    withConn {
      implicit con =>
        val numericCTE = select(NumericValue) where(OpType.GREATER_THAN,
          And) where (OpType.LESS_THAN)
        val numericps = docQuery.cteToSQL(numericCTE)
        val sm = wrap(numericps)
        sm.singleValue(1)
        sm.singleValue(2)
        sm.executeQuery
    }
  }


  private def populateParameters(sqlString: String, cteList: List[SQLQueryBuilder])(implicit con: Connection) = {
    val paramsList = for {
      cte <- cteList
      cteParams <- cte.params
    } yield cteParams._1

    val sw = wrap(sqlString, paramsList.toList)

    val valuesManifest = for {
      cte <- cteList
      cteParam <- cte.params
    } yield (cteParam._1, cteParam._2, cteParam._3)
    valuesManifest.foreach {
      case (sym, value, mani) =>
        sw.singleOrArrayValue(sym, value)(mani.asInstanceOf[Manifest[Any]])
    }
    sw
  }

}