package biz.mediabag.docgres.tag

import biz.mediabag.tagstore.MissingTagParentsException
import eu.mediabag.commons.{LangLiteral, Tag}
import org.scalatest.FunSuite
import org.scalatest.mock.EasyMockSugar
import org.scalatest.Assertions._
import scala.collection.JavaConversions._

class SearchgresTagWriterTest extends TagFixtureSpecBase {

  import TagFixture._

  "Adding tag with missing parent" should "throw and exception" in { fp =>
    val german = new Tag("german", new LangLiteral(Map("en" -> "German", "de" -> "Deutsch", "fr" -> "Allemand")), food)
    val newTag = new Tag("bavarian", new LangLiteral(Map("en" -> "Bavarian", "de" -> "Bayer", "fr" -> "Bavarois")), german)
    val newTag2 = new Tag("munich", new LangLiteral(Map("en" -> "Munich", "de" -> "München", "fr" -> "Munich")), newTag)
    val mtpe = intercept[MissingTagParentsException] {
      fp.writer.addTags(Set(newTag, newTag2))
    }
    val list = mtpe.getParentLangMissing
    assert(list.contains("food.german@fr"))
    assert(list.contains("food.german@en"))
    assert(list.contains("food.german@de"))
    assert(list.contains("food@de"))
  }

  "Update tag and attempt to add already existing tag" should "succeed" in { fp =>
    val newFrench = new Tag("french", new LangLiteral(Map("en" -> "Frenchie", "fr" -> "Français")), food)
    fp.writer.addTags(Set(newFrench, food))
    fp.reader.searchTagById("food.french") match {
      case Some(savedTag) => assert(savedTag.title.getLiteral("en") === "Frenchie")
      case None => fail("Tag not found")
    }
  }

}
