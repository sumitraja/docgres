package biz.mediabag.docgres.document

import java.io.File

import biz.mediabag.documentapi.impl.DocumentModelDSL._
import biz.mediabag.docgres.model.{FileModelReader, searchgresModel}
import eu.mediabag.commons.DataType._
import eu.mediabag.commons.Multiplicity._
import eu.mediabag.commons.Usage._
import org.scalatest.{FlatSpec, Matchers}

class DocumentModelReaderSpec extends FlatSpec with Matchers {
  val subject = new FileModelReader(new File("src/test/resources/models"))

  val m = searchgresModel(uri = "http://mediabag.biz/model#content",
    desc = "Content" `@` "en" + "Content" `@` "fr" + "Inhalt" `@` "de")

  m.propertyGroup(SIMPLE) { pg =>
    pg metadata (uri = "http://purl.org/dc/terms/title",
      desc = "Title" `@` "en" + "Titre" `@` "fr" + "Titel" `@` "de", dataType = LITERALLANG, multi = BAG,
      usage = REQUIRED)
    pg metadata (uri = "http://purl.org/dc/terms/description",
      desc = "Description" `@` "en" + "Description" `@` "fr" + "Beschreibung" `@` "de",
      dataType = LITERALLANG, multi = BAG, usage = OPTIONAL)
    pg metadata (uri = "http://purl.org/dc/terms/source",
      desc = "Source URI" `@` "en", dataType = ANYURI, multi = SIMPLE, usage = OPTIONAL)
    pg metadata (uri = "http://purl.org/dc/terms/format",
      desc = "Source URI" `@` "en", dataType = TAG, multi = SIMPLE, usage = OPTIONAL)
    pg metadata (uri = "http://purl.org/dc/terms/extent",
      desc = "Duration" `@` "en" + "Durée" `@` "fr" + "Dauer" `@` "de",
      dataType = DURATION, multi = SIMPLE, usage = OPTIONAL)
  }

  m.propertyGroup(BAG) { pg =>
    pg metadata (uri = "http://mediabag.biz/model/property#latlong",
      desc = "Longitude & Latitude" `@` "en" + "Longitude et Latitude" `@` "fr" + "Longitude & Latitude" `@` "de",
      dataType = WGS84_POINT, usage = OPTIONAL, multi = BAG)
    pg metadata (uri = "http://mediabag.biz/model/property#coversCircle",
      desc = "Covers region (Longitude/Latitude and distance)" `@` "en" +
        "Couvre région (longitude / latitude et distance)" `@` "fr" +
        "Covers Region (Länge / Breite und Abstand)" `@` "de",
      dataType = WGS84_CIRCLE, usage = OPTIONAL, multi = BAG)
    pg metadata (uri = "http://mediabag.biz/model/property#coversShape",
      desc = "Covers region with boundaries" `@` "en" +
        "Couvre région avec des limites" `@` "fr" +
        "Covers Region mit Grenzen" `@` "de",
      dataType = WGS84_SHAPE, usage = OPTIONAL, multi = BAG)
  }

  "A FileModelReader" should "read models from files" in {
    val models = subject.models
    assert (models.size == 1)
    val basic = models.head
    assert(basic == m.documentModel)
  }

}