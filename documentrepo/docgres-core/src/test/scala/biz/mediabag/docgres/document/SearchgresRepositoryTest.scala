package biz.mediabag.docgres.document

import java.net.URI
import java.util.UUID

import biz.mediabag.documentapi.impl.{Change, ISODataTypeConvertor, PropertyAction, Remove, Unchanged}
import biz.mediabag.postgres.jdbc.Timer
import biz.mediabag.docgres.Words
import eu.mediabag.commons.document.CompositeValue
import eu.mediabag.commons.{DataType, Multiplicity, WGS84Circle, WGS84Point, WGS84PointFormat}
import org.joda.time.Instant
import org.scalatest.Assertions._
import org.scalatest.fixture

import scala.collection.JavaConversions.seqAsJavaList
import scala.util.Random

class SearchgresRepositoryTest extends DocumentFixture with Timer {

  val pointFormat = new WGS84PointFormat
  "One asset" should "be created" in  { fixture =>
    val uuid = UUID.randomUUID().toString()
    val title = new CompositeValue(Multiplicity.BAG, "Here goes the title of this fantastic asset", "en", DataType.LITERALLANG)
    title.addValue("Aquí va el título de este activo fantástico", "es", DataType.LITERALLANG)
    title.addValue("Here goes the title of this fantastic asset", "de", DataType.LITERALLANG)
    val duration = new CompositeValue("PT01H30M5.445S", DataType.DURATION)
    val modelId = new CompositeValue("http://mediabag.biz/model#content", DataType.ANYURI)
    val owner = new CompositeValue("http://mediabag.biz/dam/owner", DataType.STRING)
    owner.addValue("Content Manager", null, DataType.STRING)
    val org = new CompositeValue("http://mediabag.biz/dam/organisation", DataType.STRING)
    org.addValue("Mediabag", null, DataType.STRING)
    val tagCV = new CompositeValue(Multiplicity.BAG)
    Array("kingdom.plantae", "kingdom.animalia").foreach(x => tagCV.addValue(x, null, DataType.TAG))
    val stringCV = new CompositeValue(Multiplicity.BAG)
    Array("kingdom.plantae", "kingdom.animalia").foreach(x => stringCV.addValue(x, null, DataType.STRING))

    var circle = new WGS84Circle
    circle.setLongitude(-0.0838332d)
    circle.setLatitude(51.6160625d)
    circle.setRadius(200000)

    val shape = Array(new WGS84Point(-0.0838332d, 51.6160625d), new WGS84Point(-0.2087024d, 51.5899046d),
      new WGS84Point(-0.2925246d, 51.5318119d), new WGS84Point(-0.2799415d, 51.4916697d), new WGS84Point(-0.1979859d, 51.4932113d),
      new WGS84Point(-0.1527221d, 51.503699d), new WGS84Point(-0.0809621d, 51.5281383d),
      new WGS84Point(-0.0838332d, 51.6160625d)).toList
    val durInstant = new Instant
    val titleInstant = new Instant
    val modelURI = uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
    val props: Map[URI, (PropertyAction, Instant)] =
      Map(uri("http://mediabag.biz/dam/owner") ->(Change(owner), titleInstant),
        uri("http://mediabag.biz/dam/organisation") ->(Change(org), titleInstant),
        modelURI ->(Change(modelId), titleInstant),
        uri("http://purl.org/dc/terms/duration") ->(Change(duration), durInstant),
        uri("http://purl.org/dc/terms/title") ->(Change(title), titleInstant),
        uri("http://my.url.org/number") ->(Change(new CompositeValue("123.456", DataType.DECIMAL)), titleInstant),
        uri("http://mediabag.biz/location") ->(Change(new CompositeValue("51.495727 -0.257342", DataType.WGS84_POINT)), titleInstant),
        uri("http://mediabag.biz/integer") ->(Change(new CompositeValue("765433", DataType.INTEGER)), titleInstant),
        uri("http://mediabag.biz/circle") ->(Change(new CompositeValue(ISODataTypeConvertor.convertCircle(circle), DataType.WGS84_CIRCLE)), titleInstant),
        uri("http://mediabag.biz/shape") ->(Change(new CompositeValue(ISODataTypeConvertor.convertShape(shape), DataType.WGS84_SHAPE)), titleInstant),
        uri("http://mediabag.biz/tagCloud") ->(Change(tagCV), titleInstant),
        uri("http://mediabag.biz/stringOftagCloud") ->(Change(stringCV), titleInstant))
    val saveIt = Map(uuid -> props)
    val start = System.currentTimeMillis()
    fixture.writer.saveProperties(saveIt)
    val end = System.currentTimeMillis()
    println("Asset " + uuid + " took " + (end - start))
    val readProps = fixture.reader.convertToCompositeValueMap(fixture.reader.getDocumentProperties(uuid).get, modelURI,
      fixture.reader.getModel(modelURI))
    val (titleCV, instant) = readProps(new URI("http://purl.org/dc/terms/title"))
    titleCV match {
      case Unchanged(cv) => assert(title == cv)
      case _ => fail("Unexpected value " + titleCV)
    }
    assert(titleInstant != instant)
    val (durationCV, instant2) = readProps(new URI("http://purl.org/dc/terms/duration"))

    durationCV match {
      case Unchanged(cv) => assert(duration == cv)
      case _ => fail("Unexpected value " + durationCV)
    }

    val (dupStringCV, dsInstant) = readProps(new URI("http://mediabag.biz/stringOftagCloud"))
    dupStringCV match {
      case Unchanged(cv) => assert(stringCV == cv)
      case _ => fail("String dup of tag values not found: " + stringCV)
    }

    val (dupTagCV, dtInstant) = readProps(new URI("http://mediabag.biz/tagCloud"))
    dupTagCV match {
      case Unchanged(cv) => assert(tagCV == cv)
      case _ => fail("Tag dup of string values not found: " + tagCV)
    }
    
    val (intCV, intInstant) = readProps(new URI("http://mediabag.biz/integer"))
    val (decCV, decInstant) = readProps(new URI("http://my.url.org/number"))
    
    intCV match {
      case Unchanged(cv) => assert(cv.getSingleValue.getType == DataType.INTEGER, "http://mediabag.biz/integer should be INTEGER")
      case _ => fail("Int value not found for http://mediabag.biz/integer")
    }
    
    decCV match {
      case Unchanged(cv) => assert(cv.getSingleValue.getType == DataType.DECIMAL, "http://my.url.org/number should be DECIMAL")
      case _ => fail("Int value not found for http://my.url.org/number")
    }

    assert(durInstant != instant2)
    val (locationCV, locationInstant) = readProps(uri("http://mediabag.biz/location"))
    val durationNew = new CompositeValue("P1DT01H30M5.445S", DataType.DURATION)
    val propsNew: Map[URI, (PropertyAction, Instant)] =
      Map(uri("http://purl.org/dc/terms/duration") ->(Change(durationNew), instant2),
        uri("http://purl.org/dc/terms/title") ->(Unchanged(title), titleInstant),
        uri("http://mediabag.biz/location") ->(Remove(locationCV.cv), locationInstant))
    val saveItNew = Map(uuid -> propsNew)
    val startNew = System.currentTimeMillis()
    fixture.writer.saveProperties(saveItNew)
    val endNew = System.currentTimeMillis()
    println("Asset update " + uuid + " took " + (endNew - startNew))

    val readPropsNew = fixture.reader.convertToCompositeValueMap(fixture.reader.getDocumentProperties(uuid).get, modelURI,
      fixture.reader.getModel(modelURI))
    val (titleCVNew, instantNew) = readPropsNew(new URI("http://purl.org/dc/terms/title"))
    titleCV match {
      case Unchanged(cv) => assert(title == cv)
      case _ => fail("Unexpected value " + titleCVNew)
    }
    assert(titleInstant != instantNew)
    val (durationCVNew, instant2New) = readPropsNew(new URI("http://purl.org/dc/terms/duration"))
    durationCVNew match {
      case Unchanged(cv) => assert(durationNew == cv)
      case _ => fail("Unexpected value " + durationCVNew)
    }
  }


  "Many assets" should "be created" in { fixture =>
    val random = new Random
    time("Total time taken was") {
      for (i <- 0 to 100) {
        try {
          val uuid = UUID.randomUUID().toString()
          val sentences = Words.getSentenceForLangs()
          val title = new CompositeValue(Multiplicity.SIMPLE)
          sentences.foreach {
            case (lang, sen) =>
              title.addValue(sen, lang, DataType.LITERALLANG)
          }
          val wgs = new WGS84Point((random.nextDouble() * 180).toDouble, (random.nextDouble() * 90).toDouble)
          val point = new CompositeValue(pointFormat.format(wgs), DataType.WGS84_POINT)
          val durationStr = "PT%02dH%02dM%2.3fS".format(random.nextInt(24), random.nextInt(59), random.nextDouble() * 60)
          val duration = new CompositeValue(durationStr, DataType.DURATION)
          val owner = new CompositeValue("http://mediabag.biz/dam/owner", DataType.STRING)
          owner.addValue("Content Manager", null, DataType.STRING)
          val org = new CompositeValue("http://mediabag.biz/dam/organisation", DataType.STRING)
          org.addValue("Mediabag", null, DataType.STRING)
          val modelId = new CompositeValue("http://mediabag.biz/model#content", DataType.ANYURI)
          val instant = new Instant
          val number = random.nextDouble() * 100
          val props: Map[URI, (PropertyAction, Instant)] = Map(uri("http://mediabag.biz/dam/owner") ->(Change(owner), instant),
            uri("http://mediabag.biz/dam/organisation") ->(Change(org), instant),
            uri("http://purl.org/dc/terms/duration") ->(Change(duration), instant),
            uri("http://purl.org/dc/terms/title") ->(Change(title), instant),
            uri("http://my.url.org/number") ->(Change(new CompositeValue(number.toString, DataType.DECIMAL)), instant),
            uri("http://mediabag.biz/location") ->(Change(point), instant),
            uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type") ->(Change(modelId), instant))
          val saveIt = Map(uuid -> props)
          time("Asset " + uuid + " took") {
            fixture.writer.saveProperties(saveIt)
          }
        } catch {
          case e: Exception => e.printStackTrace()
        }
      }

    }
  }
}

