package biz.mediabag.docgres.tag

import java.sql.{Connection, SQLException}
import java.text.Normalizer
import java.text.Normalizer.Form

import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.postgres.jdbc.{JDBCOps, PgLangMapper}
import biz.mediabag.tagstore.MissingTagParentsException
import eu.mediabag.commons.Tag

import scala.collection.JavaConversions._

trait SearchgresTagWriter extends PgLangMapper { self: JDBCOps =>

  def TagsTable:String

  private def InsertTagValue = s"""insert into $TagsTable(tagpath, title, lang, titlehash, search_text) select ? as tagpath, ? as title, ? as lang, ? as titleHash,
    to_tsvector(?::regconfig, array_to_string(array_prepend(?, array_agg(title)), ' ')) as search_text from (
      select title from $TagsTable as tags where lang=? and tags.tagpath @> ? order by tagpath desc
    ) as titles"""

  private val InsertTagVariables = List('tagpath, 'title, 'lang, 'hash, 'pglang, 'normalisedTitle, 'lang, 'tagpath)

  private def UpdateTagValue = s"""update $TagsTable set title = ?, titleHash = ?, search_text = (
      select to_tsvector(?::regconfig, array_to_string(array_prepend(?, array_agg(titles.title)), ' ')) from (
        select title from $TagsTable where lang=? and $TagsTable.tagpath @> ? order by tagpath desc) as titles
    ) where lang=? and tagpath = ?"""

  private def UpdateTagVariables = List('title, 'hash, 'pglang, 'normalisedTitle, 'lang, 'tagpath, 'lang, 'tagpath)

  private def SelectMissingParents = s"""with recursive t(tagpath, lang) as (
      values %s
      union all
      select subpath(tagpath, 0, nlevel(tagpath) - 1) as tagpath, lang from t where nlevel(tagpath) -1 > 0
    )
    select distinct t.tagpath, t.lang from t left join $TagsTable as tags on t.tagpath = tags.tagpath and t.lang = tags.lang
    where tags.tagpath is null and tags.lang is null"""

  protected def SelectTagByIdLang = s"""select tags.tagpath, tags.lang, titlehash from $TagsTable
    as tags right join (values %s) as vals(tagpath,lang) on tags.tagpath = vals.tagpath and tags.lang = vals.lang"""

  protected val SelectTagValuesClause = """(?::ltree, ?)"""

  def addTags(tags: java.util.Set[Tag]) = {

    def getParents(tag: Tag, tags: List[Tag]): List[Tag] = {
      if (tag == null) {
        tags
      } else {
        getParents(tag.getParent, tag :: tags)
      }
    }

    val parents = tags.map { tag ⇒
      getParents(tag.getParent, List())
    }.flatten

    val parentsDiff = parents.diff(tags)

    withConn { implicit con ⇒
      if (parentsDiff.size > 0) {
        checkForMissingParents(parentsDiff.toList)
      }

      val flatTags = flattenTags(tags)

      val findExistingValues = flatTags.toList.map { t ⇒
        SelectTagValuesClause
      }.mkString(",")

      val findExisting = wrap(SelectTagByIdLang.format(findExistingValues))
      flatTags.foreach {
        case (id, lang, title, literal) ⇒
          findExisting.singleValue(id)
          findExisting.singleValue(lang)
      }

      val existingFlatTags = forEach { rs ⇒
        (rs.getString(1), rs.getString(2), rs.getLong(3))
      } using findExisting

      val insertsUpdates = genChangeList(existingFlatTags, flatTags)
      try {

        if (insertsUpdates.get('Insert).isDefined) {
          val stmt = wrap(InsertTagValue, InsertTagVariables)
          insertsUpdates('Insert).foreach {
            case (eId, eLang, eTitle, eHash) ⇒
              stmt.objectValue('tagpath, eId)
              stmt.singleValue('title, eTitle)
              stmt.singleValue('lang, eLang)
              stmt.singleValue('hash, generateHash(eTitle))
              stmt.singleValue('pglang, mapToPgLang(eLang))
              stmt.objectValue('normalisedTitle, Normalizer.normalize(eTitle, Form.NFC))
              stmt.addBatch
          }
          stmt.executeBatch
        }
        if (insertsUpdates.get('Update).isDefined) {
          val updateStmt = wrap(UpdateTagValue, UpdateTagVariables)
          insertsUpdates('Update).foreach {
            case (eId, eLang, eTitle, eHash) ⇒
              updateStmt.singleValue('title, eTitle)
              updateStmt.singleValue('hash, eHash)
              updateStmt.singleValue('lang, eLang)
              updateStmt.singleValue('pglang, mapToPgLang(eLang))
              updateStmt.objectValue('normalisedTitle, Normalizer.normalize(eTitle, Form.NFC))
              updateStmt.objectValue('tagpath, eId)
              updateStmt.addBatch
          }
          updateStmt.executeBatch
        }
      } catch {
        case e: SQLException ⇒ if (e.getNextException != null) {
            throw e.getNextException
        } else {
            throw e
        }
      }
    }
  }

  private def checkForMissingParents(parentsDiff: List[Tag])(implicit con: Connection) = {
    val missingValuesStmt = parentsDiff.map { tag ⇒
      tag.title.getLangToLiteralMap.keys.toList.map { lang ⇒
        SelectTagValuesClause
      }
    }.flatten.mkString(",")

    val missingValues = parentsDiff.map { tag ⇒
      tag.title.getLangToLiteralMap.keys.map { lang ⇒
        tag.getAbsoluteId -> lang
      }
    }.flatten

    val missingStmt = wrap(SelectMissingParents.format(missingValuesStmt))
    missingValues.foreach {
      case (tagId, lang) ⇒ {
        missingStmt.singleValue(tagId)
        missingStmt.singleValue(lang)
      }
    }

    val missingParents = forEach { rs ⇒
      rs.getString(1) + "@" + rs.getString(2)
    } using missingStmt
    if (missingParents.isDefined) {
      throw new MissingTagParentsException(missingParents.get)
    }
  }
  def genChangeList(existingFlatTags: Option[Iterable[(String, String, Long)]], flatTags: Iterable[(String, String, String, Long)]) = {
    existingFlatTags match {
      case Some(n) ⇒ flatTags.groupBy {
        case (id, lang, title, hash) ⇒ {
          n.find {
            case (eId, eLang, eHash) ⇒
              eId == id && lang == eLang
          } match {
            case Some((eId, eLang, eHash)) if (eHash != hash) ⇒ 'Update
            case None ⇒ 'Insert
            case _ ⇒ 'Ignore
          }
        }
      }

      case None ⇒ Map[Symbol, Set[(String, String, String, Long)]]()
    }
  }

  def flattenTags(tags: Iterable[Tag]) = {
    tags.map { tag ⇒
      tag.title.getLangToLiteralMap.map {
        case (lang, literal) ⇒
          (tag.getAbsoluteId, lang, literal, generateHash(literal))
      }
    }.flatten.toList.sortWith(_._1.count( _ == '.') < _._1.count(_ == '.'))
  }
}
