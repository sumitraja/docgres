package biz.mediabag.docgres.tag

import java.text.Normalizer
import java.text.Normalizer.Form

import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.postgres.jdbc.{JDBCOps, PgLangMapper, StatementWrapper}
import eu.mediabag.commons.{LangLiteral, Tag}

trait SearchgresTagReader extends PgLangMapper { self: JDBCOps =>

  def TagsTable:String

  private def SelectTagById = s"""select tagpath, title, lang from $TagsTable where tagpath @> ?::ltree order by tagpath asc"""

  private def SelectTagByIdVariables = List('tagpath)

  private def SelectTagSearchBase(inline:String) = s"""with search as (
      $inline
    )
    select search.tagpath as hit, tags.tagpath, title, lang from $TagsTable as tags join search on tags.tagpath @> search.tagpath
      group by search.tagpath, tags.tagpath, title, lang"""

  private def SelectTagByPattern = SelectTagSearchBase(s"""
      select tagpath from $TagsTable where tagpath ~ ? order by tagpath asc""")

  private def SelectTagByPatternVariables = List('pattern)

  private def SelectTagByTitle = SelectTagSearchBase(s"""
      select tagpath, ts_rank_cd(search_text, plainto_tsquery(?::regconfig, ?),16) as rank from $TagsTable
      where search_text @@ plainto_tsquery(?::regconfig, ?) and lang=? order by rank desc""")

  private def SelectTagByTitleVariables = List('pglang, 'normalisedTitle, 'pglang, 'normalisedTitle, 'lang)

  def searchTagById(id: String): Option[Tag] = {
    if (id == null) {
      throw new IllegalArgumentException("ID cannot be null")
    }
    withConn { implicit con ⇒
      val stmt = wrap(SelectTagById, SelectTagByIdVariables)
      stmt.objectValue('tagpath, id)
      reduceAll { (rs, tag: Tag) ⇒
        val newTagId = rs.getString(1)
        val newTag = if (tag == null || tag.getAbsoluteId != newTagId) {
          val ll = new LangLiteral
          if (newTagId.lastIndexOf(".") > -1) {
            new Tag(newTagId.substring(newTagId.lastIndexOf(".") + 1), ll)
          } else {
            new Tag(newTagId, ll)
          }
        } else {
          tag
        }
        newTag.title.addLiteral(rs.getString(2), rs.getString(3))
        if (newTag != tag) {
          newTag.setParent(tag)
        }
        newTag
      } using (stmt)
    }
  }

  def searchTagsByTitle(title: String, lang: String): List[Tag] = {
    withConn { implicit con ⇒
      val stmt = wrap(SelectTagByTitle, SelectTagByTitleVariables)
      stmt.objectValue('pglang, mapToPgLang(lang))
      stmt.singleValue('normalisedTitle, Normalizer.normalize(title, Form.NFC))
      stmt.singleValue('lang, lang)
      val hits = runTagQuery(stmt)
      debug("Found tags " + hits + " for query \"" + title + "\"@" + lang)
      hits
    }

  }

  def searchTagsWithIdMatchingPattern(pattern: String) = {
    val lqueryPattern = if (pattern.endsWith("*")) {
      pattern + "{1,}"
    } else {
      pattern
    }

    withConn { implicit con ⇒
      val stmt = wrap(SelectTagByPattern, SelectTagByPatternVariables)
      stmt.objectValue('pattern, lqueryPattern)
      val hits = runTagQuery(stmt)
      debug("Found tags " + hits + " for query \"" + pattern)
      hits
    }
  }

  private def getRootTags = {
    searchTagsWithIdMatchingPattern("*{1}")
  }

  // Make this more functional
  private def runTagQuery(query: StatementWrapper) = {
    var hit: (String, Tag) = null
    var tag: Tag = null
    val tagCache = collection.mutable.Map[String, Tag]()
    val hits = collection.mutable.ListBuffer[Tag]()
    def createAndCacheHit(id: String) = {
      val trimmedId = if (id.lastIndexOf(".") > -1) {
        id.substring(id.lastIndexOf(".") + 1)
      } else {
        id
      }
      val hit = new Tag(trimmedId, new LangLiteral)
      tagCache.put(id, hit)
      (id, hit)
    }
    def checkAndResetHitTrack(hitId: String) = {
      if (hit == null) {
        tag = null
        hit = createAndCacheHit(hitId)
      } else if (hit._1 != hitId) {
        tag = null
        hits += hit._2
        hit = createAndCacheHit(hitId)
      }
    }
    forEach { rs ⇒
      val newHitId = rs.getString(1)
      checkAndResetHitTrack(newHitId)

      val currentTagId = rs.getString(2)
      val tagBeingProcessed = tagCache.get(currentTagId) match {
        case Some(n) ⇒ n
        case None ⇒ createAndCacheHit(currentTagId)._2
      }
      if (tag != tagBeingProcessed) {
        tagBeingProcessed.setParent(tag)
      }
      if (tagBeingProcessed.title.getLiteral(rs.getString(4)) == null) {
        tagBeingProcessed.title.addLiteral(rs.getString(3), rs.getString(4))
      }
      tag = tagBeingProcessed
    } using (query)
    if (hit != null) hits += hit._2
    hits.toList
  }
}
