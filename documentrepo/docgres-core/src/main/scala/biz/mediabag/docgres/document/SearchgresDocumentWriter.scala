package biz.mediabag.docgres.document

import java.net.URI
import java.sql.Connection

import biz.mediabag.documentapi.DocumentModifiedException
import biz.mediabag.documentapi.impl.{Change, PropertyAction, Remove}
import biz.mediabag.postgres.jdbc.{JDBCOps, PgLangMapper, SerialException}
import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.docgres.SQLConstants
import eu.mediabag.commons._
import eu.mediabag.commons.document.CompositeValue
import org.joda.time.Instant

import scala.collection.JavaConversions._

trait SearchgresDocumentWriter extends PgLangMapper { self: JDBCOps with SQLConstants =>

  type DocId = String

  protected def dataTypeConvertor: DataTypeConvertor

  private val LeximeErrors = List("40001", "23505")

  private val LiteralHashFormat = "%s@%s"

  private def CheckLexForHash = s"select id from $LeximeTable where hash=?"

  private def NextLeximeId = "select nextval('" + Prefix + "repository_id')"

  private def GetCompositeValueId = s"select composite_value_id, last_modified from $AssetPropertiesTable where $LinkedDocumentId = ? and property_id = ?"

  private def InsertCompositeValue = s"insert into $CompositeValuesTable (id, value) values (?, ?)"

  private def UpdateAssetPropertyCompositeValue = s"UPDATE $AssetPropertiesTable SET composite_value_id=?, last_modified = ? WHERE $LinkedDocumentId=? and property_id=?"

  private def DeleteAssetPropertyCompositeValue = s"delete from $AssetPropertiesTable WHERE $LinkedDocumentId=? and property_id=? and composite_value_id = ?"

  private def InsertAssetProperty = s"insert into $AssetPropertiesTable($LinkedDocumentId, property_id, composite_value_id, last_modified) values (?, ?, ?, ?)"

  private def DeleteCompositeValue = s"delete from $CompositeValuesTable where id = ?"

  private def FindExistingCompositeId = s"""select c.id, p.id, a.last_modified from $CompositeValuesTable c join $AssetPropertiesTable a
    on a.composite_value_id = c.id join $LeximeTable  p on a.property_id=p.id  where a.$LinkedDocumentId=? and p.hash=?"""

  private lazy val dataTypeColMap =
    Map(DataType.DURATION -> (s"${Prefix}insert_lexime_duration(?, ?)"), //hash, lex
      DataType.ASSET -> (s"${Prefix}insert_lexime_document(?, ?)"), // hash, lex
      DataType.ANYURI -> (s"${Prefix}insert_lexime_anyuri(?, ?)"), //hash, lex
      DataType.LITERALLANG -> (s"${Prefix}insert_lexime_text(?, ?, ?, ?)"), //hash, lex, lang, pgLang
      DataType.STRING -> (s"${Prefix}insert_lexime_string(?, ?)"), //hash, lex
      DataType.TAG -> (s"${Prefix}insert_lexime_tag(?, ?)"), //hash, lex
      DataType.UUID -> (s"${Prefix}insert_lexime_uuid(?, ?)"), //hash, lex
      DataType.DECIMAL -> (s"${Prefix}insert_lexime_numeric(?, ?, false)"), //hash,lex
      DataType.INTEGER -> (s"${Prefix}insert_lexime_numeric(?, ?, true)"), //hash,lex
      DataType.DATETIME -> (s"${Prefix}insert_lexime_datetime(?, ?)"), //hash,lex),
      DataType.TIME -> (s"${Prefix}insert_lexime_time(?, ?)"), //hash,lex),
      DataType.WGS84_POINT -> s"${Prefix}insert_lexime_point(?, ?, ?, ?)", //hash,lex, lon, lat
      DataType.WGS84_CIRCLE -> s"${Prefix}insert_lexime_circle(?, ?, ?, ?, ?)", //hash,lex, lon, lat, radius
      DataType.WGS84_SHAPE -> s"${Prefix}insert_lexime_shape(?, ?, ?)" //hash, lex, point[]
      )

  /**
   * Convert this to do a select to determine all the leximes that need to be created using VALUES
   * e.g. select a.id, a.hash, hashes.hash from lexime a right join (values(1319505313), (1234), (1608892887), (-225537041)) as hashes(hash) on a.hash = hashes.hash
   * then create the leximes that are missing and then create the composite values. This was all the inserts and updates can be done in a batch.
   */
  private def createHashToIdMap(assetProps: Map[String, Map[URI, (PropertyAction, Instant)]], con: Connection): Map[Long, Int] = {
    repeatOnSQLStates(LeximeErrors, con) execute { implicit con =>
      assetProps.foldLeft(Map[Long, Int]()) {
        case (map, (assetId, props)) =>
          val assetLexId = getOrInsertLexime(assetId, DataType.ASSET)
          var nMap = map + assetLexId
          props.foreach {
            case (property, (Change(newCV), lastModified)) =>
              nMap += getOrInsertLexime(property.toString(), DataType.ANYURI)
              nMap ++= newCV.getValues.map { pve =>
                val lang = if (pve.getLang == null) None else Some(pve.getLang())
                getOrInsertLexime(pve.getValue(), pve.getType(), lang)
              }
            case _ =>
          }
          nMap
      }

    }
  }
  
  def saveProperties(assetProps: Map[DocId, Map[URI, (PropertyAction, Instant)]]): Unit = {
    try {
      repeatOnSQLStates(LeximeErrors, 10) execute { implicit con =>
        val hashes = createHashToIdMap(assetProps, con)
        val leximeIds = assetProps.map {
          case (assetId, props) =>
            val assetLexId = hashes(generateLexHash(assetId))
            val propsCvsOption = props.collect {
              case (property, (Change(newCV), lastModified)) => {
                val propertyLexId = hashes(generateLexHash(property.toString()))
                val newCvId = insertCompositeValue(newCV, hashes)
                Some((propertyLexId, Update(newCvId), lastModified, assetId, property))
              }
              case (property, (Remove(cv), lastModified)) => {
                val option = findExistingCvAndPropertyId(assetLexId, property.toString())
                option match {
                  case Some((cvId, propertyLexId, timestamp)) => {
                    if (lastModified != timestamp) {
                      throw new DocumentModifiedException(assetId + ":" + property)
                    }
                    Some((propertyLexId, Delete(cvId), lastModified, assetId, property))
                  }
                  case None => None
                }
              }
            }
            val propsCvs = for (option <- propsCvsOption if (option.isDefined)) yield option.get
            (assetLexId, propsCvs)
        }

        leximeIds foreach {
          case (assetLexId, propCv) =>
            propCv.foreach {
              case (propertyLexId, Update(newCvId), lastModified, assetId, property) =>
                val currentCvId = getCurrentCvId(assetLexId, propertyLexId)
                currentCvId match {
                  case Some((id, timestamp)) => {
                    if (lastModified != timestamp) {
                      throw new DocumentModifiedException(assetId + ":" + property + " modified on " + timestamp + " expected " + lastModified)
                    }
                    updateAssetPropertyCompositeValue(assetLexId, propertyLexId, newCvId)
                    deleteCompositeValue(id, con)
                  }
                  case None => {
                    insertAssetPropertyValue(assetLexId, propertyLexId, newCvId)
                  }
                }
              case (propertyLexId, Delete(cvId), lastModified, assetId, property) =>
                deleteAssetPropertyCompositeValue(assetLexId, propertyLexId, cvId)
            }
        }
      }
    } catch {
      case e: SerialException => throw new DocumentModifiedException(e)
    }
  }

  private def findExistingCvAndPropertyId(ali: Int, property: String)(implicit con: Connection) = {
    val hash = generateHash(property)
    val ps = wrap(FindExistingCompositeId, List('ali, 'hash))
    ps.singleValue('ali, ali)
    ps.singleValue('hash, hash)
    forOne { rs =>
      (rs.getInt(1), rs.getInt(2), new Instant(rs.getTimestamp(3).getTime()))
    } using (ps)
  }

  private def deleteAssetPropertyCompositeValue(ali: Int, pli: Int, cli: Int)(implicit con: Connection): Unit = {
    val deleteAP = wrap(DeleteAssetPropertyCompositeValue, List('ali, 'pli, 'cli))
    deleteAP.singleValue('ali, ali)
    deleteAP.singleValue('pli, pli)
    deleteAP.singleValue('cli, cli)
    deleteAP.executeUpdate
    val deleteCV = wrap(DeleteCompositeValue, List('cvId))
    deleteCV.singleValue('cvId, cli)
    deleteCV.executeUpdate
  }

  private def updateAssetPropertyCompositeValue(ali: Int, pli: Int, cli: Int)(implicit con: Connection): Unit = {
    val ps = wrap(UpdateAssetPropertyCompositeValue, List('cli, 'timestamp, 'ali, 'pli))
    ps.singleValue('cli, cli)
    ps.singleValue('ali, ali)
    ps.singleValue('pli, pli)
    ps.singleValue('timestamp, new Instant)
    ps.executeUpdate
  }

  private def insertAssetPropertyValue(ali: Int, pli: Int, cli: Int)(implicit con: Connection): Unit = {
    val ps = wrap(InsertAssetProperty, List('ali, 'pli, 'cli, 'timestamp))
    ps.singleValue('ali, ali)
    ps.singleValue('pli, pli)
    ps.singleValue('cli, cli)
    ps.singleValue('timestamp, new Instant)
    ps.executeUpdate
  }

  private def deleteCompositeValue(cli: Int, con: Connection) = {
    val ps = con.prepareStatement(DeleteCompositeValue)
    ps.setInt(1, cli)
    ps.executeUpdate
  }

  private def generateLexHash(value: String, language: Option[String] = None): Long = {
    language match {
      case Some(lang) =>
        generateHash(LiteralHashFormat.format(value, lang))
      case None => {
        generateHash(value)
      }
    }
  }

  private def getOrInsertLexime(lex: String, dataType: DataType, language: Option[String] = None)(implicit con: Connection) = {
    val hash = generateLexHash(lex, language)
    val stmtStr = "select " + dataTypeColMap(dataType)
    val colList = dataType match {
      case DataType.STRING | DataType.ANYURI | DataType.ASSET => List('hash, 'lex)
      case DataType.LITERALLANG => List('hash, 'lex, 'language, 'pglang)
      case DataType.WGS84_POINT => List('hash, 'lex, 'lon, 'lat)
      case DataType.WGS84_CIRCLE => List('hash, 'lex, 'lon, 'lat, 'radius)
      case DataType.WGS84_SHAPE => List('hash, 'lex, 'points)
      case d => List('hash, 'lex)
    }
    val aps = wrap(stmtStr, colList)
    aps.singleValue('hash, hash)
    dataType match {
      case DataType.LITERALLANG => {
        aps.objectValue('lex, lex)
        language match {
          case Some(n) => {
            aps.singleValue('language, n)
            aps.singleValue('pglang, mapToPgLang(n))
          }
          case None => {
            aps.singleValue('language, null)
            aps.singleValue('pglang, mapToPgLang(""))
          }
        }
      }
      case DataType.WGS84_POINT => {
        val point = dataTypeConvertor.convertPoint(lex)
        aps.singleValue('lon, point.getLongitude.toString)
        aps.singleValue('lat, point.getLatitude.toString)
        aps.singleValue('lex, lex)
      }
      case DataType.WGS84_SHAPE => {
        val shape = dataTypeConvertor.convertShape(lex).map { point =>
          point.getLongitude.toString + " " + point.getLatitude.toString
        }
        aps.arrayValue('points, shape)
        aps.singleValue('lex, lex)
      }
      case DataType.WGS84_CIRCLE => {
        val circle = dataTypeConvertor.convertCircle(lex)
        aps.singleValue('lon, circle.getLongitude.toString)
        aps.singleValue('lat, circle.getLatitude.toString)
        aps.singleValue('radius, circle.getRadius.toString)
        aps.singleValue('lex, lex)
      }
      case d => aps.singleValue('lex, lex)
    }

    val id = try {
      forOne { rs =>
        rs.getInt(1)
      } using (aps)
    } catch {
      case e: Exception => {
        debug(s"Insert of $hash for [$lex] failed")
        throw e
      }
    }
    (hash, id.get)
  }

  private def getCurrentCvId(ali: Int, pli: Int)(implicit con: Connection) = {
    val gcvps = wrap(GetCompositeValueId, List('ali, 'pli, 'timestamp))
    gcvps.singleValue('ali, ali)
    gcvps.singleValue('pli, pli)
    var result =
      forOne { rs =>
        (rs.getInt(1), new Instant(rs.getTimestamp(2).getTime()))
      } using (gcvps)
    result
  }

  private def insertCompositeValue(cv: CompositeValue, hashes:Map[Long, Int])(implicit con: Connection) = {
    val cvid = getNextRepositoryId(con)
    val cvps = wrap(InsertCompositeValue, List('cvid, 'leximeId))
    debug("Inserting " + cv)
    cv.getValues.foreach { pve =>
      val lang = if (pve.getLang == null) None else Some(pve.getLang())
      val leximeId = hashes(generateLexHash(pve.getValue(), lang))
      cvps.singleValue('cvid, cvid)
      cvps.singleValue('leximeId, leximeId)
      debug("Inserting CV " + cvid + ", " + leximeId)
      cvps.executeUpdate
    }
    cvid
  }

  private def makePolygon(shape: List[WGS84Point]): String = {
    val builder = new StringBuilder("POLYGON((")
    for (point <- shape) {
      builder.append(point.getLongitude.toString).append(" ").append(point.getLatitude.toString)
      builder.append(",")
    }
    builder.deleteCharAt(builder.lastIndexOf(","))
    builder.append("))")
    builder.toString
  }

  private def getNextRepositoryId(implicit con: Connection) = {
    val ps = wrap(NextLeximeId)
    val id = forOne { rs =>
      rs.getInt(1)
    } using (ps)
    id match {
      case Some(n) => n
      case None => throw new RuntimeException("Failed to get Repository id")
    }
  }
}

sealed class Action(cvId: Int)
case class Update(cvId: Int) extends Action(cvId)
case class Delete(cvId: Int) extends Action(cvId)
