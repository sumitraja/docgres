package biz.mediabag.docgres.model

import java.io.File

import biz.mediabag.documentapi.{Metadata, MetadataGroup}
import biz.mediabag.documentapi.impl.DocumentModelDSL

import scala.Array.canBuildFrom
import scala.io.Source


trait SearchgresDocumentModelReader {
  def models: Iterable[SearchgresDocumentModel] 
}

trait CompilingSearchgresDocumentModelReader extends SearchgresDocumentModelReader {

  import scala.reflect.runtime._
  import scala.tools.reflect.ToolBox

  private val toolBox = currentMirror.mkToolBox()
  val sources: Iterable[String]
  val withImports = List("java.net.URI", "biz.mediabag.documentapi.impl.DocumentModelDSL", "biz.mediabag.documentapi.impl.LangLiteralWrapper",
    "biz.mediabag.documentapi.impl.DocumentModelDSL._", "eu.mediabag.commons.DataType._", "eu.mediabag.commons.LangLiteral",
    "eu.mediabag.commons.Multiplicity._", "biz.mediabag.documentapi.Metadata", "biz.mediabag.documentapi.MetadataGroup",
    "eu.mediabag.commons.Domain._", "eu.mediabag.commons.Usage._", "biz.mediabag.docgres.model.searchgresModel")

  val imports = withImports.map(p => s"import $p").mkString("\n")
  def models: Iterable[SearchgresDocumentModel] = {
    sources.map { str =>
      val tree = toolBox.parse(s"$imports\n$str")
      toolBox.eval(tree).asInstanceOf[DocumentModelDSL[Metadata, MetadataGroup[Metadata]]#DocumentModelWrapper].documentModel.asInstanceOf[SearchgresDocumentModel]
    }
  }
}

class FileModelReader(val directory: File) extends CompilingSearchgresDocumentModelReader {
  val list = directory.listFiles
  val sources = if (list != null) {
    val files = directory.listFiles.filter(p => p.isFile && p.getName.endsWith(".model.scala")).map(Source.fromFile(_, "UTF-8")).toList
    files.map { src =>
      val str = src.mkString
      src.close
      str
    }
  } else {
    List()
  }
  override val models = super.models
}