package biz.mediabag.docgres.document

import java.net.URI
import java.sql.Connection

import biz.mediabag.documentapi.impl.{Domain, User, System, Unchanged}
import biz.mediabag.documentapi.query.Query.OpType
import biz.mediabag.documentapi.query.{DocumentResult, DocumentResultList, Query}
import biz.mediabag.documentapi.{Document, DocumentModel, DocumentRepositoryException, Metadata, MetadataGroup}
import biz.mediabag.postgres.jdbc.StatementWrapper.wrap
import biz.mediabag.postgres.jdbc.{JDBCOps, StatementWrapper}
import biz.mediabag.docgres.{And, QueryOpsToSQL, SQLConstants, ToSQL}
import eu.mediabag.commons.document.CompositeValue
import eu.mediabag.commons.{DataType, Multiplicity}
import grizzled.slf4j.Logging
import org.joda.time.Instant

trait SearchgresDocumentReader[M <: Metadata, MG <: MetadataGroup[M], DM <: DocumentModel[M, MG]] extends ToSQL with QueryOpsToSQL { self: JDBCOps with SQLConstants=>

  type DocResult = (String, URI, (String, String, Instant))

  def maxCount: Int

  implicit def toURI(s: String) = new URI(s)

  //TODO merge this with QueryToSQL
  private def GetAllAssetProperties = s"""select a.lex as $DocumentUUID, b.lex $Property, c.lex $PropertyValue, c.language as
  $Language, apa.last_modified $LastModified $SelectFromBody where a.hash = any(?)"""

  private lazy val GetSelectedAssetProperties = GetAllAssetProperties + " and b.hash = any(?)"

  private lazy val CountAssets = s"""select distinct a.lex $DocumentUUID from $LeximeTable a join 
  $AssetPropertiesTable b on a.id = b.$LinkedDocumentId where is_document = true and a.hash = ANY(?)"""

  def getDocumentProperties(uuid: String): Option[List[(URI, (String, String, Instant))]] = {
    val hash = List(generateHash(uuid))
    withConn(Connection.TRANSACTION_REPEATABLE_READ) {
      implicit con =>
        val wrapper = wrap(GetAllAssetProperties, List('hash))
        wrapper.arrayValue('hash, hash)
        forEach {
          rs =>
            new URI(rs.getString(Property)) -> (rs.getString(PropertyValue), rs.getString(Language),
              new Instant(rs.getTimestamp(LastModified).getTime))
        } using (wrapper)
    }
  }

  protected def createDocumentImplFromProps(uuid: String, modelURI: URI) = {
    val props = getDocumentProperties(uuid)
    props match {
      case Some(list) => {
        list.find {
          case (prop, _) => prop == modelURI
        } match {
          case Some((prop, (model, lang, instant))) => {
            val am = getModel(new URI(model))
            if (am == null) {
              throw new DocumentRepositoryException("Unknown model " + model + " for document " + uuid)
            }
            val initialProps = convertToCompositeValueMap(list, modelURI, am)
            Some((initialProps, am))
          }
          case None => throw new DocumentRepositoryException("Document does not have the model property "
            + modelURI.toString() + " set")
        }
      }
      case None => None
    }
  }

  def getModel(modelURI: URI): DM

  def getDocumentProperties(uuids: Iterable[String], properties: Iterable[URI]): Option[Map[String, List[DocResult]]] = {
    val hash = uuids.map {
      uuid => generateHash(uuid)
    }
    val propertyHashes = properties.map {
      prop => generateHash(prop.toString())
    }

    withConn {
      implicit con =>
        val wrapper = wrap(GetSelectedAssetProperties, List('hash, 'propHashes))
        wrapper.arrayValue('hash, hash)
        wrapper.arrayValue('propHashes, propertyHashes)
        val result = getDocuments(wrapper)
        if (result.size > 0) {
          Some(result)
        } else {
          None
        }
    }
  }

  def checkDocumentsPresent(uuids: Iterable[String]): Iterable[String] = {
    val hash = uuids.map {
      uuid => generateHash(uuid)
    }

    val result = withConn(Connection.TRANSACTION_REPEATABLE_READ) {
      implicit con =>
        val wrapper = wrap(CountAssets, List('hashes))
        wrapper.arrayValue('hashes, hash)
        forEach {
          rs =>
            rs.getString(DocumentUUID)
        } using (wrapper)
    }
    if (result.isDefined) {
      uuids.toSet.diff(result.get.toSet)
    } else {
      Nil
    }
  }

  def getDocuments(aq: DocumentQuery): Map[String, List[DocResult]] = {
    withConn(Connection.TRANSACTION_REPEATABLE_READ) { implicit con =>
      val prep = aq.prepare
      getDocuments(prep)
    }
  }

  def getDocumentsWithScore(aq: DocumentQuery): List[(Float, List[DocResult])] = {
    withConn(Connection.TRANSACTION_REPEATABLE_READ) { implicit con =>
      val prep = aq.prepare
      getScoredDocuments(prep)
    }
  }

  def getDocumentCount(aq: DocumentQuery): Int = {
    withConn(Connection.TRANSACTION_REPEATABLE_READ) { implicit con =>
      val sw = aq.prepareCount
      forOne {
        rs =>
          rs.getInt(1)
      } using (sw) match {
        case Some(n) => n
        case None => 0
      }
    }
  }

  def getDocuments(sw: StatementWrapper): Map[String, List[DocResult]] = {
    debug("Using sql " + sw.toDebugSQL)
    forEach {
      rs =>
        (rs.getString(DocumentUUID), new URI(rs.getString(Property)), (rs.getString(PropertyValue), rs.getString(Language),
          new Instant(rs.getTimestamp(LastModified).getTime)))
    } using (sw) match {
      case Some(list) => {
        list.groupBy {
          case (uuidURIData) => uuidURIData._1
        }
      }
      case None => Map()
    }
  }

  def getScoredDocuments(sw: StatementWrapper): List[(Float, List[DocResult])] = {
    debug("Using sql " + sw.toDebugSQL)
    forEach {
      rs =>
        (rs.getFloat(Score), (rs.getString(DocumentUUID), new URI(rs.getString(Property)), (rs.getString(PropertyValue), rs.getString(Language),
          new Instant(rs.getTimestamp(LastModified).getTime))))
    } using (sw) match {
      case Some(list) => {
        //see http://stackoverflow.com/questions/4761386/scala-list-function-for-grouping-consecutive-identical-elements
        val l = list.tail.foldLeft(List(list take 1)) {
          case (acc @ (lst @ hd :: _) :: tl, el) =>
            if (el._2._1 == hd._2._1) (el :: lst) :: tl
            else (el :: Nil) :: acc
          case _ => throw new RuntimeException("Invalid list value returned")
        }
        l.map {
          lst =>
            val score = lst(0)._1
            val scoreRemoved = lst.map {
              case (score, rest) => rest
            }
            (score, scoreRemoved)
        }
      }
      case None => Nil
    }
  }

  def convertToCompositeValueMap(props: List[(URI, (String, String, Instant))], modelURI: URI,
    am: DocumentModel[M, MG]): Map[URI, (Unchanged, Instant)] = {
    convertToCompositeValueMap(props, {
      prop:URI =>
        if (prop == modelURI) {
          (DataType.ANYURI, Multiplicity.SIMPLE)
        } else {
          val modelProp = am.getProperty(prop)
          if (modelProp != null) {
            (modelProp.getType, modelProp.getMultiplicity)
          } else if (am.isFreeForm()) {
            (DataType.STRING, Multiplicity.SIMPLE)
          } else {
            throw new IllegalArgumentException("Unknown property " + prop + " for model " + am.getURI)
          }
        }
    })
  }

  private def convertToCompositeValueMap(props: List[(URI, (String, String, Instant))],
                                         getDataType: (URI) => (DataType, Multiplicity)): Map[URI, (Unchanged, Instant)] = {
    props.foldLeft(Map[URI, (Unchanged, Instant)]()) {
      (accum, value) =>
        value match {
          case (prop, (value, lang, lastModified)) =>
            val (dataType, multi) = getDataType(prop)
            val cvInst = accum.get(prop) match {
              case Some((Unchanged(cv), instant)) =>
                cv.addValue(value, lang, dataType); (Unchanged(cv), instant)
              case None => (Unchanged(new CompositeValue(multi, value, lang, dataType)), lastModified)
              case e => throw new RuntimeException("Bad match for properties" + e)
            }
            accum + (prop -> cvInst)
        }
    }
  }
  
  import biz.mediabag.docgres.ToSQL._

  private lazy val AssociatedDocumentValuesStmt = customSelect(LexValue) join (CompositeValuesTable + " cv") on {
    value => s"cv.value = $AliasColumn.id"
  } join (AssetPropertiesTable + " as apa") onCustom "apa.composite_value_id = cv.id" where (HashValue, OpType.ANY_OF, And)

  protected def findUnassociatedDocumentValues(ids: Set[String]): Set[String] = {
    val hashes = ids.map(id => generateHash(id))
    val assetQuery = new DocumentQuery
    
    val found = withConn {
      implicit conn =>
        val ps = wrap(assetQuery.cteToSQL(AssociatedDocumentValuesStmt), List('hashArray))
        ps.arrayValue('hashArray, hashes)
        forEach {
          rs =>
            rs.getString(1)
        } using (ps)
    }
    found match {
      case Some(lst) => {
        ids.diff(lst.toSet)
      }
      case None => ids
    }
  }

  abstract class SearchgresDocumentResultList[DOC <: Document](q: Query, val modelURI: URI) extends DocumentResultList[DOC] with Logging {

    import scala.collection.JavaConversions._

    private var resultsDirty = true
    private var count = -1
    private var results: List[DocumentResult[DOC]] = Nil
    private var offset = 0
    private var rowLimit = 1000
    private val documentQuery = buildDocumentQuery(q, 1000, 0)
    getNumberOfResults()

    override def getNumberOfResults: Int = {
      if (count == -1) {
        count = getDocumentCount(documentQuery)
      }
      count
    }

    override def getResults = {
      if (resultsDirty) {
        documentQuery.setLimitAndOffset(rowLimit, offset)
        val qr = getDocumentsWithScore(documentQuery)
        results = time("getResults") { processSearchResults(qr) }
        resultsDirty = false
      }
      results
    }

    override def getDocumentsGroupedByProperties(properties: java.util.List[URI]) = {
      null
    }

    override def getDocumentsGroupedByModel() = {
      null
    }

    override def setStart(start: Int) = {
      resultsDirty = true
      offset = start
    }

    override def setNumber(number: Int) = {
      resultsDirty = true
      rowLimit = number
    }

    private def processSearchResults(assetProps: List[(Float, List[DocResult])]) = {
      assetProps.map {
        list =>
          val score = list._1
          list._2.find {
            case (uuid, prop, _) => prop == modelURI
          } match {
            case Some((uuid, prop, (model, lang, instant))) => {
              val am = getModel(new URI(model))
              if (am == null) {
                throw new DocumentRepositoryException("Unknown model " + model + " for document " + uuid)
              }
              val props = list._2.map {
                case (uuid, prop, (model, lang, instant)) => (prop, (model, lang, instant))
              }
              val initialProps = convertToCompositeValueMap(props, modelURI, am)
              createResult(uuid, initialProps, am, score)
            }
            case None => throw new DocumentRepositoryException("Bad results, model could not be found for document " + list._2(0)._1)
          }
      }
    }

    def createResult(uuid: String, initialProps: Map[URI, (Unchanged, Instant)], dm: DM, score: Float): DocumentResult[DOC]
  }

}

