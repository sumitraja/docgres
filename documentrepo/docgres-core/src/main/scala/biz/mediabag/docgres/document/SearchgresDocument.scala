package biz.mediabag.docgres.document

import java.net.URI
import java.util.UUID

import biz.mediabag.documentapi.{Document, Metadata, MetadataGroup}
import biz.mediabag.documentapi.impl._
import biz.mediabag.docgres.model.{SearchgresDocumentModel, SearchgresDocumentValidator}
import eu.mediabag.commons.DataType
import eu.mediabag.commons.document.CompositeValue
import org.joda.time.Instant

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

class SearchgresDocument(@BeanProperty val uuid:String, override val model: SearchgresDocumentModel, override val properties: Map[URI, (Unchanged, Instant)])
extends Document with DocumentLike[Metadata, MetadataGroup[Metadata]] {
  private[docgres] def getChangedProperties = cachedProperties
  val validator = new SearchgresDocumentValidator
  
  def getModel = model
 
  override def getProperty(name: URI): CompositeValue = {
    getProperty(model.getProperty(name))
  }
  
  override def setProperty(property: URI, value: CompositeValue) {
    setProperty(model.getProperty(property), value)
  }
  
  override def removeProperty(property: URI) {
    removeProperty(model.getProperty(property))
  }
  
  override def validate = {
    validator.validate(this, model, Nil)
  }
}

object SearchgresDocument {

   def create(model: SearchgresDocumentModel, modelProperty:URI) : SearchgresDocument = {
     val doc = new SearchgresDocument(UUID.randomUUID.toString, model, Map())
     doc.cachedProperties += (modelProperty -> (Change(new CompositeValue(model.getURI.toString, DataType.ANYURI)), new Instant, System()))
     doc
  }

}