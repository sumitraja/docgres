package biz.mediabag.docgres

trait SQLConstants {
  def schema:String
  def tablePrefix:String

  val DocumentUUID = "document"
  val Property = "property"
  val PropertyValue = "property_value"
  val LastModified = "last_modified"
  val Language = "language"
  val Score = "score"
  val LinkedDocumentId = DocumentUUID + "_id"
  
  def Prefix = {
    assert(schema != null, "Schema may need early init")
    assert(tablePrefix != null, "Table prefix may need early init")
    s"$schema.$tablePrefix" + "_"
  }
  def LeximeTable = Prefix + "lexime"
  def TagsTable = Prefix + "tags"
  def AssetPropertiesTable = Prefix + "document_properties"
  def CompositeValuesTable = Prefix + "composite_values"
  def OrganisationsTable = Prefix + "organisations"
}