package biz.mediabag.docgres.model

import java.net.URI

import biz.mediabag.documentapi.{DocumentModel, DocumentValidator, Metadata, MetadataGroup}
import biz.mediabag.documentapi.impl.{DocumentModelLike, _}
import biz.mediabag.docgres.document.SearchgresDocument
import eu.mediabag.commons.{LangLiteral, State}
import scala.collection.JavaConversions._

class SearchgresDocumentModel(val uri: URI, val description: LangLiteral, val freeForm: Boolean)
  extends DocumentModel[Metadata, MetadataGroup[Metadata]] with DocumentModelLike[Metadata, MetadataGroup[Metadata]] {

  def canEqual(other: Any): Boolean = other.isInstanceOf[SearchgresDocumentModel]

  override def equals(other: Any): Boolean = other match {
    case that: SearchgresDocumentModel =>
      (that canEqual this) &&
        uri == that.uri
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(uri)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

class SearchgresDocumentValidator extends DocumentValidator[SearchgresDocument, Metadata, MetadataGroup[Metadata]](ISODataTypeConvertor) {

  def getUnsavedProperties(document: SearchgresDocument, ignoreProps: java.util.Collection[URI]): java.util.Map[URI, State] = {
    document.getChangedProperties.collect {
      case (uri, (action, instant, User())) if !ignoreProps.contains(uri) =>
        val state = action match {
          case Change(n) => State.CHANGED
          case Remove(n) => State.REMOVED
          case Unchanged(n) => throw new RuntimeException("Unxpected Unchanged" + n + " in asset changed properties")
        }
        uri -> state
    }
  }
}
