package biz.mediabag.docgres

import java.util.UUID

import biz.mediabag.documentapi.query.Query.OpType
import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.docgres.ToSQL.TagValue

import scala.collection.immutable.Queue

abstract class Value(val columnAlias:String, val lexColumn: String, val dataTypeCheck:String = "IS NOT NULL") {
  val fieldName = columnAlias + "." + lexColumn
  override def hashCode = lexColumn.hashCode
  def dataTypeLookup(prefix:String = "") = s"$prefix$lexColumn $dataTypeCheck as is_"+lexColumn
  override def equals(other: Any) = other match {
    case that: Value => this.lexColumn == that.lexColumn
    case _ => false
  }
  
  def generateWhere(sqb: SQLQueryBuilder, operator: OpType, boolOp: BooleanOperator, group: Group): SQLQueryBuilder = {
    operator match {
      case OpType.EQUALS => sqb where(fieldName + " = ?", boolOp, group)
      case OpType.GREATER_THAN => sqb where(fieldName + " > ?", boolOp, group)
      case OpType.IS_SET => sqb where(fieldName + " IS NOT NULL", boolOp, group)
      case OpType.IS_NOT_SET => sqb where(fieldName + " IS NULL", boolOp, group)
      case OpType.LESS_THAN => sqb where(fieldName + " < ?", boolOp, group)
      case OpType.ANY_OF => sqb where(fieldName + "= ANY(?)", boolOp, group)
      case OpType.BETWEEN => sqb where(fieldName + " BETWEEN ? AND ?", boolOp, group)
      case _ => throw new RuntimeException("Cannot handle " + operator.name + " in a where clause")
    }
  }
}

class BooleanOperator(val opString: String)
case object And extends BooleanOperator("and")
case object Or extends BooleanOperator("or")

class Group
case object GroupOpen extends Group
case object GroupClose extends Group
case object GroupNone extends Group

class SQLQueryBuilder(val mainValue: Value, val customSelect: Boolean, val joinTables: Queue[JoinTable] = Queue(), val unions: Queue[SQLQueryBuilder] = Queue(),
                      val wheres: Queue[(String, BooleanOperator, Group)] = Queue(), val rank: String = null, val joinParams: Queue[(Symbol, _, Manifest[_])] = Queue(),
                      val whereParams: Queue[(Symbol, _, Manifest[_])] = Queue(), val limitParams: Queue[(Symbol, _, Manifest[_])] = Queue(), val limitProperty: Boolean = false,
                      val offsetAndlimit: Boolean = false, val state: StatementState = Join, val groupBy: Boolean = false) {

  def this() = this(null, false)

  def this(value: Value) = this(value, false)

  def apply(mainValue: Value) = {
    new SQLQueryBuilder(mainValue, customSelect)
  }

  def withRank(rank: (String) => String) = {
    if (mainValue == null) {
      throw new IllegalArgumentException("A main value must be specified to use this rank function. Use rank(()=>String) instead");
    }
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank(mainValue.fieldName))

  }

  def withRank(rank: => String) = {
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank, joinParams, whereParams, limitParams, limitProperty, offsetAndlimit, state, groupBy)
  }

  def join(joinTable: String) = {
    new JoinTable(joinTable, this)
  }

  def groupById = {
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank, joinParams, whereParams, limitParams, limitProperty, offsetAndlimit, state, true)
  }

  def where(operator: OpType, boolOp: BooleanOperator): SQLQueryBuilder = {
    where(mainValue, operator, boolOp)
  }

  def where(operator: OpType): SQLQueryBuilder = {
    where(mainValue, operator, null)
  }

  def where(filterValue: Value, operator: OpType, boolOp: BooleanOperator): SQLQueryBuilder = {
    where(filterValue, operator, boolOp, GroupNone)
  }

  def where(filterValue: Value, operator: OpType, boolOp: BooleanOperator, group: Group): SQLQueryBuilder = {
    operator match {
      case OpType.EQUALS => where(filterValue.fieldName + " = ?", boolOp, group)
      case OpType.ANCESTOR if filterValue == TagValue => where(filterValue.fieldName + " @> ?", boolOp, group)
      case OpType.DESCENDANT if filterValue == TagValue => where(filterValue.fieldName + " <@ ?", boolOp, group)
      case OpType.GREATER_THAN => where(filterValue.fieldName + " > ?", boolOp, group)
      case OpType.IS_SET => where(filterValue.fieldName + " IS NOT NULL", boolOp, group)
      case OpType.IS_NOT_SET => where(filterValue.fieldName + " IS NULL", boolOp, group)
      case OpType.LESS_THAN => where(filterValue.fieldName + " < ?", boolOp, group)
      case OpType.ANY_OF => where(filterValue.fieldName + "= ANY(?)", boolOp, group)
      case OpType.BETWEEN => where(filterValue.fieldName + " BETWEEN ? AND ?", boolOp, group)
      case _ => throw new RuntimeException("Cannot handle " + operator.name + " in a where clause")
    }
  }

  def where(strGen: (Value) => String): SQLQueryBuilder = {
    where(strGen(mainValue), null, GroupNone)
  }

  def where(strGen: (Value) => String, boolOp: BooleanOperator): SQLQueryBuilder = {
    where(strGen(mainValue), boolOp, GroupNone)
  }

  def where(strGen: (Value) => String, boolOp: BooleanOperator, group: Group): SQLQueryBuilder = {
    where(strGen(mainValue), boolOp, group)
  }

  def where(str: String, boolOp: BooleanOperator, group: Group): SQLQueryBuilder = {
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres.enqueue((str, boolOp, group)), rank,
      joinParams, whereParams, limitParams, limitProperty, offsetAndlimit, Where, groupBy)
  }

  def union(builder: SQLQueryBuilder): SQLQueryBuilder = {
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions.enqueue(builder), wheres, rank,
      joinParams, whereParams, limitParams, limitProperty, offsetAndlimit, state, groupBy)
  }

  private def param[C, V](sym: Symbol, value: C, mani: Manifest[V]): SQLQueryBuilder = {
    state match {
      case Join => new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank,
        joinParams.enqueue(sym, value, mani), whereParams, limitParams, limitProperty, offsetAndlimit, state, groupBy)
      case Where => new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank,
        joinParams, whereParams.enqueue(sym, value, mani), limitParams, limitProperty, offsetAndlimit, state, groupBy)
      case LimitAndOffset => new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank,
        joinParams, whereParams, limitParams.enqueue(sym, value, mani), limitProperty, offsetAndlimit, state, groupBy)
    }

  }

  def limitPropertyParam(properties: Iterable[String]): SQLQueryBuilder = {
    val hashes = properties.map(generateHash(_))
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank, joinParams, whereParams, limitParams, true,
      offsetAndlimit, Join, groupBy).param(getUnique, hashes, Manifest.Long)
  }

  def param[T <: Any](value: T, mani: Manifest[T]): SQLQueryBuilder = {
    val sym = getUnique
    param(sym, value, mani)
  }

  def implicitParam[T <: Any](value: T)(implicit mani: Manifest[T]): SQLQueryBuilder = {
    val sym = getUnique
    param(sym, value, mani)
  }

  def param[T <: Any](value: Iterable[T], mani: Manifest[T]): SQLQueryBuilder = {
    val sym = getUnique
    param(sym, value, mani)
  }

  def withLimitAndOffset(limit: Int, offset: Int) = {
    new SQLQueryBuilder(mainValue, customSelect, joinTables, unions, wheres, rank,
      joinParams, whereParams, limitParams, limitProperty, true, LimitAndOffset, groupBy).param(getUnique, limit,
        Manifest.Int).param(getUnique, offset, Manifest.Int)
  }

  def params = joinParams.toList ::: whereParams.toList ::: limitParams.toList

  override def hashCode = {
    41 * (41 * (41 * (41 * (41 * (41 + (if (mainValue != null) mainValue.hashCode else 0))
      + (if (limitProperty) limitProperty.hashCode else 0)) +
      (if (groupBy) groupBy.hashCode else 0)) + (if (wheres.size > 0) wheres.hashCode else 0)) +
      (if (unions.size > 0) unions.hashCode else 0)) + (if (joinTables.size > 0) joinTables.hashCode else 0)
  }

  override def equals(other: Any) = other match {
    case that: SQLQueryBuilder => this.joinTables == that.joinTables && this.unions == that.unions && this.wheres == that.wheres &&
      this.mainValue == that.mainValue && this.rank == that.rank && this.groupBy == that.groupBy && this.limitProperty == that.limitProperty
    case _ => false
  }

  def getUnique = Symbol(UUID.randomUUID().toString().hashCode().toString)
}

class StatementState
case object Join extends StatementState
case object Where extends StatementState
case object LimitAndOffset extends StatementState

class JoinTable(val name: String, private val thiz: SQLQueryBuilder, private val joinClause: String = null) {

  def on(joinClauseBuilder: (Value) => String): SQLQueryBuilder = {
    val jt = new JoinTable(name, null, joinClauseBuilder(thiz.mainValue))
    new SQLQueryBuilder(thiz.mainValue, thiz.customSelect, thiz.joinTables.enqueue(jt), thiz.unions, thiz.wheres, thiz.rank, thiz.joinParams,
      thiz.whereParams, thiz.limitParams, thiz.limitProperty, thiz.offsetAndlimit, thiz.state, thiz.groupBy)
  }

  def on(joinClauseBuilder: => String): SQLQueryBuilder = {
    val jt = new JoinTable(name, null, joinClauseBuilder)
    new SQLQueryBuilder(thiz.mainValue, thiz.customSelect, thiz.joinTables.enqueue(jt), thiz.unions, thiz.wheres, thiz.rank, thiz.joinParams,
      thiz.whereParams, thiz.limitParams, thiz.limitProperty, thiz.offsetAndlimit, thiz.state, thiz.groupBy)
  }

  def onCustom(joinCondition: String): SQLQueryBuilder = {
    val jt = new JoinTable(name, null, joinCondition)
    new SQLQueryBuilder(thiz.mainValue, thiz.customSelect, thiz.joinTables.enqueue(jt), thiz.unions, thiz.wheres, thiz.rank, thiz.joinParams,
      thiz.whereParams, thiz.limitParams, thiz.limitProperty, thiz.offsetAndlimit, thiz.state, thiz.groupBy)
  }

  def toSQL = {
    name + " on " + joinClause
  }

  override def hashCode = {
    var hash = 1;
    hash = hash * 31 + name.hashCode
    hash = hash * 31 + thiz.hashCode
    hash
  }

  override def equals(other: Any) = other match {
    case that: JoinTable => this.joinClause == that.joinClause && this.name == that.name && this.thiz == that.thiz
    case _ => false
  }
}