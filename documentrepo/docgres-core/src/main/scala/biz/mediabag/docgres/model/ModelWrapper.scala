package biz.mediabag.docgres.model

import java.net.URI

import biz.mediabag.documentapi.impl.DocumentModelDSL
import biz.mediabag.documentapi.impl.DocumentModelDSL.toURI
import biz.mediabag.documentapi.{Metadata, MetadataGroup}
import eu.mediabag.commons.{DataType, LangLiteral, Multiplicity, Usage}

object searchgresModel extends DocumentModelDSL[Metadata, MetadataGroup[Metadata]] {

  def apply(uri: String, desc: LangLiteral) = {
    val model = new SearchgresDocumentModel(uri, desc, false)
    new DocumentModelWrapper(model)
  }

  def createMetadataGroup(multi: Multiplicity) = new MetadataGroup[Metadata](multi)

  def createMetadataItem(uri: URI, datatype: DataType, multi: Multiplicity, usage: Usage) = new Metadata(uri, datatype, multi, usage)
  
}