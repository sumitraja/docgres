package biz.mediabag.docgres

import biz.mediabag.documentapi.query.FindMeAll
import biz.mediabag.docgres.ToSQL._

trait QueryPlugin {

	def initialise:Unit
	
	def contribute(fma: FindMeAll[_], cte: SQLQueryBuilder)
	
}