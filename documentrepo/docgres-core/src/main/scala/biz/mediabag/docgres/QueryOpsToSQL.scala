package biz.mediabag.docgres
import biz.mediabag.documentapi.query.Query.{DataTypeQueryGroup, OpType}
import biz.mediabag.documentapi.query.{FindMeAll, OperationNotApplicableException, Query, QueryException}
import biz.mediabag.postgres.jdbc.{HashUtils, PgLangMapper}
import biz.mediabag.docgres.ToSQL._
import eu.mediabag.commons.{DataType, LangLiteral, WGS84Circle, WGS84Point}
import grizzled.slf4j.Logging

import scala.Option.option2Iterable
import scala.collection.JavaConversions._


/**
 * Once RETURN QUERY (http://www.postgresql.org/docs/9.4/static/plpgsql-control-structures.html) doesn't use work mem for
 * building up the result set then get rid of this class and convert this all to pgSQL functions
 */
trait QueryOpsToSQL extends PgLangMapper with HashUtils with Logging { self: ToSQL =>

  def buildDocumentQuery(query: Query, rowLimit: Int, offset: Int) = {
    val ctes = query.getOperations.map { op => buildSQLQuery(op) }
    if (ctes.size == 0) {
      throw new QueryException("No query elements specified!")
    }
    val assetQuery = new DocumentQuery
    assetQuery.ctes(ctes)
    assetQuery
  }

  def buildSQLQuery(fma: FindMeAll[_]) = {
    val valueTypes = fma.getValueType
    if (valueTypes == null && !(fma.getOpType() == OpType.IS_SET || fma.getOpType() == OpType.IS_NOT_SET)) {
      throw new QueryException("Bad finder in query " + fma + ". No expected value type found")
    }

    val cte = if (fma.getProperty != null) {
      (select withRank ("1")).limitPropertyParam(List(fma.getProperty.toString))
    } else {
      select withRank ("1")
    }

    fma.group match {
      case DataTypeQueryGroup.BOOLEANS | DataTypeQueryGroup.DATES | DataTypeQueryGroup.DATETIMES
        | DataTypeQueryGroup.NUMERICS | DataTypeQueryGroup.STRINGS | DataTypeQueryGroup.TIMES => combineUsingWhere(fma, cte)
      case _ => fma.getOpType match {
        case OpType.EQUALS | OpType.GREATER_THAN | OpType.IS_NOT_SET | OpType.IS_NOT_SET | OpType.IS_SET | OpType.LESS_THAN
          | OpType.ANY_OF | OpType.DESCENDANT | OpType.ANCESTOR => combineUsingWhere(fma, cte)
        case _ => combineComplex(fma, cte)
      }
    }
  }

  private def combineUsingWhere(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    val cteRet = fma.getOpType match {
      case e if (e == OpType.IS_NOT_SET | e == OpType.IS_SET) => {
        fma.group match {
          case DataTypeQueryGroup.STRINGS => cte where (IsString, e, Or, GroupOpen) where (IsAnyURI, e, Or) where (IsDocument, e, null, GroupClose)
          case grp => {
            val columns = getColumnsForGroup(grp)
            val builtCte = columns.foldLeft(cte) { (sqb, value) =>
              sqb where (value, e, Or)
            }
            builtCte
          }
        }
      }
      case OpType.EQUALS if (fma.getDataType == DataType.LITERALLANG) => {
        val ll = fma.getSingleValue.asInstanceOf[LangLiteral]
        val builtCte = ll.getLangToLiteralMap.foldLeft(cte) { (sqb, langLit) =>
          if (langLit._1 == null) {
            sqb where (HashValue, OpType.EQUALS, And) param (generateHash(langLit._2), Manifest.Long)
          } else {
            sqb where (HashValue, OpType.EQUALS, And) where (LanguageColumn + " = ?", null, GroupNone) param (generateHash(langLit._2), Manifest.Long) param (
              langLit._1, manifest[String])
          }
        }
        builtCte
      }
      case OpType.EQUALS if fma.group == DataTypeQueryGroup.STRINGS => {
        cte where (HashValue, OpType.EQUALS, And) where (IsString, OpType.EQUALS, Or, GroupOpen) where (IsAnyURI, OpType.EQUALS, Or) where (
          IsDocument, OpType.EQUALS, null, GroupClose) param (generateHash(fma.getSingleValue.asInstanceOf[String]), Manifest.Long) param (
            true, Manifest.Boolean) param (true, Manifest.Boolean) param (true, Manifest.Boolean)
      }
      case OpType.ANY_OF if fma.group == DataTypeQueryGroup.STRINGS => {
        cte where (HashValue, OpType.ANY_OF, And) where (IsString, OpType.EQUALS, Or, GroupOpen) where (IsAnyURI, OpType.EQUALS, Or) where (
          IsDocument, OpType.EQUALS, null, GroupClose) param (fma.getValues.map { fmval => generateHash(fmval.asInstanceOf[String]) }, Manifest.Long) param (
            true, Manifest.Boolean) param (true, Manifest.Boolean) param (true, Manifest.Boolean)
      }
      case OpType.EQUALS if fma.group == DataTypeQueryGroup.GEOGRAPHIES | fma.group == DataTypeQueryGroup.GEOGRAPHIES_WITH_DIM => {
        val mani = Manifest.classType(fma.getSingleValue.getClass)
        cte where (PointValue.fieldName + "= ?::geometry", Or, GroupOpen) param (fma.getSingleValue, mani.asInstanceOf[Manifest[Any]]) where (
          ShapeValue.fieldName + "= ?::geometry", Or, GroupClose) param (fma.getSingleValue, mani.asInstanceOf[Manifest[Any]])
      }
      case _ if fma.getDataType != DataType.LITERALLANG && fma.group != DataTypeQueryGroup.STRINGS => {
        val mani = Manifest.classType(fma.getSingleValue.getClass)
        val columns = fma.group.getDataTypes.map { dt =>
          dt match {
            case DataType.DATE | DataType.DATETIME => DateTimeValue
            case DataType.DECIMAL => NumericValue
            case DataType.INTEGER => NumericValue
            case DataType.DURATION => DurationValue
            case DataType.TIME => TimeValue
            case DataType.TAG => TagValue
            case DataType.WGS84_CIRCLE => CircleValue
            case DataType.WGS84_POINT => PointValue
            case DataType.WGS84_SHAPE => ShapeValue
            case e => throw new OperationNotApplicableException("Bad operation " + fma.getOpType + " for value with data type " + fma.getDataType)
          }
        }.toSet

        columns.foldLeft(cte) { (sqb, value) =>
          if (fma.getOpType == OpType.ANY_OF) {
            sqb where (value, fma.getOpType, Or) param (fma.getValues, mani.asInstanceOf[Manifest[Any]])
          } else if (fma.getOpType == OpType.BETWEEN) {
            val values = fma.getValues.toList
            sqb where (value, fma.getOpType, Or) param (values(0), mani.asInstanceOf[Manifest[Any]]) param (values(1), mani.asInstanceOf[Manifest[Any]])
          } else {
            sqb where (value, fma.getOpType, Or) param (fma.getSingleValue, mani.asInstanceOf[Manifest[Any]])
          }
        }

      }
      case e => throw new OperationNotApplicableException("Bad operation " + e.name + " for value with data type " + fma.getDataType)
    }
    cteRet
  }

  private def getColumnsForGroup(group: DataTypeQueryGroup) = {
    group.getDataTypes.flatMap { dt =>
      dt match {
        case DataType.BOOLEAN => Some(BooleanValue)
        case DataType.DATE | DataType.DATETIME => Some(DateTimeValue)
        case DataType.DECIMAL => Some(NumericValue)
        case DataType.INTEGER => Some(NumericValue)
        case DataType.DURATION => Some(DurationValue)
        case DataType.TIME => Some(TimeValue)
        case DataType.WGS84_CIRCLE => Some(CircleValue)
        case DataType.WGS84_POINT => Some(PointValue)
        case DataType.WGS84_SHAPE => Some(ShapeValue)
        case DataType.TAG => Some(TagValue)
        case _ => None
      }
    }.toSet
  }

  private def combineComplex(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    val cteRet = fma.getOpType match {
      case OpType.WITHIN_DISTANCE_OF_POINT => {
        fma.getDataType match {
          case DataType.WGS84_CIRCLE => withinDistanceOfPoint(fma, cte)
          case e => throw new OperationNotApplicableException("Bad data type " + e.name + " for operator " + OpType.WITHIN_DISTANCE_OF_POINT.name)
        }
      }
      case OpType.WITHIN => {
        val cte = select
        fma.getDataType match {
          case DataType.WGS84_CIRCLE => withinCircle(fma, cte)
          case DataType.WGS84_SHAPE | DataType.WGS84_POINT => withinShape(fma, cte)
          case e => throw new OperationNotApplicableException("Bad data type " + e.name + " for operator " + OpType.WITHIN.name)
        }
      }
      case OpType.OVERLAPS if (fma.getDataType == DataType.WGS84_CIRCLE || fma.getDataType == DataType.WGS84_SHAPE) => {
        overlaps(fma, cte)
      }
      case OpType.TEXT_SEARCH => {
        fma.getDataType match {
          case DataType.LITERALLANG => {
            textSearchLangLiteral(fma, cte)
          }
          case DataType.TAG => {
            throw new UnsupportedOperationException("Free text tag seaching is not supported yet")
          }
          case e => throw new OperationNotApplicableException("Bad data type " + e.name + " for operator " + OpType.WITHIN.name)
        }
      }
      case e => throw new OperationNotApplicableException("Bad operation type " + e.name + " on " + fma.getDataType + " for group " + fma.group)
    }
    cteRet
  }

  private def withinDistanceOfPoint(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    val circle = fma.getSingleValue.asInstanceOf[WGS84Circle]
    if (fma.group == DataTypeQueryGroup.GEOGRAPHIES) {
      val lst = List(CircleValue, PointValue, ShapeValue)
      val rankStr = """case when %s is not null then (? - ST_Distance(sourcePoint, %s, false))/? 
        else (? - ST_Distance(sourcePoint, %s, false))/? end""".format(lst(0).fieldName, lst(0).fieldName, lst(1).fieldName)
      cte withRank (rankStr) param (circle.getRadius.doubleValue,
        Manifest.Double) param (circle.getRadius.doubleValue,
          Manifest.Double) param (circle.getRadius.doubleValue,
            Manifest.Double) param (circle.getRadius.doubleValue,
              Manifest.Double) join "ST_GeographyFromText(?) as sourcePoint" on (
                "(ST_Dwithin(sourcePoint," + lst(0).fieldName + ", ?)  OR ST_Dwithin(sourcePoint," + lst(1).fieldName + ", ?))") param (
                  circle, manifest[WGS84Point]) param (
                    circle.getRadius.doubleValue, Manifest.Double) param (
                      circle.getRadius.doubleValue, Manifest.Double)
    } else if (fma.group == DataTypeQueryGroup.GEOGRAPHIES_WITH_DIM) {
      cte(ShapeValue) withRank (varname => "(? - ST_Distance(sourcePoint, " + varname + ", false))/?") param (circle.getRadius.doubleValue,
        Manifest.Double) param (circle.getRadius.doubleValue, Manifest.Double) join
        "ST_GeographyFromText(?) as sourcePoint" on {
          varname =>
            "ST_Dwithin(sourcePoint," + varname.fieldName + ", ?)"
        } param (
          circle, manifest[WGS84Point]) param (
            circle.getRadius.doubleValue, Manifest.Double)
    } else {
      throw new QueryException(fma + " cannot be used to query for a distance from point")
    }
  }

  private def withinCircle(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    val circle = fma.getSingleValue.asInstanceOf[WGS84Circle]
    if (fma.group == DataTypeQueryGroup.GEOGRAPHIES) {
      val values = List(PointValue, ShapeValue)
      cte join "ST_Buffer(ST_GeographyFromText(?),?) as geog" on ("((ST_Contains(geog::geometry, " + values.head.fieldName
        + "::geometry) and " + values.head.fieldName + " is not null) OR (ST_Contains(geog::geometry, " + values.last.fieldName
        + "::geometry) and " + values.last.fieldName + ")") param (circle, manifest[WGS84Point]) param (
          circle.getRadius.doubleValue, Manifest.Double)
    } else {
      cte(CircleValue) join "ST_Buffer(ST_GeographyFromText(?),?) as geog" on { varname =>
        "ST_Contains(geog::geometry, " + varname.fieldName + "::geometry)"
      } param (circle, manifest[WGS84Point]) param (
        circle.getRadius.doubleValue, Manifest.Double)
    }
  }

  private def withinShape(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    val ptSh = cte join "(ST_GeographyFromText(?) as geog" on {
      if (fma.group == DataTypeQueryGroup.GEOGRAPHIES) {
        "ST_Contains(geog::geometry, " + PointValue.fieldName + "::geometry) OR ST_Contains(geog::geometry, " + ShapeValue.fieldName + "::geometry))"
      } else {
        "ST_Contains(geog::geometry, " + ShapeValue.fieldName + "::geometry)"
      }
    }
    if (fma.getDataType == DataType.WGS84_POINT) {
      ptSh param (fma.getSingleValue.asInstanceOf[WGS84Point], manifest[WGS84Point])
    } else {
      val str = makePostGISPolygon(fma.getSingleValue.asInstanceOf[List[WGS84Point]])
      ptSh param (str, manifest[String])
    }
  }

  private def overlaps(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    if (fma.getDataType == DataType.WGS84_SHAPE) {
      val str = makePostGISPolygon(fma.getSingleValue.asInstanceOf[List[WGS84Point]])
      cte(ShapeValue) join "ST_GeographyFromText(?) as sourceGeom" on { varname =>
        "ST_Overlaps(" + varname.fieldName + "::geometry, sourceGeom::geometry)"
      } param (
        str, manifest[String])
    } else {
      val circle = fma.getSingleValue.asInstanceOf[WGS84Circle]
      cte(ShapeValue) join "ST_Buffer(ST_GeographyFromText(?),?) as sourceGeom" on { varname =>
        "ST_Overlaps(" + varname.fieldName + "::geometry, sourceGeom::geometry)"
      } param (
        circle, manifest[WGS84Point]) param (
          circle.getRadius.doubleValue, Manifest.Double)
    }
  }

  private def textSearchLangLiteral(fma: FindMeAll[_], cte: SQLQueryBuilder) = {
    val ll = fma.getSingleValue.asInstanceOf[LangLiteral]
    val (lang, lit) = if (ll.getLangToLiteralMap.size > 1) {
      warn("Multiple language literals found for parameter " + fma + " using default")
      (null, ll.getDefaultLiteral)
    } else {
      ll.getLangToLiteralMap.head
    }
    val trimmedLit = "[ ]{1,}".r.replaceAllIn("""[\&\|\(\)]""".r.replaceAllIn(lit.trim, ""), " & ")
    if (lang == null || lang == "") {
      cte(TextValue) withRank { varname =>
        "ts_rank_cd(" + varname + ", to_tsquery(?), 16)"
      } where { varname =>
        "to_tsquery(?) @@ " + varname.fieldName
      } param (trimmedLit, manifest[String]) param (trimmedLit, manifest[String])
    } else {
      cte(TextValue) withRank { varname =>
        "ts_rank_cd(" + varname + ", to_tsquery(?::regconfig, ?), 16)"
      } where { varname =>
        "to_tsquery(?::regconfig, ?) @@ " + varname.fieldName + " and " + LanguageColumn + " = ?"
      } param (mapToPgLang(lang), manifest[String]) param (
        trimmedLit, manifest[String]) param (
          mapToPgLang(lang), manifest[String]) param (
            trimmedLit, manifest[String]) param (lang, manifest[String])
    }
  }
  private def makePostGISPolygon(shape: List[WGS84Point]) = {
    var builder = "POLYGON((" +
      shape.map { point =>
        point.getLongitude.toString + " " + point.getLatitude.toString
      }.reduceLeft { (acc, point) =>
        acc + "," + point
      }
    builder += "))"
    builder
  }

  private def completeShape(shape: List[WGS84Point]) = {
    if (!shape(0).equals(shape(shape.size - 1))) {
      val point = shape.get(shape.size - 1)
      val completeShape = shape ::: List(point)
      completeShape
    } else {
      shape
    }
  }

}