package biz.mediabag.docgres

import java.sql.Connection
import java.util.UUID

import biz.mediabag.documentapi.query.Query.OpType
import biz.mediabag.postgres.jdbc.StatementWrapper
import biz.mediabag.postgres.jdbc.StatementWrapper._
import biz.mediabag.docgres.ToSQL.TagValue._

import scala.collection.immutable.Queue


trait ToSQL {
  self: SQLConstants =>

  import biz.mediabag.docgres.ToSQL._

  def select(value: Value) = new SQLQueryBuilder(value)

  def select() = new SQLQueryBuilder()

  def customSelect(value: Value) = new SQLQueryBuilder(value, true)

  def SelectFromBody = s""" from $LeximeTable a join $AssetPropertiesTable apa on a.id = apa.$LinkedDocumentId
    join $LeximeTable b on b.id = apa.property_id join $CompositeValuesTable cvc on apa.composite_value_id = cvc.id
    join $LeximeTable c on cvc.value = c.id"""

  def DataTypeValuesColumns = ToSQL.DataTypeValues.map { value => value.dataTypeLookup("c.") }.mkString(", ")

  def SelectHead = s"""select distinct a.lex as $DocumentUUID, b.lex $Property, c.lex $PropertyValue, c.language as $Language,
      apa.last_modified as $LastModified, $DataTypeValuesColumns"""

  // mutable object
  class DocumentQuery {

    private lazy val SelectBody = SelectFromBody + " join %s on a.id = %s.id"

    private def BaseCountSelect(finalCTEName: String) =
      s"""select count(distinct a.lex) as asset_count """ + SelectBody.format(finalCTEName, finalCTEName)

    private def BaseSelect(finalCTEName: String) = {
      SelectHead + s""", $finalCTEName.$Score as $Score """ + SelectBody.format(finalCTEName, finalCTEName) +
        """ order by score desc, a.lex, b.lex"""
    }

    private var cteList = Queue[SQLQueryBuilder]()

    def cte(sql: SQLQueryBuilder) = {
      cteList = cteList.enqueue(sql)
    }

    def ctes(sqls: Iterable[SQLQueryBuilder]) = {
      cteList = cteList.enqueue(sqls.toList)
    }

    def toSQL = toBaseSQL(BaseSelect)

    def toCountSQL = toBaseSQL(BaseCountSelect)

    private def toBaseSQL(selectMethod: (String) => String) = {
      var counter = CTEStartIndex - 1
      val cteMap = cteList.map {
        sql =>
          counter = counter + 1
          counter -> sql
      }

      val scores = cteMap.map {
        case (name, sql) =>
          "CTE_" + name
      }

      val scoresStringReduce = if (scores.size == 0) {
        ""
      } else if (scores.size == 1) {
        "(" + scores(0) + "." + Score + ")"
      } else {
        "(" + scores.flatMap(r => List(r + "." + Score + "/" + scores.size)).mkString(" + ") + ")"
      }

      val cteString = cteMap.map {
        case (counter, sql) =>
          "CTE_" + counter + " as (" + cteToSQL(sql, counter) + ")"
      }.reduceLeft {
        (rest, n) =>
          rest + "," + n
      }

      val withString = "with " + cteString

      withString + "\n" + selectMethod("CTE_" + counter)
    }

    def prepare(implicit con: Connection): StatementWrapper = {
      prepare(toSQL)
    }

    def setLimitAndOffset(limit: Int, offset: Int = 0) {
      val last = cteList.last.withLimitAndOffset(limit, offset)
      cteList = cteList.slice(0, cteList.size - 1).enqueue(last)
    }

    private def prepare(sqlString: String)(implicit con: Connection): StatementWrapper = {

      val paramsList = for {
        cte <- cteList
        cteParams <- cte.params
      } yield cteParams._1

      val sw = wrap(sqlString, paramsList.toList)

      val valuesManifest = for {
        cte <- cteList
        cteParam <- cte.params
      } yield (cteParam._1, cteParam._2, cteParam._3)
      valuesManifest.foreach {
        case (sym, value, mani) =>
          sw.singleOrArrayValue(sym, value)(mani.asInstanceOf[Manifest[Any]])
      }
      sw
    }

    def prepareCount(implicit con: Connection): StatementWrapper = {
      prepare(toCountSQL)
    }

    def cteToSQL(sqb: SQLQueryBuilder, counter: Int = CTEStartIndex) = {
      var retSQL = if (!sqb.customSelect) {
        if (counter == CTEStartIndex) {
          firstCTE(sqb.rank, sqb.limitProperty)
        } else {
          subsequentCTE(sqb.rank, "CTE_" + (counter - 1), sqb.limitProperty)
        }
      } else {
        "select " + sqb.mainValue.fieldName + " from " + LeximeTable + " cc ";
      }

      sqb.joinTables foreach {
        jt =>
          retSQL += " join " + jt.toSQL
      }

      if (sqb.limitProperty) {
        retSQL += " join " + LeximeTable + " apa_cc on apa.property_id = apa_cc.id and apa_cc.hash = ANY(?)"
      }

      if (sqb.wheres.size > 0) {
        val mappedWheres = sqb.wheres.foldLeft("") { (acc, where) =>
          acc + "\n" + (where._3 match {
            case GroupOpen => "("
            case _ => ""
          }) + where._1 +
            (if (where._2 != null && sqb.wheres.indexOf(where) != sqb.wheres.size - 1) " " + where._2.opString + " " else "") + (where._3 match {
            case GroupClose => ")"
            case _ => ""
          })
        }
        retSQL += " where " + mappedWheres
      }

      if (sqb.groupBy) {
        retSQL += "\ngroup by ccvc.id"
      }

      if (sqb.offsetAndlimit) {
        retSQL += s"\norder by apa.$LinkedDocumentId limit ? offset ?"
      }
      retSQL
    }

  }

  private def SelectStart = s"select apa.$LinkedDocumentId as id"

  private def Rank = ", %s as " + Score

  private def FirstCTEJoin = s""" from $LeximeTable $AliasColumn join $CompositeValuesTable ccvc on cc.id = ccvc.value join
    $AssetPropertiesTable apa on apa.composite_value_id = ccvc.id """

  private def SubsequentCTEJoin = s""" from $AssetPropertiesTable apa join %s on apa.$LinkedDocumentId = %s.id join
      $CompositeValuesTable ccvc on ccvc.id = apa.composite_value_id  join $LeximeTable cc on ccvc.value = cc.id"""

  private val CTELimit = "order by apa.$LinkedDocumentId limit ? offset ?"

  def firstCTE(rank: String, limitToProperty: Boolean) =
    SelectStart + (if (rank != null) Rank.format(rank) else "") + FirstCTEJoin

  def subsequentCTE(rank: String, firstCTEName: String, limitToProperty: Boolean) = SelectStart + (if (rank != null) {
    Rank.format(rank)
  } else {
    ""
  }) + SubsequentCTEJoin.format(firstCTEName, firstCTEName)

}

object ToSQL {

  val CTEStartIndex = 1
  val AliasColumn = "cc"
  val HashColumn = AliasColumn + ".hash"
  val LanguageColumn = AliasColumn + ".language"
  val LexColumn = AliasColumn + ".lex"
  val IdColumn = AliasColumn + ".id"

  case object NumericValue extends Value(AliasColumn, "numeric_val")

  case object TextValue extends Value(AliasColumn, "text_val")

  case object LexValue extends Value(AliasColumn, "lex")

  case object ShapeValue extends Value(AliasColumn, "shape_val")

  case object PointValue extends Value(AliasColumn, "point_val")

  case object BooleanValue extends Value(AliasColumn, "boolean_val")

  case object DateTimeValue extends Value(AliasColumn, "datetime_val")

  case object TimeValue extends Value(AliasColumn, "time_val")

  case object DurationValue extends Value(AliasColumn, "duration_val")

  case object CircleValue extends Value(AliasColumn, "shape_val")

  case object TagValue extends Value(AliasColumn, "tag_val") {
    override def generateWhere(sqb: SQLQueryBuilder, operator: OpType, boolOp: BooleanOperator, group: Group): SQLQueryBuilder = {
      operator match {
        case OpType.ANCESTOR => sqb where(fieldName + " @> ?", boolOp, group)
        case OpType.DESCENDANT => sqb where(fieldName + " <@ ?", boolOp, group)
        case _ => super.generateWhere(sqb, operator, boolOp, group)
      }
    }
  }

  case object HashValue extends Value(AliasColumn, "hash") {
    override def generateWhere(sqb: SQLQueryBuilder, operator: OpType, boolOp: BooleanOperator, group: Group): SQLQueryBuilder = {
      operator match {
        case OpType.EQUALS => sqb where(fieldName + "= ?", boolOp, group)
        case OpType.ANY_OF => sqb where(fieldName + "= ANY (?)", boolOp, group)
        case _ => throw new RuntimeException("Cannot handle " + operator.name + " in a comparision on hash in a where clause")
      }
    }
  }

  case object UUIDValue extends Value(AliasColumn, "uuid_val")

  case object IsString extends Value(AliasColumn, "is_string", "IS TRUE")

  case object IsDocument extends Value(AliasColumn, "is_document", "IS TRUE")

  case object IsAnyURI extends Value(AliasColumn, "is_uri", "IS TRUE")

  case object IsInteger extends Value(AliasColumn, "is_int", "IS TRUE")

  def DataTypeValues = List[Value](NumericValue, TextValue, ShapeValue, PointValue, BooleanValue, DateTimeValue, TimeValue,
    DurationValue, CircleValue, TagValue, UUIDValue, IsString, IsDocument, IsAnyURI, IsInteger)
}