package biz.mediabag.docgres

import java.net.URI
import java.util.UUID

import biz.mediabag.documentapi.impl.{Change, Unchanged}
import biz.mediabag.documentapi.query.{DocumentResult, DocumentResultList, Query}
import biz.mediabag.documentapi.{DocumentModel, DocumentRepository, Metadata, MetadataGroup}
import biz.mediabag.postgres.jdbc.{FlywayConfiguration, JDBCConfiguration, JDBCOps}
import biz.mediabag.docgres.document.{SearchgresDocument, SearchgresDocumentReader, SearchgresDocumentWriter}
import biz.mediabag.docgres.model.{SearchgresDocumentModel, SearchgresDocumentModelReader}
import biz.mediabag.docgres.tag.{SearchgresTagWriter, SearchgresTagReader}
import biz.mediabag.tagstore.TagManager
import eu.mediabag.commons.{DataType, DataTypeConvertor}
import eu.mediabag.commons.DateFormats._
import eu.mediabag.commons.document.CompositeValue
import org.joda.time.Instant
import scala.collection.JavaConversions._

abstract class SearchgresDocumentRepository extends DocumentRepository[Metadata, MetadataGroup[Metadata], SearchgresDocument, SearchgresDocumentModel]
  with TagManager with SearchgresDocumentReader[Metadata, MetadataGroup[Metadata], SearchgresDocumentModel] with SearchgresDocumentWriter
  with SearchgresTagReader with SearchgresTagWriter { self:SearchgresDocumentRepositoryConfiguration =>
  
  override protected val dataTypeConvertor = new DataTypeConvertor(ISO8601_FULL_FORMAT_STRING_MS, ISO8601_FULL_FORMAT_STRING,
    ISO8601_DATE_FORMAT_STRING, ISO8601_TIME_FORMAT_STRING)

  override def tablePrefix = "searchgres"

  def maxCount = config.getInt("maxResults")

  val ModelProperty = new URI(DocumentModel.MODEL_URI)

  def validateAndSave(document: SearchgresDocument) = {
    val map = document.validate
    if (map.size() > 0) {
      map
    } else {
      saveProperties(Map(document.getUuid -> document.getChangedProperties.map {
        case (uri, (pa, instant, domain)) => uri -> (pa, instant)
      }))
      null
    }
  }

  def get(uuid: String) = {
    createDocumentImplFromProps(uuid, DocumentModel.MODEL_URI) match {
      case Some((initialProps, docModel)) => new SearchgresDocument(uuid, docModel, initialProps)
      case None => null
    }
  }

  def create(docType: URI): SearchgresDocument = {
    val dm = getModel(docType)
    SearchgresDocument.create(dm, ModelProperty)
  }

  def search(query: Query): DocumentResultList[SearchgresDocument] = new SearchgresRepoResultListNG(query, ModelProperty)

  def remove(id: String) = {
    throw new IllegalAccessException("Not implemented yet. Will archive eventually")
  }

  def getModel(modelURI: URI): SearchgresDocumentModel = modelReader.models.find(p => p.uri == modelURI) match {
    case Some(model) => model
    case None => null
  }

  //TODO: Remove this class once assetrepo is gone
  class SearchgresRepoResultListNG(q: Query, modelURI: URI) extends SearchgresDocumentResultList[SearchgresDocument](q, modelURI) {
    def createResult(uuid: String, initialProps: Map[URI, (Unchanged, Instant)], dm: SearchgresDocumentModel, score: Float) = {
      new DocumentResult(new SearchgresDocument(uuid, dm, initialProps), score)
    }
  }
  def getTagById(id: String) = {
    searchTagById(id) match {
      case Some(n) => n
      case None => null
    }
  }

  def getTagsByTitle(title: String, lang: String) = {
    searchTagsByTitle(title, lang)
  }

  def getTagsWithIdMatchingPattern(pattern :String)= {
    searchTagsWithIdMatchingPattern(pattern)
  }
}

trait SearchgresDocumentRepositoryConfiguration extends SQLConstants with JDBCConfiguration with FlywayConfiguration {

  protected def modelReader: SearchgresDocumentModelReader

  override def schema = getRepositorySchema

  protected def getRepositorySchema: String = getFlywaySchema

  override protected def getFlywaySchema: String

  override protected def getFlywayLocations = List("db/searchgres")

  override protected def getFlywayTable = "flyway_searchgres_table"

  override def createSchemas = true

  override def getFlywayTablePrefix = tablePrefix

}