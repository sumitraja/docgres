CREATE SEQUENCE ${schemaName}.${tablePrefix}_repository_id
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  CACHE 1;
ALTER TABLE ${schemaName}.${tablePrefix}_repository_id
  OWNER TO contentmgr;

CREATE TABLE ${schemaName}.${tablePrefix}_lexime
(
  id integer,
  hash bigint NOT NULL,
  lex text,
  datetime_val timestamp without time zone,
  numeric_val numeric,
  point_val geography(Point,4326),
  shape_val geography(Polygon,4326),
  text_val tsvector,
  duration_val interval,
  language character varying(2), -- ISO 639-1 language code for the literal
  money_val money,
  boolean_val boolean,
  time_val time without time zone,
  tag_val ltree,
  uuid_val uuid,
  is_string boolean,
  is_uri boolean,
  is_document boolean,
  is_int boolean,
  CONSTRAINT ${tablePrefix}_lexime_pk PRIMARY KEY (hash ),
  CONSTRAINT ${tablePrefix}_lexime_id_unique UNIQUE (id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ${schemaName}.${tablePrefix}_lexime
  OWNER TO contentmgr;
COMMENT ON COLUMN ${schemaName}.${tablePrefix}_lexime.language IS 'ISO 639-1 language code for the literal';

CREATE INDEX ${tablePrefix}_lexime_idx_tag_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING GIST
  (tag_val );

CREATE INDEX ${tablePrefix}_lexime_idx_boolean_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (boolean_val );

CREATE INDEX ${tablePrefix}_lexime_idx_datetime_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (datetime_val , id );

CREATE INDEX ${tablePrefix}_lexime_idx_duration_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (duration_val );

CREATE INDEX ${tablePrefix}_lexime_idx_hash_numeric_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING hash
  (numeric_val );

CREATE UNIQUE INDEX ${tablePrefix}_lexime_idx_id
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (id );

CREATE INDEX ${tablePrefix}_lexime_idx_is_document
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (is_document );

CREATE INDEX ${tablePrefix}_lexime_idx_is_string
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (is_string );

CREATE INDEX ${tablePrefix}_lexime_idx_is_uri
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (is_uri );

CREATE INDEX ${tablePrefix}_lexime_idx_language
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (language COLLATE pg_catalog."default" );

CREATE INDEX ${tablePrefix}_lexime_idx_numeric_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (numeric_val );

CREATE INDEX ${tablePrefix}_lexime_idx_point_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING gist
  (point_val );

CREATE INDEX ${tablePrefix}_lexime_idx_shape_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING gist
  (shape_val );

CREATE INDEX ${tablePrefix}_lexime_idx_text_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING gin
  (text_val );

CREATE INDEX ${tablePrefix}_lexime_idx_time_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (time_val );

CREATE INDEX ${tablePrefix}_lexime_idx_uuid_val
  ON ${schemaName}.${tablePrefix}_lexime
  USING btree
  (uuid_val );

CREATE TABLE ${schemaName}.${tablePrefix}_composite_values
(
  id integer NOT NULL,
  value integer NOT NULL,
  CONSTRAINT ${tablePrefix}_composite_values_pk PRIMARY KEY (id , value ),
  CONSTRAINT ${tablePrefix}_composite_values_value_fk FOREIGN KEY (value)
      REFERENCES ${schemaName}.${tablePrefix}_lexime (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ${schemaName}.${tablePrefix}_composite_values
  OWNER TO contentmgr;

CREATE INDEX ${tablePrefix}_composite_values_idx_id
  ON ${schemaName}.${tablePrefix}_composite_values
  USING btree
  (id );

CREATE INDEX ${tablePrefix}_composite_values_idx_value
  ON ${schemaName}.${tablePrefix}_composite_values
  USING btree
  (value );

CREATE INDEX ${tablePrefix}_fki_composite_values_value_fk
  ON ${schemaName}.${tablePrefix}_composite_values
  USING btree
  (value );

CREATE TABLE ${schemaName}.${tablePrefix}_document_properties
(
  document_id integer NOT NULL,
  property_id integer NOT NULL,
  composite_value_id integer NOT NULL,
  last_modified timestamp without time zone NOT NULL,
  CONSTRAINT ${tablePrefix}_document_properties_pk PRIMARY KEY (document_id , property_id , composite_value_id ),
  CONSTRAINT ${tablePrefix}_document_properties_id_fk FOREIGN KEY (document_id)
      REFERENCES ${schemaName}.${tablePrefix}_lexime (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ${tablePrefix}_document_properties_property_id_fk FOREIGN KEY (property_id)
      REFERENCES ${schemaName}.${tablePrefix}_lexime (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ${tablePrefix}_document_properties_no_multiple_cv UNIQUE (document_id , property_id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ${schemaName}.${tablePrefix}_document_properties
  OWNER TO contentmgr;

CREATE INDEX ${tablePrefix}_document_properties_idx_a
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (document_id );

CREATE INDEX ${tablePrefix}_document_properties_idx_a_p_o
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (document_id , property_id );

CREATE INDEX ${tablePrefix}_document_properties_idx_c
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (composite_value_id );

CREATE INDEX ${tablePrefix}_document_properties_idx_p
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (property_id );

CREATE INDEX ${tablePrefix}_fki_document_properties_document_id_fk
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (document_id );

CREATE INDEX ${tablePrefix}_fki_document_properties_composite_value_fk
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (composite_value_id );

CREATE INDEX ${tablePrefix}_fki_document_properties_property_id_fk
  ON ${schemaName}.${tablePrefix}_document_properties
  USING btree
  (property_id );

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_duration(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isFieldNull boolean;
BEGIN
	SELECT id, (duration_val is null) INTO nextLeximeId, isFieldNull FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
	IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
        INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, duration_val) VALUES (nextLeximeId, in_hash, in_lex, in_lex::interval);
    ELSIF isFieldNull IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET duration_val = in_lex::interval;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_document(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isDocument boolean;
BEGIN
    SELECT id, is_document INTO nextLeximeId, isDocument FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
    	SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
        INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, is_document) VALUES (nextLeximeId, in_hash, in_lex, true);
    ELSIF isDocument IS false THEN
            UPDATE ${schemaName}.${tablePrefix}_lexime SET is_document = true;
    END IF;
	RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_anyuri(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isUri boolean;
BEGIN
    SELECT id, is_uri INTO nextLeximeId, isUri FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
        INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, is_uri) VALUES (nextLeximeId, in_hash, in_lex, true);
    ELSIF isUri IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET is_uri = true;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

-- in_lex is expected to be NFC normalised
CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_text(in_hash bigint, in_lex text, in_lang character varying(2), 
	in_pglang text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
BEGIN
    SELECT id INTO nextLeximeId FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NOT NULL THEN
    	RETURN nextLeximeId;
    END IF;
    SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	INSERT INTO ${schemaName}.${tablePrefix}_lexime(id, hash, lex, text_val, "language") VALUES (nextLeximeId, in_hash, 
					in_lex, to_tsvector(in_pglang::regconfig, in_lex), in_lang);
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_string(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isString boolean;
BEGIN
    SELECT id, is_string INTO nextLeximeId, isString FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
    	INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, is_string) VALUES (nextLeximeId, in_hash, in_lex, true);
    ELSIF isString IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET is_string = true;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_tag(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isFieldNull boolean;
BEGIN
    SELECT id, (tag_val is null) INTO nextLeximeId, isFieldNull FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
	IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
        INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, tag_val) VALUES (nextLeximeId, in_hash, in_lex, in_lex::ltree);
    ELSIF isFieldNull IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET tag_val = in_lex::ltree;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_numeric(in_hash bigint, in_lex text, in_int boolean   ) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isFieldNull boolean;
BEGIN
    SELECT id, (numeric_val is null) INTO nextLeximeId, isFieldNull FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	    INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, numeric_val, is_int) VALUES (nextLeximeId, in_hash, in_lex, in_lex::numeric, in_int);
	ELSIF isFieldNull IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET numeric_val = in_lex::numeric;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_datetime(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isFieldNull boolean;
BEGIN
    SELECT id, (datetime_val is null) INTO nextLeximeId, isFieldNull FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	    INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, datetime_val) VALUES (nextLeximeId, in_hash, in_lex, in_lex::timestamp);
	ELSIF isFieldNull IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET datetime_val = in_lex::timestamp;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_time(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
    isFieldNull boolean;
BEGIN
    SELECT id, (time_val is null) INTO nextLeximeId, isFieldNull FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
        SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	    INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, time_val) VALUES (nextLeximeId, in_hash, in_lex, in_lex::time);
	ELSIF isFieldNull IS false THEN
        UPDATE ${schemaName}.${tablePrefix}_lexime SET time_val = in_lex::time;
    END IF;
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_point(in_hash bigint, in_lex text, in_lon varchar, in_lat varchar) RETURNS integer as $$
DECLARE
	nextLeximeId int;
BEGIN
    SELECT id INTO nextLeximeId FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NOT NULL THEN
    	RETURN nextLeximeId;
    END IF;
    SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, point_val) VALUES (nextLeximeId, in_hash, in_lex, 
	ST_GeographyFromText('POINT(' || in_lon || ' ' || in_lat || ')'));
    
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_circle(in_hash bigint, in_lex text, in_lon varchar, in_lat varchar, in_radius varchar) RETURNS integer as $$
DECLARE
	nextLeximeId int;
BEGIN
    SELECT id INTO nextLeximeId FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NOT NULL THEN
    	RETURN nextLeximeId;
    END IF;
    SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, shape_val) VALUES (nextLeximeId, in_hash, in_lex, 
		ST_Buffer(ST_GeographyFromText('POINT(' || in_lon || ' ' || in_lat || ')'), in_radius::numeric));
    
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_shape(in_hash bigint, in_lex text, in_shape varchar[]) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	polygon_str text;
BEGIN
    SELECT id INTO nextLeximeId FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NOT NULL THEN
    	RETURN nextLeximeId;
    END IF;
    SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
	SELECT array_to_string(in_shape, ', ') INTO polygon_str;
	INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, shape_val) VALUES (nextLeximeId, in_hash, in_lex, 
		ST_GeographyFromText('POLYGON((' || polygon_str ||'))'));
    RETURN nextLeximeId;
END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION ${schemaName}.${tablePrefix}_insert_lexime_uuid(in_hash bigint, in_lex text) RETURNS integer as $$
DECLARE
	nextLeximeId int;
	isUuid boolean;
BEGIN
    SELECT id, (uuid is null) INTO nextLeximeId, isUuid FROM ${schemaName}.${tablePrefix}_lexime WHERE in_hash = hash;
    IF nextLeximeId IS NULL THEN
    	SELECT nextval('${schemaName}.${tablePrefix}_repository_id') INTO nextLeximeId;
        INSERT INTO ${schemaName}.${tablePrefix}_lexime (id, hash, lex, uuid_val) VALUES (nextLeximeId, in_hash, in_lex, in_lex);
    ELSIF isUuid IS false THEN
            UPDATE ${schemaName}.${tablePrefix}_lexime SET uuid_val = in_lex;
    END IF;
	RETURN nextLeximeId;
END;
$$ language plpgsql;

-- Tag Manager

CREATE TABLE ${schemaName}.${tablePrefix}_tags
(
  tagpath public.ltree not null,
  title text,
  lang text,
  search_text tsvector,
  titleHash bigint NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE ${schemaName}.${tablePrefix}_tags
  OWNER TO contentmgr;

CREATE INDEX ${tablePrefix}_tags_tagpath_idx
  ON ${schemaName}.${tablePrefix}_tags
  USING gist
  (tagpath );

CREATE INDEX ${tablePrefix}_tags_text_idx
  ON ${schemaName}.${tablePrefix}_tags
  USING gin
  (search_text );

ALTER TABLE ${schemaName}.${tablePrefix}_tags ADD CONSTRAINT ${tablePrefix}_tags_unique_idx UNIQUE (tagpath, lang);





