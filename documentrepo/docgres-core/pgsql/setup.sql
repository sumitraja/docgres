\c postgres
create database template_mediabag encoding='UTF-8';
create user contentmgr nosuperuser nocreatedb nocreaterole password 'contentmgr';
\c template_mediabag
CREATE SCHEMA assetrepository AUTHORIZATION contentmgr;
CREATE SCHEMA tagmanager AUTHORIZATION contentmgr;
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA assetrepository;
grant all on spatial_ref_sys to PUBLIC;
CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA assetrepository;
CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA assetrepository;
CREATE EXTENSION IF NOT EXISTS ltree WITH SCHEMA tagmanager;
CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA assetrepository;
create database assetrepository owner=contentmgr template=template_mediabag encoding='UTF-8';
create database test_assetrepository owner=contentmgr template=template_mediabag encoding='UTF-8';
