#!/bin/sh

# directory structure must be
# conf/ - all configuration file
# lib/ - all jar files

if [ "$1" = "debug" ] 
then
	JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,address=8800,server=y,suspend=n
fi

if [ "$1" = "profile" ] 
then
    JAVA_OPTS=-Xshare:off
fi

baseDir=`dirname "$0"`
runClassPath=.:./conf
if [ `uname -s` = "Darwin" ]; then
  for i in "${baseDir}"/conf/*; do
    runClassPath="${runClassPath}":"$i"
  done
  for i in "${baseDir}"/lib/*; do
    runClassPath="${runClassPath}":"$i"
  done
else
  runClassPath=".:"${baseDir}"/lib/*:${CLASSPATH}:"${baseDir}"/conf"
fi
echo "Classpath: ${runClassPath}"

"`which java`" $JAVA_OPTS -classpath "${runClassPath}" biz.mediabag.shell.MBConsole
