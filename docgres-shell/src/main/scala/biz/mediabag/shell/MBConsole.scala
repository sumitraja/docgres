package biz.mediabag.shell

import eu.mediabag.commons.{ DataType, _ }
import java.net.URI
import java.io.PrintWriter
import eu.mediabag.commons.document.PropertyValueElement
import eu.mediabag.commons.document.CompositeValue
import scala.collection.JavaConversions._

object MBConsole extends InterpreterWrapper with Setup {

  def welcomeMsg: String = "Welcome to the Searchgres Shell\n" + "The following variables are bound:\n" +
    "docRepo = Document Repository\ntagMgr = Tag Manager"
  def prompt: String = "\nmediabag> "
  val imports = List("biz.mediabag.documentapi.query._", "java.net.URI",
    "scala.collection.JavaConversions._", "biz.mediabag.shell.Utils._", "eu.mediabag.commons.DataType._",
    "eu.mediabag.commons.document._, eu.mediabag.commons._, biz.mediabag.shell.MBConsole._", "biz.mediabag.tagstore._",
    "biz.mediabag.documentapi.query.Query._", "biz.mediabag.documentapi.query.Query.DataTypeQueryGroup._",
    "biz.mediabag.documentapi.query.Query.OpType._", "eu.mediabag.commons.Multiplicity._")
  override val log = new PrintWriter(System.out)

  def main(args: Array[String]) {
    imports foreach (autoImport(_))
    bind("docRepo", docRepo)
    bind("tagMgr", docRepo)
    startInterpreting()
  }
}

object Utils {
  implicit def toURI(str: String) = {
    new URI(str)
  }

  implicit def toCompositeValue(tup: Tuple3[String, String, DataType]) {
    new CompositeValue(tup._1, tup._2, tup._3)
  }

  implicit def toCompositeValue(tup: Tuple2[Multiplicity, List[PropertyValueElement]]) {
    new CompositeValue(tup._1, tup._2.toArray:_*)
  }

  implicit def toCompositeValue(tup: Tuple4[Multiplicity, String, String, DataType]) {
    new CompositeValue(tup._1, tup._2, tup._3, tup._4)
  }

  implicit def toPropertyValueElement(tup: Tuple3[String, String, DataType]) {
    new PropertyValueElement(tup._1, tup._2, tup._3)
  }

  implicit def toTuple(pve: PropertyValueElement) {
    (pve.getValue, pve.getValue, pve.getType)
  }

  implicit def toLangLiteralWrapper(s:String) = {
    new LangLiteralWrapper(s)
  }

  implicit def toLangLiteral(llw:LangLiteralWrapper) = {
    llw.ll
  }
}

class LangLiteralWrapper(val literal: String, val ll: LangLiteral = new LangLiteral) {
  def this(ll: LangLiteral = new LangLiteral) = this(null, ll)
  def `@`(lang: String) = {
    ll.addLiteral(literal, lang)
    ll
  }

  def +(inll: LangLiteral) = {
    val map = inll.getLangToLiteralMap() ++ (ll.getLangToLiteralMap())
    new LangLiteral(map)
  }
}
