package biz.mediabag.shell

import java.io.{File, PrintWriter}

import biz.mediabag.docgres.model.FileModelReader
import biz.mediabag.docgres.{SearchgresDocumentRepository, SearchgresDocumentRepositoryConfiguration}
import org.joda.time.Interval

import scala.actors.Actor._


case class TimeEvent(eventName: String, start: Long, end: Long)
case class Stop

trait Setup {
  val docRepo = new {
    val modelReader = new FileModelReader(new File("models/"))
    val configFilename = "searchgres.jdbc.properties"
  } with SearchgresDocumentRepository with ShellConfiguration

  def log:PrintWriter
  
  def time[A](name: String)(fun: => A): A = {
    val start = System.currentTimeMillis
    val ret = fun
    val end = System.currentTimeMillis
    logwriter ! TimeEvent(name, start, end)
    ret
  }
  
  val logwriter = actor {
    loop {
      react {
        case e: TimeEvent =>
          val interval = new Interval(e.start, e.end);
          println(e.eventName + "," + e.start + "," + e.end + "," + interval.toDurationMillis)
          log.println(e.eventName + "," + e.start + "," + e.end + "," + interval.toDurationMillis)
        case s: Stop => {
          log.flush
          exit()
        }
      }
    }
  }
  logwriter.start
}

trait ShellConfiguration extends SearchgresDocumentRepositoryConfiguration {
  override def getFlywaySchema = config.getString("schema")
}

